package com.tryonics.ceylincononmotor.apimanager;


import android.content.Context;

import com.tryonics.ceylincononmotor.BuildConfig;
import com.tryonics.ceylincononmotor.util.AppPreferences;
import com.tryonics.ceylincononmotor.util.Constants;

import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by Dimal on 11/2/17.
 */

public class RxRestEngine {

    private static volatile RxRestEngine instance;
    private HttpLoggingInterceptor logging;
    private OkHttpClient.Builder httpClient;

    public static RxRestEngine getInstance() {

        if (instance == null) {
            synchronized (RxRestEngine.class) {
                if (instance == null) {
                    instance = new RxRestEngine();
                }
            }
        }
        return instance;
    }

    public ApiInterface getService(Context context) {

        AppPreferences appPreferences = new AppPreferences(context);

        logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BODY);

        //logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", appPreferences.getAuthorization()).build();
            return chain.proceed(request);
        });

        httpClient.connectTimeout(30, TimeUnit.SECONDS);
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        httpClient.writeTimeout(30, TimeUnit.SECONDS);

        //if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(logging);
        //}

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();

        return retrofit.create(ApiInterface.class);

    }

}
