package com.tryonics.ceylincononmotor.apimanager;

import com.tryonics.ceylincononmotor.model.CommonResponse;
import com.tryonics.ceylincononmotor.model.auth.AuthResponse;
import com.tryonics.ceylincononmotor.model.jobinit.JobInitResponse;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

/**
 * Created by Dimal Govinnage on 11/7/19.
 */

public interface ApiInterface {

    @Headers("Accept: application/json")
    @Multipart
    @POST("auth/login")
    Single<Response<AuthResponse>> requestLogin(
            @PartMap Map<String, RequestBody> params
    );


    @Headers("Accept: application/json")
    @Multipart
    @POST("{job_id}/job_init")
    Observable<Response<JobInitResponse>> jobInitialization(
            @Path("job_id") String backendJobId,
            @PartMap Map<String, RequestBody> params
    );

    @Headers("Accept: application/json")
    @Multipart
    @POST("{job_id}/data_sync")
    Observable<Response<CommonResponse>> formDataSubmit(
            @Path("job_id") String backendJobId,
            @PartMap Map<String, RequestBody> params
    );

    @Headers("Accept: application/json")
    @Multipart
    @POST("{job_id}/image_sync")
    Observable<Response<CommonResponse>> imageDataSubmit(
            @Path("job_id") String backendJobId,
            @PartMap Map<String, RequestBody> params
    );

    @Headers("Accept: application/json")
    @Multipart
    @POST("{job_id}/complete")
    Observable<Response<CommonResponse>> jobDataUploadComplete(
            @Path("job_id") String backendJobId,
            @Part("job_close_timestamp") RequestBody empty
    );

}
