package com.tryonics.ceylincononmotor.database;

/**
 * Created by Dimal on 27/04/2020.
 */

public final class DataBaseContract {

    interface Tables {
        String JOB_TABLE = "job_table";
        String IMAGE_TABLE = "image_table";
        String FORM_DATA_TABLE = "form_data_table";
        String LOCATION_DATA_TABLE = "location_data_table";
    }

    interface JobTableColumns {
        String ID = "id";
        String SMS_REFERENCE_NO = "sms_job_ref_no";
        String SMS_CLAIM_NO = "sms_claim_no";
        String SMS_JOB_TYPE = "sms_job_type"; //(OST and UWI)
        String SMS_RECEIVED_TIME = "sms_received_time";
        String JOB_START_TIME = "job_start_time";
        String CURRENT_JOB_STATUS = "current_job_status"; //PENDING | INPROGRESS | CLOSED
        String SYNC_JOB_ID = "sync_job_id";
        String SYNC_STATUS = "sync_status";
        String SYNC_COMPLETE = "sync_complete";
    }

    interface ImageTableColumns {
        String ID = "id";
        String SMS_CLAIM_NO = "sms_claim_no";
        String CATEGORY_ID = "category_id";
        String IMAGE_URI = "image_uri";
        String IMAGE_CAPTURE_TIME = "image_capture_time";
        String IMAGE_SYNC_COMPLETE = "image_sync_complete";
    }

    interface FormDataTableColumns {
        String ID = "id";
        String SMS_CLAIM_NO = "sms_claim_no";
        String CATEGORY_ID = "category_id";
        String SURVEY_DATE = "survey_date";
        String LOSS_DATE = "loss_date";
        String CLAIM_AMOUNT = "claim_amount";
        String CAUSE_OF_LOSS = "cause_of_loss";
        String SALVAGE = "salvage";
        String NOTE = "note";
        String CLAIM_ADJUSTMENT = "claim_adjustment";
        String FORM_SYNC_COMPLETE = "form_sync_complete";
    }

    interface LocationDataTableColumns {
        String ID = "id";
        String SMS_CLAIM_NO = "sms_claim_no";
        String CATEGORY_ID = "category_id";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String LOCATION_TYPE = "location_type";
        String LOCATION_SYNC_COMPLETE = "form_sync_complete";
    }
}
