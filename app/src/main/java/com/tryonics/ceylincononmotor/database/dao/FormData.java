package com.tryonics.ceylincononmotor.database.dao;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

@Entity(
        indexes = {
                @Index(value = "smsClaimNo,categoryId", unique = true)
        }
)
public class FormData implements Parcelable {


    @Id(autoincrement = true)
    @Index(unique = true)
    private Long id;
    private String smsClaimNo;
    private int categoryId;
    private String riskLocation;
    private String surveyDate;
    private String lossDate;
    private String claimAmount;
    private int causeOfLossId;
    private String causeOfLoss;
    private String causeOfLossDetail;
    private int salvage;
    private String note;
    private String claimAdjustment;
    private int syncStatus;

    @Generated(hash = 1789666036)
    public FormData(Long id, String smsClaimNo, int categoryId, String riskLocation,
            String surveyDate, String lossDate, String claimAmount, int causeOfLossId,
            String causeOfLoss, String causeOfLossDetail, int salvage, String note,
            String claimAdjustment, int syncStatus) {
        this.id = id;
        this.smsClaimNo = smsClaimNo;
        this.categoryId = categoryId;
        this.riskLocation = riskLocation;
        this.surveyDate = surveyDate;
        this.lossDate = lossDate;
        this.claimAmount = claimAmount;
        this.causeOfLossId = causeOfLossId;
        this.causeOfLoss = causeOfLoss;
        this.causeOfLossDetail = causeOfLossDetail;
        this.salvage = salvage;
        this.note = note;
        this.claimAdjustment = claimAdjustment;
        this.syncStatus = syncStatus;
    }

    @Generated(hash = 1412441845)
    public FormData() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSmsClaimNo() {
        return this.smsClaimNo;
    }

    public void setSmsClaimNo(String smsClaimNo) {
        this.smsClaimNo = smsClaimNo;
    }

    public int getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getSurveyDate() {
        return this.surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getLossDate() {
        return this.lossDate;
    }

    public void setLossDate(String lossDate) {
        this.lossDate = lossDate;
    }

    public String getClaimAmount() {
        return this.claimAmount;
    }

    public void setClaimAmount(String claimAmount) {
        this.claimAmount = claimAmount;
    }

    public String getCauseOfLoss() {
        return this.causeOfLoss;
    }

    public void setCauseOfLoss(String causeOfLoss) {
        this.causeOfLoss = causeOfLoss;
    }

    public int getSalvage() {
        return this.salvage;
    }

    public void setSalvage(int salvage) {
        this.salvage = salvage;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getClaimAdjustment() {
        return this.claimAdjustment;
    }

    public void setClaimAdjustment(String claimAdjustment) {
        this.claimAdjustment = claimAdjustment;
    }

    public int getSyncStatus() {
        return this.syncStatus;
    }

    public void setSyncStatus(int syncStatus) {
        this.syncStatus = syncStatus;
    }

    public String getRiskLocation() {
        return this.riskLocation;
    }

    public void setRiskLocation(String riskLocation) {
        this.riskLocation = riskLocation;
    }

    public int getCauseOfLossId() {
        return this.causeOfLossId;
    }

    public void setCauseOfLossId(int causeOfLossId) {
        this.causeOfLossId = causeOfLossId;
    }

    public String getCauseOfLossDetail() {
        return this.causeOfLossDetail;
    }

    public void setCauseOfLossDetail(String causeOfLossDetail) {
        this.causeOfLossDetail = causeOfLossDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.smsClaimNo);
        dest.writeInt(this.categoryId);
        dest.writeString(this.riskLocation);
        dest.writeString(this.surveyDate);
        dest.writeString(this.lossDate);
        dest.writeString(this.claimAmount);
        dest.writeInt(this.causeOfLossId);
        dest.writeString(this.causeOfLoss);
        dest.writeString(this.causeOfLossDetail);
        dest.writeInt(this.salvage);
        dest.writeString(this.note);
        dest.writeString(this.claimAdjustment);
        dest.writeInt(this.syncStatus);
    }

    protected FormData(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.smsClaimNo = in.readString();
        this.categoryId = in.readInt();
        this.riskLocation = in.readString();
        this.surveyDate = in.readString();
        this.lossDate = in.readString();
        this.claimAmount = in.readString();
        this.causeOfLossId = in.readInt();
        this.causeOfLoss = in.readString();
        this.causeOfLossDetail = in.readString();
        this.salvage = in.readInt();
        this.note = in.readString();
        this.claimAdjustment = in.readString();
        this.syncStatus = in.readInt();
    }

    public static final Creator<FormData> CREATOR = new Creator<FormData>() {
        @Override
        public FormData createFromParcel(Parcel source) {
            return new FormData(source);
        }

        @Override
        public FormData[] newArray(int size) {
            return new FormData[size];
        }
    };
}
