package com.tryonics.ceylincononmotor.database.dao;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

@Entity(
        indexes = {
                @Index(value = "smsClaimNo,categoryId", unique = true)
        }
)
public class LocationData {

    @Id(autoincrement = true)
    @Index(unique = true)
    private Long id;
    private String smsClaimNo;
    private int categoryId;
    private double latitude;
    private double longitude;
    //private int locationType;
    private int syncStatus;
@Generated(hash = 86616839)
public LocationData(Long id, String smsClaimNo, int categoryId, double latitude,
        double longitude, int syncStatus) {
    this.id = id;
    this.smsClaimNo = smsClaimNo;
    this.categoryId = categoryId;
    this.latitude = latitude;
    this.longitude = longitude;
    this.syncStatus = syncStatus;
}
@Generated(hash = 1606831457)
public LocationData() {
}
public Long getId() {
    return this.id;
}
public void setId(Long id) {
    this.id = id;
}
public String getSmsClaimNo() {
    return this.smsClaimNo;
}
public void setSmsClaimNo(String smsClaimNo) {
    this.smsClaimNo = smsClaimNo;
}
public int getCategoryId() {
    return this.categoryId;
}
public void setCategoryId(int categoryId) {
    this.categoryId = categoryId;
}
public double getLatitude() {
    return this.latitude;
}
public void setLatitude(double latitude) {
    this.latitude = latitude;
}
public double getLongitude() {
    return this.longitude;
}
public void setLongitude(double longitude) {
    this.longitude = longitude;
}
public int getSyncStatus() {
    return this.syncStatus;
}
public void setSyncStatus(int syncStatus) {
    this.syncStatus = syncStatus;
}
    
}
