package com.tryonics.ceylincononmotor.database.dao;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

@Entity
public class ImageData implements Parcelable {

    public static final Parcelable.Creator<ImageData> CREATOR = new Parcelable.Creator<ImageData>() {
        @Override
        public ImageData createFromParcel(Parcel source) {
            return new ImageData(source);
        }

        @Override
        public ImageData[] newArray(int size) {
            return new ImageData[size];
        }
    };
    @Id(autoincrement = true)
    @Index(unique = true)
    public Long id;
    private String smsClaimNo;
    private int imageCategory;
    private String imageUrl;
    private int syncStatus;

    @Generated(hash = 1039587857)
    public ImageData(Long id, String smsClaimNo, int imageCategory, String imageUrl,
                     int syncStatus) {
        this.id = id;
        this.smsClaimNo = smsClaimNo;
        this.imageCategory = imageCategory;
        this.imageUrl = imageUrl;
        this.syncStatus = syncStatus;
    }

    @Generated(hash = 950102263)
    public ImageData() {
    }

    protected ImageData(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.smsClaimNo = in.readString();
        this.imageCategory = in.readInt();
        this.imageUrl = in.readString();
        this.syncStatus = in.readInt();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSmsClaimNo() {
        return this.smsClaimNo;
    }

    public void setSmsClaimNo(String smsClaimNo) {
        this.smsClaimNo = smsClaimNo;
    }

    public int getImageCategory() {
        return this.imageCategory;
    }

    public void setImageCategory(int imageCategory) {
        this.imageCategory = imageCategory;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getSyncStatus() {
        return this.syncStatus;
    }

    public void setSyncStatus(int syncStatus) {
        this.syncStatus = syncStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.smsClaimNo);
        dest.writeInt(this.imageCategory);
        dest.writeString(this.imageUrl);
        dest.writeInt(this.syncStatus);
    }
}
