package com.tryonics.ceylincononmotor.database.dao;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

@Entity(
        indexes = {
                @Index(value = "smsClaimNo", unique = true)
        }
)
public class JobData implements Parcelable {


    public static final Parcelable.Creator<JobData> CREATOR = new Parcelable.Creator<JobData>() {
        @Override
        public JobData createFromParcel(Parcel source) {
            return new JobData(source);
        }

        @Override
        public JobData[] newArray(int size) {
            return new JobData[size];
        }
    };
    @Id(autoincrement = true)
    public Long id;
    private String smsJobRefNo;
    private String smsClaimNo;
    private String smsJobType; //(OST - Claims and UWI - Underwriting)
    private String smsJobCreateDate;
    private long smsReceivedTime;
    private long jobStartTime;
    private long jobClosedTime;
    private long jobSyncDoneTime;
    private int currentJobStatus; //PENDING | IN-PROGRESS | CLOSED
    private int otsSyncStatus; //OTS UPLOAD
    private String jobId;
    private int jobSyncLevel; // INIT/IMAGES/FORM/LOCATION
    private int syncStatus;//COMPLETE REQUEST

    @Generated(hash = 1885331571)
    public JobData(Long id, String smsJobRefNo, String smsClaimNo,
                   String smsJobType, String smsJobCreateDate, long smsReceivedTime,
                   long jobStartTime, long jobClosedTime, long jobSyncDoneTime,
                   int currentJobStatus, int otsSyncStatus, String jobId, int jobSyncLevel,
                   int syncStatus) {
        this.id = id;
        this.smsJobRefNo = smsJobRefNo;
        this.smsClaimNo = smsClaimNo;
        this.smsJobType = smsJobType;
        this.smsJobCreateDate = smsJobCreateDate;
        this.smsReceivedTime = smsReceivedTime;
        this.jobStartTime = jobStartTime;
        this.jobClosedTime = jobClosedTime;
        this.jobSyncDoneTime = jobSyncDoneTime;
        this.currentJobStatus = currentJobStatus;
        this.otsSyncStatus = otsSyncStatus;
        this.jobId = jobId;
        this.jobSyncLevel = jobSyncLevel;
        this.syncStatus = syncStatus;
    }

    @Generated(hash = 1431362739)
    public JobData() {
    }

    protected JobData(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.smsJobRefNo = in.readString();
        this.smsClaimNo = in.readString();
        this.smsJobType = in.readString();
        this.smsJobCreateDate = in.readString();
        this.smsReceivedTime = in.readLong();
        this.jobStartTime = in.readLong();
        this.jobClosedTime = in.readLong();
        this.jobSyncDoneTime = in.readLong();
        this.currentJobStatus = in.readInt();
        this.otsSyncStatus = in.readInt();
        this.jobId = in.readString();
        this.jobSyncLevel = in.readInt();
        this.syncStatus = in.readInt();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSmsJobRefNo() {
        return this.smsJobRefNo;
    }

    public void setSmsJobRefNo(String smsJobRefNo) {
        this.smsJobRefNo = smsJobRefNo;
    }

    public String getSmsClaimNo() {
        return this.smsClaimNo;
    }

    public void setSmsClaimNo(String smsClaimNo) {
        this.smsClaimNo = smsClaimNo;
    }

    public String getSmsJobType() {
        return this.smsJobType;
    }

    public void setSmsJobType(String smsJobType) {
        this.smsJobType = smsJobType;
    }

    public String getSmsJobCreateDate() {
        return this.smsJobCreateDate;
    }

    public void setSmsJobCreateDate(String smsJobCreateDate) {
        this.smsJobCreateDate = smsJobCreateDate;
    }

    public long getSmsReceivedTime() {
        return this.smsReceivedTime;
    }

    public void setSmsReceivedTime(long smsReceivedTime) {
        this.smsReceivedTime = smsReceivedTime;
    }

    public long getJobStartTime() {
        return this.jobStartTime;
    }

    public void setJobStartTime(long jobStartTime) {
        this.jobStartTime = jobStartTime;
    }

    public long getJobClosedTime() {
        return this.jobClosedTime;
    }

    public void setJobClosedTime(long jobClosedTime) {
        this.jobClosedTime = jobClosedTime;
    }

    public long getJobSyncDoneTime() {
        return this.jobSyncDoneTime;
    }

    public void setJobSyncDoneTime(long jobSyncDoneTime) {
        this.jobSyncDoneTime = jobSyncDoneTime;
    }

    public int getCurrentJobStatus() {
        return this.currentJobStatus;
    }

    public void setCurrentJobStatus(int currentJobStatus) {
        this.currentJobStatus = currentJobStatus;
    }

    public int getOtsSyncStatus() {
        return this.otsSyncStatus;
    }

    public void setOtsSyncStatus(int otsSyncStatus) {
        this.otsSyncStatus = otsSyncStatus;
    }

    public String getJobId() {
        return this.jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public int getJobSyncLevel() {
        return this.jobSyncLevel;
    }

    public void setJobSyncLevel(int jobSyncLevel) {
        this.jobSyncLevel = jobSyncLevel;
    }

    public int getSyncStatus() {
        return this.syncStatus;
    }

    public void setSyncStatus(int syncStatus) {
        this.syncStatus = syncStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.smsJobRefNo);
        dest.writeString(this.smsClaimNo);
        dest.writeString(this.smsJobType);
        dest.writeString(this.smsJobCreateDate);
        dest.writeLong(this.smsReceivedTime);
        dest.writeLong(this.jobStartTime);
        dest.writeLong(this.jobClosedTime);
        dest.writeLong(this.jobSyncDoneTime);
        dest.writeInt(this.currentJobStatus);
        dest.writeInt(this.otsSyncStatus);
        dest.writeString(this.jobId);
        dest.writeInt(this.jobSyncLevel);
        dest.writeInt(this.syncStatus);
    }
}
