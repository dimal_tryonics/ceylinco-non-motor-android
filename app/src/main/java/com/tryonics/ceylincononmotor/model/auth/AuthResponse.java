package com.tryonics.ceylincononmotor.model.auth;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tryonics.ceylincononmotor.model.Error;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthResponse implements Parcelable {

    @JsonProperty("error")
    public Error error;

    @JsonProperty("access_token")
    public String accessToken;

    @JsonProperty("token_type")
    public String tokenType;

    public AuthResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.error, flags);
        dest.writeString(this.accessToken);
        dest.writeString(this.tokenType);
    }

    protected AuthResponse(Parcel in) {
        this.error = in.readParcelable(Error.class.getClassLoader());
        this.accessToken = in.readString();
        this.tokenType = in.readString();
    }

    public static final Creator<AuthResponse> CREATOR = new Creator<AuthResponse>() {
        @Override
        public AuthResponse createFromParcel(Parcel source) {
            return new AuthResponse(source);
        }

        @Override
        public AuthResponse[] newArray(int size) {
            return new AuthResponse[size];
        }
    };
}
