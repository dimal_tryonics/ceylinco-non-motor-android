package com.tryonics.ceylincononmotor.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Success implements Parcelable {

    public static final Creator<Success> CREATOR = new Creator<Success>() {
        @Override
        public Success createFromParcel(Parcel source) {
            return new Success(source);
        }

        @Override
        public Success[] newArray(int size) {
            return new Success[size];
        }
    };
    @JsonProperty("code")
    public String code;
    @JsonProperty("message")
    public String message;
    @JsonProperty("target")
    public String target;

    public Success() {
    }

    protected Success(Parcel in) {
        this.code = in.readString();
        this.message = in.readString();
        this.target = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.message);
        dest.writeString(this.target);
    }
}
