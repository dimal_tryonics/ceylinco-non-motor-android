package com.tryonics.ceylincononmotor.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImageUploadCompleteResponse implements Parcelable {

    @JsonProperty("error")
    public Error error;

    @JsonProperty("success")
    public Success success;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.error, flags);
        dest.writeParcelable(this.success, flags);
    }

    public ImageUploadCompleteResponse() {
    }

    protected ImageUploadCompleteResponse(Parcel in) {
        this.error = in.readParcelable(Error.class.getClassLoader());
        this.success = in.readParcelable(Success.class.getClassLoader());
    }

    public static final Creator<ImageUploadCompleteResponse> CREATOR = new Creator<ImageUploadCompleteResponse>() {
        @Override
        public ImageUploadCompleteResponse createFromParcel(Parcel source) {
            return new ImageUploadCompleteResponse(source);
        }

        @Override
        public ImageUploadCompleteResponse[] newArray(int size) {
            return new ImageUploadCompleteResponse[size];
        }
    };
}
