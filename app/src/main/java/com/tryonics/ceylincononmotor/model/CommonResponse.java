package com.tryonics.ceylincononmotor.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CommonResponse implements Parcelable {

    public static final Creator<CommonResponse> CREATOR = new Creator<CommonResponse>() {
        @Override
        public CommonResponse createFromParcel(Parcel source) {
            return new CommonResponse(source);
        }

        @Override
        public CommonResponse[] newArray(int size) {
            return new CommonResponse[size];
        }
    };
    @JsonProperty("error")
    public Error error;
    @JsonProperty("success")
    public Success success;

    public CommonResponse() {
    }

    protected CommonResponse(Parcel in) {
        this.error = in.readParcelable(Error.class.getClassLoader());
        this.success = in.readParcelable(Success.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.error, flags);
        dest.writeParcelable(this.success, flags);
    }
}
