package com.tryonics.ceylincononmotor.model.option;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageCategory implements Parcelable {

    public int categoryId;
    public int categoryImage;
    public String categoryName;
    public boolean completeStatus;


    public ImageCategory() {
    }

    public ImageCategory(int categoryId, int categoryImage, String categoryName, boolean completeStatus) {
        this.categoryId = categoryId;
        this.categoryImage = categoryImage;
        this.categoryName = categoryName;
        this.completeStatus = completeStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.categoryId);
        dest.writeInt(this.categoryImage);
        dest.writeString(this.categoryName);
        dest.writeByte(this.completeStatus ? (byte) 1 : (byte) 0);
    }

    protected ImageCategory(Parcel in) {
        this.categoryId = in.readInt();
        this.categoryImage = in.readInt();
        this.categoryName = in.readString();
        this.completeStatus = in.readByte() != 0;
    }

    public static final Creator<ImageCategory> CREATOR = new Creator<ImageCategory>() {
        @Override
        public ImageCategory createFromParcel(Parcel source) {
            return new ImageCategory(source);
        }

        @Override
        public ImageCategory[] newArray(int size) {
            return new ImageCategory[size];
        }
    };
}