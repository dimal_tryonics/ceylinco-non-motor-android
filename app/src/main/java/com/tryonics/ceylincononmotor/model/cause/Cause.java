package com.tryonics.ceylincononmotor.model.cause;

import android.os.Parcel;
import android.os.Parcelable;


public class Cause implements Parcelable {

    public int id;
    public String cause;
    public String causeDetail;


    public Cause() {
    }

    public Cause(int id, String cause) {
        this.id = id;
        this.cause = cause;
    }

    public Cause(int id, String cause, String causeDetail) {
        this.id = id;
        this.cause = cause;
        this.causeDetail = causeDetail;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.cause);
        dest.writeString(this.causeDetail);
    }

    protected Cause(Parcel in) {
        this.id = in.readInt();
        this.cause = in.readString();
        this.causeDetail = in.readString();
    }

    public static final Creator<Cause> CREATOR = new Creator<Cause>() {
        @Override
        public Cause createFromParcel(Parcel source) {
            return new Cause(source);
        }

        @Override
        public Cause[] newArray(int size) {
            return new Cause[size];
        }
    };
}
