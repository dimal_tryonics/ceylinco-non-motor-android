package com.tryonics.ceylincononmotor.model.jobinit;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tryonics.ceylincononmotor.model.Error;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JobInitResponse implements Parcelable {

    @JsonProperty("error")
    public Error error;


    @JsonProperty("job_id")
    public String jobId;

    public JobInitResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.error, flags);
        dest.writeString(this.jobId);
    }

    protected JobInitResponse(Parcel in) {
        this.error = in.readParcelable(Error.class.getClassLoader());
        this.jobId = in.readString();
    }

    public static final Creator<JobInitResponse> CREATOR = new Creator<JobInitResponse>() {
        @Override
        public JobInitResponse createFromParcel(Parcel source) {
            return new JobInitResponse(source);
        }

        @Override
        public JobInitResponse[] newArray(int size) {
            return new JobInitResponse[size];
        }
    };
}
