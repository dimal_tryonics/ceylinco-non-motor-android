package com.tryonics.ceylincononmotor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;


import com.tryonics.ceylincononmotor.apimanager.ApiInterface;
import com.tryonics.ceylincononmotor.apimanager.RxRestEngine;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.databinding.ActivityLoginBinding;
import com.tryonics.ceylincononmotor.model.auth.AuthResponse;
import com.tryonics.ceylincononmotor.util.AppPreferences;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.BaseActivity;
import com.tryonics.ceylincononmotor.util.DeviceInfo;
import com.tryonics.ceylincononmotor.util.NetworkConnection;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;


public class LoginActivity extends BaseActivity {

    private static final String TAG = LoginActivity.class.getName();

    private ActivityLoginBinding binding;

    private AppPreferences appPreferences;
    private ApiInterface restEngine;
    private DaoSession daoSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.progressBar.setVisibility(View.GONE);

        appPreferences = new AppPreferences(getApplicationContext());
        restEngine = RxRestEngine.getInstance().getService(getApplicationContext());
        daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();

        binding.btnLogin.setOnClickListener(view1 -> login(binding.etUsername.getText().toString().trim(), binding.etPassword.getText().toString().trim()));
    }

    private void login(String userID, String password){

        if (!TextUtils.isEmpty(userID) && !TextUtils.isEmpty(password)) {

            NetworkConnection networkConnection = new NetworkConnection(getApplicationContext());
            networkConnection.setListener(new NetworkConnection.NetworkConnectionStatus() {
                @Override
                public void connectionSuccessful() {
                    Log.e(TAG, "connectionSuccessful: " );
                    binding.progressBar.setVisibility(View.VISIBLE);
                    binding.btnLogin.setEnabled(false);
                    userValidation(userID, password);
                }

                @Override
                public void connectionFail() {
                    showInfoDialog("Error","Please check your internet connection and try again");
                }
            });

        } else {
            showInfoDialog("Error","Invalid username or password");

        }
    }


    private void userValidation(String userID, String password) {

        Log.e(TAG, "userID: " + userID );
        Log.e(TAG, "password: " + password );

        RequestBody rUserID = RequestBody.create(userID, MediaType.parse("text/plain"));
        RequestBody rPassword = RequestBody.create(password, MediaType.parse("text/plain"));
        RequestBody rDeviceDes = RequestBody.create(DeviceInfo.getDeviceData(), MediaType.parse("text/plain"));

        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        requestBodyMap.put("user_id", rUserID);
        requestBodyMap.put("password", rPassword);
        requestBodyMap.put("device_description", rDeviceDes);

        restEngine.requestLogin(requestBodyMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Response<AuthResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();

                    }

                    @Override
                    public void onSuccess(Response<AuthResponse> response) {

                        binding.btnLogin.setEnabled(true);
                        binding.progressBar.setVisibility(View.GONE);

                        if (response != null) {
                            int code = response.code();

                            if (code == 200) {
                                AuthResponse authResponse = (AuthResponse) response.body();

                                if (authResponse.error != null) {
                                    showInfoDialog(authResponse.error.code, authResponse.error.message);
                                } else {
                                    requestSuccess(authResponse);
                                }
                            }else if(code == 403){
                                showInfoDialog("Inactive User", "Inactive user. Please contact system administrator " + " Error code: " + code);
                            } else {
                                showInfoDialog("Error code: " + code, "An unknown error occurred");
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        binding.btnLogin.setEnabled(true);
                        binding.progressBar.setVisibility(View.GONE);
                        Log.e(TAG, "onError: " + e.getMessage() );
                        showInfoDialog("Error" , "An unknown error occurred - " + e.getMessage());
                    }
                });

    }

    private void requestSuccess(AuthResponse authResponse){
        appPreferences.setAuthorization(authResponse.tokenType + " " + authResponse.accessToken);
        //dataSource.insertGlobalSettings(authResponse);

        Intent intent = new Intent(LoginActivity.this, ViewPagerActivity.class);
        startActivity(intent);
        finish();
    }

    /*private void requestSuccess(AuthResponse authResponse){
        appPreferences.setAuthToken(authResponse.tokenType + " " + authResponse.accessToken);
        dataSource.insertGlobalSettings(authResponse);

        Intent intent = new Intent(LoginActivity.this, DrawerMainActivity.class);
        startActivity(intent);
        finish();
    }*/
}