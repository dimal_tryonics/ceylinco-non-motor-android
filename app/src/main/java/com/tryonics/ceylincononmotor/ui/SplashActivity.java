package com.tryonics.ceylincononmotor.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tryonics.ceylincononmotor.databinding.ActivitySplashBinding;
import com.tryonics.ceylincononmotor.util.AppPreferences;

import java.util.List;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 2000;
    private final int SPLASH_DISPLAY_INTERVAL = 1000;
    private AppPreferences appPreferences;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySplashBinding binding = ActivitySplashBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        appPreferences = new AppPreferences(getApplicationContext());

        checkPermissionStatus();
    }

    private void checkPermissionStatus() {

        Dexter.withContext(SplashActivity.this)
                .withPermissions(RECEIVE_SMS, READ_SMS, CAMERA, READ_PHONE_STATE, WRITE_EXTERNAL_STORAGE)

                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            startApplication();
                        } else {
                            checkPermissionStatus();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .withErrorListener(error -> Toast.makeText(getApplicationContext(), "Permission error occurred", Toast.LENGTH_LONG).show())
                .check();
    }

    private void startApplication() {

        countDownTimer = new CountDownTimer(SPLASH_DISPLAY_LENGTH, SPLASH_DISPLAY_INTERVAL) {

            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {

                Intent intent;

                if (appPreferences.isAuthorized()) {
                    intent = new Intent(SplashActivity.this, ViewPagerActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                }

                startActivity(intent);

                finish();
            }

        }.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

}
