package com.tryonics.ceylincononmotor.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tryonics.ceylincononmotor.BuildConfig;
import com.tryonics.ceylincononmotor.adapter.PhotoAdapter;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.ImageData;
import com.tryonics.ceylincononmotor.database.dao.ImageDataDao;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.databinding.ActivityPhotoBinding;
import com.tryonics.ceylincononmotor.model.option.ImageCategory;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.BitmapDecorder;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.file.TryonicsFileManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class PhotoActivity extends AppCompatActivity implements PhotoAdapter.ItemClickListener, View.OnClickListener {

    private static final String TAG = PhotoActivity.class.getName();


    private ActivityPhotoBinding binding;

    private JobData jobData;
    private ImageCategory imageCategory;

    private ImageDataDao imageDataDao;
    private List<ImageData> imageDataListForDelete = new LinkedList<>();

    private PhotoAdapter photoAdapter;
    private File captureImageFile;

    private List<File> listPhotoFiles;
    private Snackbar snackbar;

    //RxJava Observable patten implementation
    final Observable<File> singleCompressor = Observable.create(emitter -> {
        if (captureImageFile.exists()) {
            Bitmap bitmap = BitmapDecorder.getScaledBitmapFromFile(captureImageFile.getAbsolutePath(), getImageQualitySetting());
            File newFile = null;
            FileOutputStream out;
            try {
                //creates a new file with the name formatted with vehicleNumber
                newFile = TryonicsFileManager.createFolderWithVehicleNumber(jobData);
                if (newFile != null) {
                    out = new FileOutputStream(newFile.getAbsolutePath());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, out);

                    //Release bitmap resource
                    bitmap.recycle();
                    bitmap = null;

                    //Release outputstream
                    out.flush();
                    out.close();
                    out = null;
                }
                //Notify Observer about the jobcompletion with the fileName
                emitter.onNext(newFile);
                //This will trigger the observers onCompleted event
                emitter.onComplete();

            } catch (IOException e) {
                showToastMessage("Error " + e.getMessage());
                emitter.onError(e);
            }
        } else {
            showToastMessage("Capture image not found");
        }
    });

    //RxJava Observable patten implementation
    final Observable<File> multiFileCompressor = Observable.create( emitter -> {

        for(File file : listPhotoFiles){
            if(file.exists()){
                Bitmap bitmap = BitmapDecorder.getScaledBitmapFromFile(file.getAbsolutePath(), getImageQualitySetting());
                File newFile = null;
                FileOutputStream out;
                try {
                    //creates a new file with the name formatted with vehicleNumber
                    newFile = TryonicsFileManager.createFolderWithVehicleNumber(jobData);

                    if (newFile != null) {
                        out = new FileOutputStream(newFile.getAbsolutePath());
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, out);

                        //Release bitmap resource
                        bitmap.recycle();
                        bitmap = null;

                        //Release outputstream
                        out.flush();
                        out.close();
                        out = null;
                    }
                    //Notify Observer about the jobcompletion with the fileName
                    emitter.onNext(newFile);

                } catch (IOException e) {
                    showToastMessage("Error " + e.getMessage());
                    emitter.onError(e);
                }

            }else {
                showToastMessage("Selected image not found");
            }
        }
        //This will trigger the observers onCompleted event
        emitter.onComplete();
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPhotoBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        getBundleData();

        setupRecyclerViewLayout();

        binding.tvAddPhoto.setOnClickListener(this);
        binding.tvGallery.setOnClickListener(this);
        binding.tvDelete.setOnClickListener(this);

        changeDeleteButtonVisibility(View.GONE);
    }

    private void getBundleData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobData = extras.getParcelable(Constants.EXTRA_JOB_DATA);
            imageCategory = extras.getParcelable(Constants.EXTRA_JOB_CATEGORY);
        }
    }

    private void setupRecyclerViewLayout() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        binding.rvPhotoGrid.setLayoutManager(gridLayoutManager);
        photoAdapter = new PhotoAdapter(this);

        selectAllImage();
    }

    private void selectAllImage() {
        DaoSession daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();

        imageDataDao = daoSession.getImageDataDao();

        imageDataDao.queryBuilder().LOG_VALUES = true;
        imageDataDao.queryBuilder().LOG_SQL = true;

        List<ImageData> imageDataList = imageDataDao.queryBuilder().where(ImageDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), ImageDataDao.Properties.ImageCategory.eq(imageCategory.categoryId)).list();

        //SET EMPTY LIST //if (imageDataList.size() > 0) {
        photoAdapter.addImageDataList(imageDataList);
        binding.rvPhotoGrid.setAdapter(photoAdapter);
        photoAdapter.setClickListener(this);
        //}
    }

    @Override
    public void onImageItemClick(View view, int position, ImageData imageData) {
        if (photoAdapter.isActivePhotoRemove()) {
            imageDataListForDelete.add(imageData);
        } else {
            Intent intent = new Intent(getApplicationContext(), ImageDetailActivity.class);
            intent.putExtra(Constants.EXTRA_IMAGE_PATH, imageData.getImageUrl());
            startActivity(intent);
        }
    }

    @Override
    public void onImageItemLongClick(View view, int position, ImageData ImageData) {
        photoAdapter.setActivePhotoRemove(true);
        changeDeleteButtonVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (photoAdapter.isActivePhotoRemove()) {
            Log.e(TAG, "DELETE VIEW VISIBLE");
            changeDeleteButtonVisibility(View.GONE);
            photoAdapter.setActivePhotoRemove(false);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == binding.tvAddPhoto.getId()) {
            checkPermission();
        }

        if(v.getId() == binding.tvGallery.getId()){
            ImagePicker.create(this)
                    .toolbarImageTitle("Gallery - Tap to select") // image selection title
                    .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                    .includeVideo(false) // Show video on image picker
                    .enableLog(true)
                    .folderMode(true)
                    .showCamera(false)
                    .multi() // single mode
                    .limit(10) // max images can be selected (99 by default)
                    .start(); // start image picker activity with request code
        }

        if (v.getId() == binding.tvDelete.getId()) {

            if (photoAdapter.isActivePhotoRemove()) {
                deleteCapturedImage();
            } else {
                finish();
            }
        }
    }

    private void checkPermission() {

        Dexter.withContext(PhotoActivity.this)
                .withPermissions(CAMERA, WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        /*if (report.areAllPermissionsGranted()) {
                            takePicture();
                        } else {
                            showToastMessage("Error occurred! Some permissions are not granted try again");
                        }*/

                        if (report.areAllPermissionsGranted()) {

                            if (getCameraSetting()) {
                                takePicture();
                            } else {
                                Intent intent = new Intent(PhotoActivity.this, CameraViewActivity.class);
                                intent.putExtra(Constants.EXTRA_JOB_CATEGORY, imageCategory);
                                intent.putExtra(Constants.EXTRA_JOB_DATA, jobData);
                                startActivityForResult(intent, Constants.REQUEST_CUSTOM_CAMERA);
                            }
                        } else {
                            showToastMessage("Error occurred! Some permissions are not granted try again");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).onSameThread()
                .check();

    }

    public void takePicture() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            captureImageFile = TryonicsFileManager.createLocalImageFile(getApplicationContext());

            if (captureImageFile != null) {

                Uri photoURI = FileProvider.getUriForFile(this, "com.tryonics.ceylincononmotor", captureImageFile);
                getApplicationContext().grantUriPermission("com.android.camera", photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Constants.REQUEST_IMAGE_CAPTURE);

                Log.e(TAG, " TAG 1 - Path " + photoURI.getPath());

            } else {
                Toast.makeText(this, "Sorry, Image file can not be create", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                compressSingleListener();
            }
        }

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            List<Image> images = ImagePicker.getImages(data);

            listPhotoFiles = new ArrayList<>();

            for (int i = 0; i < images.size(); i++) {
                listPhotoFiles.add(new File(images.get(i).getPath()));
            }
            //Send images for compression
            compressMultipleListener();
        }

        if(requestCode == Constants.REQUEST_CUSTOM_CAMERA){
            Log.e(TAG, "onActivityResult: 1" );
            if (resultCode == RESULT_OK) {
                Log.e(TAG, "onActivityResult: 2" );
                setupRecyclerViewLayout();
            }
        }
    }

    @SuppressLint("AutoDispose")
    private void compressSingleListener() {
        /*
            Start image compression on out of the mainThrea and get result passed back to the main thread.
            We have used RxJava Observable design patten here to get notified compressionCompletion  asyncronously.
         */
        singleCompressor.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<File>() {


                    @Override
                    public void onSubscribe(Disposable d) {
                        showImageOptimizer();
                    }

                    @Override
                    public void onNext(File file) {
                        //Compression done & it's notified with the new compressed fileName.
                        //Now its safe to delete the original file
                        Log.e(TAG, "COMPRESSED FILE " + file.getAbsolutePath());
                        boolean flag = captureImageFile.delete();
                        Log.e(TAG," ORIGINAL FILE DELETED " + flag + " " + captureImageFile.getAbsolutePath());
                        saveCapturedImage(file);
                        //notifySystemGallery(file.getPath());
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideImageOptimizer();
                    }

                    @Override
                    public void onComplete() {
                        hideImageOptimizer();
                    }
                });
    }

    @SuppressLint("AutoDispose")
    private void compressMultipleListener() {
        multiFileCompressor.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<File>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        showImageOptimizer();
                    }

                    @Override
                    public void onNext(File file) {
                        Log.e(TAG, "COMPRESSED FILE " + file.getAbsolutePath());
                        saveCapturedImage(file);
                        //notifySystemGallery(file.getPath());
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideImageOptimizer();
                    }

                    @Override
                    public void onComplete() {
                        hideImageOptimizer();
                    }
                });
    }


    ///////

    private boolean getCameraSetting() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        return sharedPreferences.getBoolean("camera_setting", false);
    }

    private boolean getImageQualitySetting() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        return sharedPreferences.getBoolean("high_quality_images", false);
    }

    /////


    private void saveCapturedImage(File file) {
        if (file == null) {
            return;
        }

        ImageData imageData = new ImageData(null, jobData.getSmsClaimNo(), imageCategory.categoryId, file.getAbsolutePath(), Constants.SYNC_PENDING);
        imageDataDao.insert(imageData);
        Log.e(TAG, "IMAGE SAVED SUCCESSFULLY");

        addCapturedImageToList(imageData);
    }

    private void addCapturedImageToList(ImageData imageData) {
        photoAdapter.addImageData(imageData);
    }

    private void deleteCapturedImage() {
        if (imageDataListForDelete.isEmpty()) {
            return;
        }

        for (ImageData imageData : imageDataListForDelete) {
            Log.e(TAG, "DELETE " + imageData.getImageUrl());
            imageDataDao.delete(imageData);
        }

        changeDeleteButtonVisibility(View.GONE);
        photoAdapter.setActivePhotoRemove(false);
        selectAllImage();


    }

    private void changeDeleteButtonVisibility(int visibility) {
        binding.tvDelete.setVisibility(visibility);
    }


    private void showImageOptimizer() {
        snackbar = Snackbar
                .make(binding.rootConstraintLayout, "Optimizing image. Please wait!", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void hideImageOptimizer() {
        if (snackbar != null && snackbar.isShown())
            snackbar.dismiss();
    }

    public void showToastMessage(final String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

}
