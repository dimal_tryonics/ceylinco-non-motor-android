package com.tryonics.ceylincononmotor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.adapter.CategoryAdapter;
import com.tryonics.ceylincononmotor.apimanager.ApiInterface;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.FormDataDao;
import com.tryonics.ceylincononmotor.database.dao.ImageDataDao;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.databinding.ActivitySubNavigationBinding;
import com.tryonics.ceylincononmotor.model.option.ImageCategory;
import com.tryonics.ceylincononmotor.tryonicsevents.RefreshInprogressJobEvent;
import com.tryonics.ceylincononmotor.ui.dialog.DialogOTSUpload;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class SubNavigationActivity extends AppCompatActivity implements CategoryAdapter.ItemClickListener, View.OnClickListener {

    private static final String TAG = SubNavigationActivity.class.getName();

    private ActivitySubNavigationBinding binding;

    private DaoSession daoSession;

    private ApiInterface restEngine;

    private JobData jobData;
    private ImageCategory imageCategory;

    private ArrayList<ImageCategory> imageCategoryArrayList;
    private CategoryAdapter categoryAdapter;

    private RecyclerView rvOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySubNavigationBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        getBundleData();

        rvOptions = binding.rvOptions;

        setupRecyclerViewLayout();

        validateCategoryList();

        binding.tvUpload.setOnClickListener(this);
    }

    private void getBundleData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobData = extras.getParcelable(Constants.EXTRA_JOB_DATA);
            imageCategory = extras.getParcelable(Constants.EXTRA_JOB_CATEGORY);
        }

        checkJobType();
    }

    private void checkJobType() {
        if (imageCategory.categoryId == Constants.CATEGORY_REPORT_OTS) {
            binding.tvUpload.setVisibility(View.VISIBLE);
        } else {
            binding.tvUpload.setVisibility(View.GONE);
        }

    }

    private void setupRecyclerViewLayout() {
        LinearLayoutManager llmTextTools = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        rvOptions.setLayoutManager(llmTextTools);
        categoryAdapter = new CategoryAdapter(this);

        createOptionList();
    }

    private void createOptionList() {

        imageCategoryArrayList = new ArrayList<>();

        //GET SELECTED CATEGORY BY MAIN SELECTION
        ImageCategory imageCategory1 = new ImageCategory(Constants.CATEGORY_REPORT_OTS, R.drawable.ic_camera_24dp, "Scan OTS Documents", false);
        ImageCategory imageCategory2 = new ImageCategory(Constants.CATEGORY_REPORT_OTS_FORM, R.drawable.ic_report_24dp, "Form Data OTS", false);

        ImageCategory imageCategory3 = new ImageCategory(Constants.CATEGORY_REPORT_FLA, R.drawable.ic_camera_24dp, "Scan FLA Documents", false);
        ImageCategory imageCategory4 = new ImageCategory(Constants.CATEGORY_REPORT_FLA_FORM, R.drawable.ic_report_24dp, "Form Data FLA", false);

        if (imageCategory.categoryId == Constants.CATEGORY_REPORT_OTS) {
            imageCategoryArrayList.add(imageCategory1);
            imageCategoryArrayList.add(imageCategory2);
        }

        if (imageCategory.categoryId == Constants.CATEGORY_REPORT_FLA) {
            imageCategoryArrayList.add(imageCategory3);
            imageCategoryArrayList.add(imageCategory4);
        }

        categoryAdapter.addJobDataList(jobData, imageCategoryArrayList);

        rvOptions.setAdapter(categoryAdapter);
        categoryAdapter.setClickListener(this);

    }

    private void validateCategoryList() {

        if (imageCategoryArrayList == null || imageCategoryArrayList.isEmpty()) {
            return;
        }

        daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();

        //CLAIM VALIDATION
        for (int i = 0; i < imageCategoryArrayList.size(); i++) {

            ImageCategory imageCategory = imageCategoryArrayList.get(i);

            //TAKE PHOTO
            ImageDataDao imageDataDao = daoSession.getImageDataDao();
            long imageCount = imageDataDao.queryBuilder().where(ImageDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), ImageDataDao.Properties.ImageCategory.eq(imageCategory.categoryId)).count();

            if (imageCategory.categoryId == Constants.CATEGORY_REPORT_OTS) {
                if (imageCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                } else {
                    imageCategoryArrayList.get(i).completeStatus = false;
                }
            }

            if (imageCategory.categoryId == Constants.CATEGORY_REPORT_FLA) {
                if (imageCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                } else {
                    imageCategoryArrayList.get(i).completeStatus = false;
                }
            }

            if (imageCategory.categoryId == Constants.CATEGORY_REPORT_OTS_FORM) {

                FormDataDao formData = daoSession.getFormDataDao();
                long otsFormCount = formData.queryBuilder().where(FormDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), FormDataDao.Properties.CategoryId.eq(Constants.CATEGORY_REPORT_OTS_FORM)).count();

                if (imageCount > 0 || otsFormCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                } else {
                    imageCategoryArrayList.get(i).completeStatus = false;
                }

            }

            if (imageCategory.categoryId == Constants.CATEGORY_REPORT_FLA_FORM) {
                FormDataDao formData = daoSession.getFormDataDao();
                long flaFormCount = formData.queryBuilder().where(FormDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), FormDataDao.Properties.CategoryId.eq(Constants.CATEGORY_REPORT_FLA_FORM)).count();

                if (imageCount > 0 || flaFormCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                } else {
                    imageCategoryArrayList.get(i).completeStatus = false;
                }
            }

        }

        categoryAdapter.notifyDataSetChanged();

    }

    @Override
    public void onOptionItemClick(View view, int position, ImageCategory imageCategory) {

        Intent intent;

        if (imageCategory.categoryId == Constants.CATEGORY_REPORT_OTS || imageCategory.categoryId == Constants.CATEGORY_REPORT_FLA) {
            intent = new Intent(getApplicationContext(), PhotoActivity.class);
            intent.putExtra(Constants.EXTRA_JOB_DATA, jobData);
            intent.putExtra(Constants.EXTRA_JOB_CATEGORY, imageCategory);
            startActivity(intent);
        } else if (imageCategory.categoryId == Constants.CATEGORY_REPORT_OTS_FORM || imageCategory.categoryId == Constants.CATEGORY_REPORT_FLA_FORM) {
            intent = new Intent(getApplicationContext(), FormActivity.class);
            intent.putExtra(Constants.EXTRA_JOB_DATA, jobData);
            intent.putExtra(Constants.EXTRA_JOB_CATEGORY, imageCategory);
            startActivity(intent);
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvUpload:

                if (validateDataCapture()) {
                    showUploadConfirmationDialog();
                } else {
                    showSnackbar("SORRY: Completion of the Upload Photo or Filling the from is required to continue the proceed");
                }

                break;
        }
    }

    private boolean validateDataCapture() {

        boolean completeStatus = false;

        for (ImageCategory image : imageCategoryArrayList) {
            if (image.completeStatus) {
                completeStatus = true;
            }
        }

        return completeStatus;
    }

    private void showUploadConfirmationDialog() {

        DialogOTSUpload dialogOTSUpload = new DialogOTSUpload().show(SubNavigationActivity.this, jobData, imageCategory);
        dialogOTSUpload.setClickListener(() -> {
            EventBus.getDefault().postSticky(new RefreshInprogressJobEvent());
            finish();
        });
    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar
                .make(binding.root, message, Snackbar.LENGTH_LONG);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.snack_bar_bg));
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        validateCategoryList();
    }


}
