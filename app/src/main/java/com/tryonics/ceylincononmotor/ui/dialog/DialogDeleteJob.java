package com.tryonics.ceylincononmotor.ui.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.database.dao.JobData;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogDeleteJob extends DialogFragment {

    public static final String EXTRA_JOB = "extra_JOB";
    private static final String TAG = DialogDeleteJob.class.getSimpleName();
    private JobStart clickListener;

    public DialogDeleteJob() {
        // Required empty public constructor
    }

    //Show dialog with provide text and text color
    public static DialogDeleteJob show(@NonNull AppCompatActivity appCompatActivity, JobData jobData) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_JOB, jobData);
        DialogDeleteJob fragment = new DialogDeleteJob();
        fragment.setArguments(args);
        fragment.show(appCompatActivity.getSupportFragmentManager(), TAG);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        //Make dialog full screen with transparent background
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dialog_delete_job, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        JobData jobData = getArguments().getParcelable(EXTRA_JOB);

        TextView tvMessage = view.findViewById(R.id.tvMessage);
        tvMessage.setText("Are you sure you want to delete " + jobData.getSmsClaimNo() + "?");

        Button btnClose = view.findViewById(R.id.btnCancel);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onClose(DialogDeleteJob.this, jobData);
            }
        });

        Button btnStart = view.findViewById(R.id.btnYes);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onDelete(DialogDeleteJob.this, jobData);
            }
        });

    }

    //Callback to listener if user is done with text editing
    public void setClickListener(JobStart clickListener) {
        this.clickListener = clickListener;
    }

    public interface JobStart {
        void onClose(DialogDeleteJob dialog, JobData jobData);
        void onDelete(DialogDeleteJob dialog, JobData jobData);
    }
}
