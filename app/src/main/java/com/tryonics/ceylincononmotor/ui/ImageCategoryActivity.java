package com.tryonics.ceylincononmotor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.adapter.CategoryAdapter;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.FormDataDao;
import com.tryonics.ceylincononmotor.database.dao.ImageDataDao;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.database.dao.JobDataDao;
import com.tryonics.ceylincononmotor.database.dao.LocationDataDao;
import com.tryonics.ceylincononmotor.databinding.ActivityOptionsBinding;
import com.tryonics.ceylincononmotor.model.option.ImageCategory;
import com.tryonics.ceylincononmotor.service.UploadService;
import com.tryonics.ceylincononmotor.tryonicsevents.InprogressJobEvent;
import com.tryonics.ceylincononmotor.tryonicsevents.RemoveInprogressJobEvent;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.DateTimeModule;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class ImageCategoryActivity extends AppCompatActivity implements CategoryAdapter.ItemClickListener, View.OnClickListener {

    private static final String TAG = ImageCategoryActivity.class.getName();

    private ActivityOptionsBinding binding;

    private String claimNo;

    private CategoryAdapter categoryAdapter;
    private DaoSession daoSession;

    private JobData jobData;
    private ArrayList<ImageCategory> imageCategoryArrayList;

    private boolean flaReportCompleted = false;
    private boolean underwritingPhotoCompleted = false;
    private boolean underwritingReportCompleted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOptionsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        getBundleData();


        binding.tvUploadComplete.setOnClickListener(this);

    }

    private void getBundleData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            JobData jobData = extras.getParcelable(Constants.EXTRA_JOB_DATA);
            claimNo = jobData.getSmsClaimNo();

            getUpdatedJobData();

        }
    }

    private void getUpdatedJobData() {
        daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();

        JobDataDao jobDataDao = daoSession.getJobDataDao();
        jobData = jobDataDao.queryBuilder().where(JobDataDao.Properties.SmsClaimNo.eq(claimNo)).unique();

        setupRecyclerViewLayout();
        validateCategoryList();

    }

    private void setupRecyclerViewLayout() {
        LinearLayoutManager llmTextTools = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        binding.rvOptions.setLayoutManager(llmTextTools);
        categoryAdapter = new CategoryAdapter(this);

        createOptionList();
    }

    private void createOptionList() {

        imageCategoryArrayList = new ArrayList<>();

        if (jobData.getSmsJobType().contains(Constants.CLAIM)) {

            ImageCategory imageCategory1 = new ImageCategory(Constants.CATEGORY_PHOTOS, R.drawable.ic_camera_24dp, "Take Photos", false);
            //ImageCategory imageCategory2 = new ImageCategory(Constants.CATEGORY_SCAN_DOCUMENT, R.drawable.ic_scan_24dp, "Scan Documents", false);
            ImageCategory imageCategory3 = new ImageCategory(Constants.CATEGORY_REPORT_OTS, R.drawable.ic_report_24dp, "OTS Report", false);
            ImageCategory imageCategory4 = new ImageCategory(Constants.CATEGORY_REPORT_FLA, R.drawable.ic_report_24dp, "FLA Report *", false);
            ImageCategory imageCategory5 = new ImageCategory(Constants.CATEGORY_REPORT_LOCATION, R.drawable.ic_location_24dp, "Pin Location", false);

            imageCategoryArrayList.add(imageCategory1);
            //imageCategoryArrayList.add(imageCategory2);
            imageCategoryArrayList.add(imageCategory3);
            imageCategoryArrayList.add(imageCategory4);
            imageCategoryArrayList.add(imageCategory5);

        } else if (jobData.getSmsJobType().contains(Constants.UNDERWRITING)) {

            ImageCategory imageCategory1 = new ImageCategory(Constants.CATEGORY_PHOTOS, R.drawable.ic_camera_24dp, "Take Photos *", false);
            ImageCategory imageCategory2 = new ImageCategory(Constants.CATEGORY_SCAN_DOCUMENT, R.drawable.ic_scan_24dp, "Scan Documents", false);
            ImageCategory imageCategory3 = new ImageCategory(Constants.CATEGORY_REPORT_NORMAL, R.drawable.ic_report_24dp, "Report *", false);
            ImageCategory imageCategory4 = new ImageCategory(Constants.CATEGORY_REPORT_LOCATION, R.drawable.ic_location_24dp, "Pin Location", false);

            imageCategoryArrayList.add(imageCategory1);
            imageCategoryArrayList.add(imageCategory2);
            imageCategoryArrayList.add(imageCategory3);
            imageCategoryArrayList.add(imageCategory4);
        }

        categoryAdapter.addJobDataList(jobData, imageCategoryArrayList);

        binding.rvOptions.setAdapter(categoryAdapter);
        categoryAdapter.setClickListener(this);

    }

    @Override
    public void onOptionItemClick(View view, int position, ImageCategory imageCategory) {

        Intent intent;

        if (imageCategory.categoryId == Constants.CATEGORY_REPORT_OTS) {
            if (jobData.getOtsSyncStatus() == Constants.SYNC_COMPLETE) {
                showInforSnackbar("INFO: You have already upload OTS Report to the server.");
            } else {
                intent = new Intent(getApplicationContext(), SubNavigationActivity.class);
                intent.putExtra(Constants.EXTRA_JOB_DATA, jobData);
                intent.putExtra(Constants.EXTRA_JOB_CATEGORY, imageCategory);
                startActivity(intent);
            }
        } else if (imageCategory.categoryId == Constants.CATEGORY_REPORT_FLA) {
            intent = new Intent(getApplicationContext(), SubNavigationActivity.class);
            intent.putExtra(Constants.EXTRA_JOB_DATA, jobData);
            intent.putExtra(Constants.EXTRA_JOB_CATEGORY, imageCategory);
            startActivity(intent);
        } else if (imageCategory.categoryId == Constants.CATEGORY_PHOTOS || imageCategory.categoryId == Constants.CATEGORY_REPORT_NORMAL || imageCategory.categoryId == Constants.CATEGORY_SCAN_DOCUMENT) {
            intent = new Intent(getApplicationContext(), PhotoActivity.class);
            intent.putExtra(Constants.EXTRA_JOB_DATA, jobData);
            intent.putExtra(Constants.EXTRA_JOB_CATEGORY, imageCategory);
            startActivity(intent);
        } else {
            intent = new Intent(getApplicationContext(), PinLocationActivity.class);
            intent.putExtra(Constants.EXTRA_JOB_DATA, jobData);
            intent.putExtra(Constants.EXTRA_JOB_CATEGORY, imageCategory);
            startActivity(intent);
        }

    }

    private void validateCategoryList() {

        if (imageCategoryArrayList == null || imageCategoryArrayList.isEmpty()) {
            return;
        }

        DaoSession daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();

        //CLAIM VALIDATION
        for (int i = 0; i < imageCategoryArrayList.size(); i++) {

            ImageCategory imageCategory = imageCategoryArrayList.get(i);

            //TAKE PHOTO
            ImageDataDao imageDataDao = daoSession.getImageDataDao();
            long imageCount = imageDataDao.queryBuilder().where(ImageDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), ImageDataDao.Properties.ImageCategory.eq(imageCategory.categoryId)).count();


            if (imageCategory.categoryId == Constants.CATEGORY_PHOTOS) {
                if (imageCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                    underwritingPhotoCompleted = true;
                } else {
                    imageCategoryArrayList.get(i).completeStatus = false;
                }
            }

            if (imageCategory.categoryId == Constants.CATEGORY_SCAN_DOCUMENT) {
                if (imageCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                } else {
                    imageCategoryArrayList.get(i).completeStatus = false;
                }
            }

            if (imageCategory.categoryId == Constants.CATEGORY_REPORT_NORMAL) {
                if (imageCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                    underwritingReportCompleted = true;
                } else {
                    imageCategoryArrayList.get(i).completeStatus = false;
                }
            }

            if (imageCategory.categoryId == Constants.CATEGORY_REPORT_OTS) {

                FormDataDao formData = daoSession.getFormDataDao();
                long otsFormCount = formData.queryBuilder().where(FormDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), FormDataDao.Properties.CategoryId.eq(Constants.CATEGORY_REPORT_OTS_FORM)).count();

                if (imageCount > 0 || otsFormCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                } else {
                    imageCategoryArrayList.get(i).completeStatus = false;
                }

            }

            if (imageCategory.categoryId == Constants.CATEGORY_REPORT_FLA) {
                FormDataDao formData = daoSession.getFormDataDao();
                long flaFormCount = formData.queryBuilder().where(FormDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), FormDataDao.Properties.CategoryId.eq(Constants.CATEGORY_REPORT_FLA_FORM)).count();

                if (imageCount > 0 || flaFormCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                    flaReportCompleted = true;
                } else {
                    imageCategoryArrayList.get(i).completeStatus = false;
                }
            }

            //PIN LOCATION
            if (imageCategory.categoryId == Constants.CATEGORY_REPORT_LOCATION) {
                LocationDataDao locationData = daoSession.getLocationDataDao();
                long locationDataCount = locationData.queryBuilder().where(LocationDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), LocationDataDao.Properties.CategoryId.eq(imageCategory.categoryId)).count();
                if (locationDataCount > 0) {
                    imageCategoryArrayList.get(i).completeStatus = true;
                }
            }
        }

        categoryAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.tvUploadComplete) {

            if (jobData.getSmsJobType().contains(Constants.CLAIM)) {

                if (flaReportCompleted) {

                    jobData.setSyncStatus(Constants.SYNC_PENDING);
                    jobData.setCurrentJobStatus(Constants.CLOSED_JOB);
                    jobData.setJobClosedTime(DateTimeModule.getCurrentTimeStamp());
                    daoSession.update(jobData);

                    EventBus.getDefault().postSticky(new RemoveInprogressJobEvent(jobData));
                    startUploadService();
                    finish();

                } else {
                    showSnackbar("ERROR : Sorry, Looks like you have not completed the FLA Report.");
                }

            }


            if (jobData.getSmsJobType().contains(Constants.UNDERWRITING)) {

                if (underwritingPhotoCompleted && underwritingReportCompleted) {

                    Log.e(TAG, "SEND UNDERWRITING JOB");


                    jobData.setSyncStatus(Constants.SYNC_PENDING);
                    jobData.setCurrentJobStatus(Constants.CLOSED_JOB);
                    jobData.setJobClosedTime(DateTimeModule.getCurrentTimeStamp());
                    daoSession.update(jobData);


                    EventBus.getDefault().postSticky(new RemoveInprogressJobEvent(jobData));
                    startUploadService();
                    finish();

                } else {
                    showSnackbar("ERROR : Sorry, Looks like you have not completed the Take Photo and Report tasks");
                }
            }


        }
    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar
                .make(binding.rootConstraintLayout, message, Snackbar.LENGTH_LONG);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.snack_bar_bg));
        snackbar.show();
    }

    private void showInforSnackbar(String message) {
        Snackbar snackbar = Snackbar
                .make(binding.rootConstraintLayout, message, Snackbar.LENGTH_LONG);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.snack_bar_info_bg));
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getUpdatedJobData();
    }

    public void startUploadService() {
        Intent serviceIntent = new Intent(this, UploadService.class);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

}
