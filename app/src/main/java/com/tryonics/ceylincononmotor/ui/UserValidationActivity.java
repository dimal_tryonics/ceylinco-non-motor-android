package com.tryonics.ceylincononmotor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.apimanager.ApiInterface;
import com.tryonics.ceylincononmotor.apimanager.RxRestEngine;
import com.tryonics.ceylincononmotor.databinding.ActivityUserValidationBinding;
import com.tryonics.ceylincononmotor.model.auth.AuthResponse;
import com.tryonics.ceylincononmotor.util.AppPreferences;
import com.tryonics.ceylincononmotor.util.BaseActivity;
import com.tryonics.ceylincononmotor.util.DeviceInfo;
import com.tryonics.ceylincononmotor.util.IMEI;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;


public class UserValidationActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = UserValidationActivity.class.getName();

    private ActivityUserValidationBinding binding;

    private AppPreferences appPreferences;
    private ApiInterface restEngine;

    private boolean userActivationStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUserValidationBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        appPreferences = new AppPreferences(getApplicationContext());
        restEngine = RxRestEngine.getInstance().getService(getApplicationContext());

        binding.btnDone.setOnClickListener(this);

        requestUserValidation();

    }


    private void getUserImeiNumber() {

        //String imeiNumber = "111222333444"; //TESTING IMEI NUMBER
        String imeiNumber = IMEI.getIMEI(getApplicationContext());

        final String deviceDes = DeviceInfo.getDeviceData();

        if (imeiNumber != null) {
            performNetworkRequest(imeiNumber, deviceDes);
        } else {
            showSnackbar("ERROR: IMEI can not retrieve from this device");
        }

    }

    private void performNetworkRequest(String imeiNo, String deviceDes) {
        userValidation(imeiNo, deviceDes);
    }

    private void userValidation(String imeiNo, String deviceDes) {

        /*restEngine.login(imeiNo, deviceDes)
                .delay(3000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<AuthResponse>>() {
                    @Override
                    public void onNext(Response<AuthResponse> response) {

                        if (response != null) {
                            int code = response.code();

                            Log.e(TAG, "HTTP STATUS CODE : " + code);

                            if (code == 200) {
                                AuthResponse authResponse = (AuthResponse) response.body();

                                if (authResponse.error != null) {
                                    inactiveUser(authResponse);
                                } else {
                                    requestSuccess(imeiNo, authResponse);
                                }
                            }
                        }


                    }

                    @Override
                    public void onError(Throwable e) {

                        networkError();

                    }

                    @Override
                    public void onComplete() {

                    }
                });*/

    }

    private void requestUserValidation() {

        binding.imageView.setImageResource(R.drawable.img_checking);
        binding.tvTitle.setText(getResources().getString(R.string.user_validation_error_title));
        binding.tvMessage.setText(getResources().getString(R.string.user_validation_error_request));
        binding.btnDone.setEnabled(false);
        binding.progressBar.setVisibility(View.VISIBLE);

        getUserImeiNumber();

    }

    private void inactiveUser(AuthResponse authResponse) {
        userActivationStatus = false;

        binding.imageView.setImageResource(R.drawable.img_error);
        binding.tvTitle.setText("User account is inactive!");
        binding.tvMessage.setText(authResponse.error.message);
        binding.btnDone.setEnabled(true);
        binding.btnDone.setText("RETRY");
        binding.progressBar.setVisibility(View.GONE);
    }

    private void networkError() {
        binding.imageView.setImageResource(R.drawable.img_no_internet);
        binding.tvTitle.setText(getResources().getString(R.string.title_no_internet));
        binding.tvMessage.setText(getResources().getString(R.string.message_no_internet));
        binding.btnDone.setText("RETRY");
        binding.btnDone.setEnabled(true);
        binding.progressBar.setVisibility(View.GONE);
    }

    private void requestSuccess(String imeiNo, AuthResponse authResponse) {

        userActivationStatus = true;
        appPreferences.setIMEINo(imeiNo);
        appPreferences.setAuthorization(authResponse.tokenType + " " + authResponse.accessToken);

        binding.imageView.setImageResource(R.drawable.img_valid_user);
        binding.tvTitle.setText("Authentication Successful");
        binding.tvMessage.setText(getResources().getString(R.string.user_validation_error_complete));
        binding.btnDone.setEnabled(true);
        binding.btnDone.setText("DONE");
        binding.progressBar.setVisibility(View.GONE);

    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar
                .make(binding.rootConstraintLayout, message, Snackbar.LENGTH_LONG);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.snack_bar_bg));
        snackbar.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnDone:

                if (userActivationStatus) {

                    Intent intent = new Intent(UserValidationActivity.this, ViewPagerActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    requestUserValidation();
                }

                break;
        }

    }
}
