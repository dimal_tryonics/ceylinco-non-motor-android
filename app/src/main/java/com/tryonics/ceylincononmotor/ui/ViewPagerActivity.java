package com.tryonics.ceylincononmotor.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.tryonics.ceylincononmotor.adapter.FragmentViewPagerAdapter;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.ImageData;
import com.tryonics.ceylincononmotor.database.dao.ImageDataDao;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.database.dao.JobDataDao;
import com.tryonics.ceylincononmotor.databinding.ActivityDrawerBinding;
import com.tryonics.ceylincononmotor.fragment.ClosedJobListFragment;
import com.tryonics.ceylincononmotor.fragment.InProgressListFragment;
import com.tryonics.ceylincononmotor.fragment.PendingListFragment;
import com.tryonics.ceylincononmotor.service.UploadService;
import com.tryonics.ceylincononmotor.util.AppPreferences;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.DateTimeModule;
import com.tryonics.ceylincononmotor.util.ImageManager;

import java.util.List;

public class ViewPagerActivity extends AppCompatActivity implements View.OnClickListener, InProgressListFragment.OnInProgressFragmentListener {

    private static final String TAG = ViewPagerActivity.class.getName();
    public ViewPager2 viewPager;
    private ActivityDrawerBinding binding;
    private FragmentViewPagerAdapter viewPagerAdapter;
    private DaoSession daoSession;
    private String[] tab_name_list = {"Pending", "In-Progress", "Closed"};
    public AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDrawerBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();
        appPreferences = new AppPreferences(getApplicationContext());

        configFragmentList();

        registerServiceLogout();

        clearPastRecords();
        startSync();


    }

    private void configFragmentList() {

        binding.ivSettings.setOnClickListener(this);

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("First"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Second"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Third"));

        viewPagerAdapter = new FragmentViewPagerAdapter(this);
        viewPager = binding.vpJob;

        viewPager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(viewPagerAdapter);


        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                Log.e(TAG, "onPageSelected:position " + position);

                if (position == 0) {
                    PendingListFragment fragment = viewPagerAdapter.pendingListFragment;
                    fragment.loadPendingJobs(Constants.FILTER_ALL);
                } else if (position == 1) {
                    InProgressListFragment fragment1 = viewPagerAdapter.inProgressListFragment;

                    new Handler().postDelayed(() -> {
                        fragment1.loadPendingJobs(Constants.FILTER_ALL);
                    }, 300);

                } else if (position == 2) {
                    ClosedJobListFragment fragment2 = viewPagerAdapter.closedListFragment;

                    new Handler().postDelayed(() -> {
                        fragment2.loadClosedJobList();
                    }, 300);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });


        new TabLayoutMediator(binding.tabLayout, viewPager,
                new TabLayoutMediator.TabConfigurationStrategy() {
                    @Override
                    public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                        tab.setText(tab_name_list[position]);
                    }
                }).attach();


        /*viewPager.unregisterOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });*/

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binding.ivSettings.getId()) {
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onInProgressJobClickListener(JobData jobData) {
        Intent intent = new Intent(getApplicationContext(), ImageCategoryActivity.class);
        intent.putExtra(Constants.EXTRA_JOB_DATA, jobData);
        startActivity(intent);
    }

    private void startSync() {


        List<JobData> jobData = daoSession.queryBuilder(JobData.class)
                .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.CLOSED_JOB),
                        JobDataDao.Properties.SyncStatus.eq(Constants.SYNC_PENDING))
                .orderDesc(JobDataDao.Properties.JobStartTime)
                .list();

        if (jobData.size() > 0) {
            startUploadService();
        }
    }

    public void startUploadService() {
        Intent serviceIntent = new Intent(this, UploadService.class);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    private void clearPastRecords() {

        List<JobData> jobDataList = daoSession.queryBuilder(JobData.class)
                .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.CLOSED_JOB),
                        JobDataDao.Properties.SyncStatus.eq(Constants.SYNC_COMPLETE))
                .orderDesc(JobDataDao.Properties.JobStartTime)
                .list();

        for (JobData jobData: jobDataList) {

            long hoursDiff = DateTimeModule.getHoursDiff(jobData.getJobClosedTime());
            Log.e(TAG, "clearPastRecords: " + hoursDiff );

            if(hoursDiff >= Constants.JOB_DELETE_TIME) {

                ImageDataDao imageDataDao = daoSession.getImageDataDao();
                List<ImageData> imageDataList = imageDataDao.queryBuilder().where(ImageDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), ImageDataDao.Properties.SyncStatus.eq(Constants.SYNC_COMPLETE)).list();
                for (ImageData imageData: imageDataList) {
                    ImageManager.removeLocalImage(imageData.getImageUrl());
                    imageDataDao.delete(imageData);
                }

                JobDataDao jobDataDao = daoSession.getJobDataDao();
                jobDataDao.delete(jobData);
            }


        }

    }

    private void registerServiceLogout(){
        LocalBroadcastManager mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("com.tryonics.ais_icms.service.logout");
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals("com.tryonics.ais_icms.service.logout")){
                appPreferences.removeAuthorization();
                appPreferences.removeDutyStatus();
                appPreferences.removeJobId();
                appPreferences.removeLastLocation();
                appPreferences.removeIMEINo();
                appPreferences.removeImeiNumberWithTimestamp();
                finish();
            }
        }
    };


}
