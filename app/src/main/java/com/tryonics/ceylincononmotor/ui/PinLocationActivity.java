package com.tryonics.ceylincononmotor.ui;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.database.dao.LocationData;
import com.tryonics.ceylincononmotor.database.dao.LocationDataDao;
import com.tryonics.ceylincononmotor.databinding.ActivityPinLocationBinding;
import com.tryonics.ceylincononmotor.model.option.ImageCategory;
import com.tryonics.ceylincononmotor.ui.dialog.DialogLocationService;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.LocationServiceValidator;
import com.tryonics.ceylincononmotor.util.location.LocationService;
import com.tryonics.ceylincononmotor.util.location.LocationValidator;

import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;

public class PinLocationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = PinLocationActivity.class.getName();

    private ActivityPinLocationBinding binding;

    private BroadcastReceiver locationUpdateReceiver;
    private boolean locationReceived = true;
    private CountDownTimer locationTimeOutCounter;
    private LocationService locationService;
    private final ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {

            String name = className.getClassName();

            if (name.endsWith("LocationService")) {
                locationService = ((LocationService.LocationServiceBinder) service).getService();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (className.getClassName().equals("LocationService")) {
                locationService.stopUpdatingLocation();
                locationService = null;
            }
        }
    };
    private ProgressDialog progressDialog;

    private DaoSession daoSession;

    private JobData jobData;
    private ImageCategory imageCategory;

    private Location selectedLocation;
    //private int selectedLocationType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPinLocationBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();

        getBundleData();

        binding.progressBar.setVisibility(View.GONE);
        binding.tvBtnFindLocation.setOnClickListener(this);
        binding.tvDone.setOnClickListener(this);

        initLocationBroadcastReceiver();
    }

    private void getBundleData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobData = extras.getParcelable(Constants.EXTRA_JOB_DATA);
            imageCategory = extras.getParcelable(Constants.EXTRA_JOB_CATEGORY);
        }
        selectAllImage();
    }

    private void selectAllImage() {

        LocationDataDao locationDataDao = daoSession.getLocationDataDao();
        LocationData locationData = locationDataDao.queryBuilder().where(LocationDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), LocationDataDao.Properties.CategoryId.eq(imageCategory.categoryId)).unique();

        //Log.e(TAG, "selectAllImage: Latitude " + locationData.getLatitude() + "  Longitude " +locationData.getLongitude() + " LOCATION TYPE "+ locationData.getLocationType() );

        if (locationData != null) {

            Location location = new Location(LocationManager.GPS_PROVIDER);
            location.setLatitude(locationData.getLatitude());
            location.setLongitude(locationData.getLongitude());

            selectedLocation = location;
            //selectedLocationType = locationData.getLocationType();

            if (selectedLocation != null) {
                binding.imgLocationReceive.setImageResource(R.drawable.ic_check_24dp);
                binding.tvLocationReceive.setText("Done");
            }

            /*
            if (selectedLocationType == Constants.LOCATION_TYPE_1) {
                binding.rbFloodArea.setChecked(true);
            } else if (selectedLocationType == Constants.LOCATION_TYPE_2) {
                binding.rbNotAFloodArea.setChecked(true);
            }
            */
        }

    }

    private void initLocationBroadcastReceiver() {

        final Intent locationService = new Intent(this.getApplication(), LocationService.class);
        this.getApplication().startService(locationService);
        this.getApplication().bindService(locationService, serviceConnection, Context.BIND_AUTO_CREATE);

        locationUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (locationTimeOutCounter != null) {
                    locationTimeOutCounter.cancel();
                }

                stopLocation();

                Location newLocation = intent.getParcelableExtra("location");

                if (!locationReceived) {

                    if (LocationValidator.isValidLatLng(newLocation.getLatitude(), newLocation.getLongitude())) {

                        selectedLocation = newLocation;

                        binding.imgLocationReceive.setImageResource(R.drawable.ic_check_24dp);
                        binding.tvLocationReceive.setText("Done");

                        progressDialog.dismiss();

                        locationReceived = true;

                    } else {
                        showSnackbar("Sorry, Application can't retrieve your current Location. Please check your location service settings");
                    }
                }

            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(
                locationUpdateReceiver,
                new IntentFilter("LocationUpdated"));


    }

    private void locationTimeOut() {

        locationTimeOutCounter = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                progressDialog.dismiss();
                stopLocation();
                showSnackbar("Sorry, Application can't retrieve your current Location. Please check your location service settings");
            }
        }.start();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvBtnFindLocation:

                checkPermissionStatus();

                break;

            case R.id.tvDone:

                if (validateForm()) {

                    binding.progressBar.setVisibility(View.VISIBLE);

                    new Handler().postDelayed(() -> {

                        binding.progressBar.setVisibility(View.GONE);
                        saveLocationData();
                        finish();

                    }, 1000);

                } else {
                    showSnackbar("Please check weather you have clicked FIND LOCATION and Location type ");
                }

                break;

        }

    }

    private void checkPermissionStatus() {

        Dexter.withContext(PinLocationActivity.this)
                .withPermissions(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)

                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            getLocation();

                        } else {
                            checkPermissionStatus();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .withErrorListener(error -> showSnackbar("ERROR: Permission error occurred"))
                .check();
    }

    private void getLocation(){

        LocationServiceValidator locationService = new LocationServiceValidator(getApplicationContext());
        locationService.setLocationServiceListener(new LocationServiceValidator.OnLocationServiceListener() {
            @Override
            public void LocationServiceAvailable(int LocationMode) {
                progressDialog = ProgressDialog.show(PinLocationActivity.this, null, "Retrieving current location ...", true);
                progressDialog.setCancelable(false);
                locationReceived = false;
                locationTimeOut();
                startLocation();
            }

            @Override
            public void LocationServiceUnavailable() {
                DialogLocationService.show(PinLocationActivity.this);
            }
        });


    }

    private boolean validateForm() {

        /*boolean validationStatus = false;

        int selectedId = binding.segmentLocationType.getCheckedRadioButtonId();

        if (selectedId != -1 && selectedLocation != null) {
            validationStatus = true;
            selectedLocationType = Integer.parseInt(findViewById(selectedId).getTag().toString());
        }

        return validationStatus;*/

        boolean validationStatus = false;

        if (selectedLocation != null) {
            validationStatus = true;
            //selectedLocationType = Integer.parseInt(findViewById(selectedId).getTag().toString());
        }

        return validationStatus;
    }


    private void saveLocationData() {

        LocationDataDao locationDataDao = daoSession.getLocationDataDao();

        LocationData locationData = new LocationData();
        locationData.setSmsClaimNo(jobData.getSmsClaimNo());
        locationData.setCategoryId(imageCategory.categoryId);
        locationData.setLatitude(selectedLocation.getLatitude());
        locationData.setLongitude(selectedLocation.getLongitude());
        //locationData.setLocationType(selectedLocationType);
        locationData.setSyncStatus(Constants.SYNC_PENDING);

        locationDataDao.insertOrReplace(locationData);

    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar
                .make(binding.rootConstraintLayout, message, Snackbar.LENGTH_LONG);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.snack_bar_bg));
        snackbar.show();
    }


    private void startLocation() {
        locationService.startUpdatingLocation();
    }

    private void stopLocation() {
        locationService.stopUpdatingLocation();
    }

    @Override
    public void onDestroy() {
        try {
            if (locationUpdateReceiver != null) {
                unregisterReceiver(locationUpdateReceiver);
            }
        } catch (IllegalArgumentException ex) {
            //ex.printStackTrace();
        }
        super.onDestroy();
    }
}
