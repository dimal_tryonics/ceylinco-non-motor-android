package com.tryonics.ceylincononmotor.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;
import com.otaliastudios.cameraview.controls.Flash;
import com.otaliastudios.cameraview.controls.Mode;
import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.ImageData;
import com.tryonics.ceylincononmotor.database.dao.ImageDataDao;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.databinding.ActivityCameraViewBinding;
import com.tryonics.ceylincononmotor.databinding.ActivityPhotoBinding;
import com.tryonics.ceylincononmotor.model.option.ImageCategory;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.BitmapDecorder;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.file.TryonicsFileManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CameraViewActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String TAG = CameraViewActivity.class.getName();

    private ActivityCameraViewBinding binding;

    private ImageDataDao imageDataDao;
    private ImageData imageData;

    private JobData jobData;
    private ImageCategory imageCategory;

    private File photoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityCameraViewBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        getBundleData();

        selectDatabase();

        binding.btnCapture.setOnClickListener(this);
        binding.imgFlash.setOnClickListener(this);

        binding.camera.setLifecycleOwner(this);
        binding.camera.setMode(Mode.PICTURE);

        binding.camera.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(PictureResult result) {
                result.toFile(Objects.requireNonNull(TryonicsFileManager.createLocalImageFile(getApplicationContext())), file -> compressSingleListener(file));
            }
        });

    }

    private void getBundleData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobData = extras.getParcelable(Constants.EXTRA_JOB_DATA);
            imageCategory = extras.getParcelable(Constants.EXTRA_JOB_CATEGORY);
        }
    }

    private void selectDatabase() {
        DaoSession daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();
        imageDataDao = daoSession.getImageDataDao();
        imageDataDao.queryBuilder().LOG_VALUES = true;
        imageDataDao.queryBuilder().LOG_SQL = true;
    }

    @SuppressLint("AutoDispose")
    private void compressSingleListener(File originalFile) {
        photoFile = originalFile;
        singleCompressor.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<File>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        //showImageOptimizer();
                    }

                    @Override
                    public void onNext(File file) {
                        //Compression done & it's notified with the new compressed fileName.
                        //Now its safe to delete the original file
                        Log.e(TAG, "COMPRESSED FILE " + file.getAbsolutePath());
                        boolean flag = photoFile.delete();
                        Log.e(TAG," ORIGINAL FILE DELETED " + flag + " " + photoFile.getAbsolutePath());
                        saveCapturedImage(file);
                        //notifySystemGallery(file.getPath());
                    }

                    @Override
                    public void onError(Throwable e) {
                        //hideImageOptimizer();
                        binding.btnCapture.setEnabled(true);
                    }

                    @Override
                    public void onComplete() {
                        //hideImageOptimizer();
                        binding.btnCapture.setEnabled(true);
                    }
                });
    }

    private void saveCapturedImage(File file) {
        if (file == null) {
            return;
        }

        ImageData imageData = new ImageData(null, jobData.getSmsClaimNo(), imageCategory.categoryId, file.getAbsolutePath(), Constants.SYNC_PENDING);
        imageDataDao.insert(imageData);
        Log.e(TAG, "IMAGE SAVED SUCCESSFULLY");

    }

    private void notifySystemGallery(String currentPhotoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    //RxJava Observable patten implementation
    final Observable<File> singleCompressor = Observable.create(emitter -> {
        if (photoFile.exists()) {
            Bitmap bitmap = BitmapDecorder.getScaledBitmapFromFile(photoFile.getAbsolutePath(), getImageQualitySetting());
            File newFile = null;
            FileOutputStream out;
            try {
                //creates a new file with the name formatted with vehicleNumber
                newFile = TryonicsFileManager.createFolderWithVehicleNumber(jobData);
                if (newFile != null) {
                    out = new FileOutputStream(newFile.getAbsolutePath());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, out);

                    //Release bitmap resource
                    bitmap.recycle();
                    bitmap = null;

                    //Release outputstream
                    out.flush();
                    out.close();
                    out = null;
                }
                //Notify Observer about the jobcompletion with the fileName
                emitter.onNext(newFile);
                //This will trigger the observers onCompleted event
                emitter.onComplete();

            } catch (IOException e) {
                showToastMessage("Error " + e.getMessage());
                emitter.onError(e);
            }
        } else {
            showToastMessage("Capture image not found");
        }
    });

    private boolean getImageQualitySetting() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        return sharedPreferences.getBoolean("high_quality_images", false);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == binding.imgFlash.getId()) {
            if (binding.camera.getFlash() == Flash.OFF) {
                binding.camera.setFlash(Flash.ON);
                binding.imgFlash.setImageDrawable(getDrawable(R.drawable.ic_flash_on_24));
            } else if (binding.camera.getFlash() == Flash.ON) {
                binding.camera.setFlash(Flash.OFF);
                binding.imgFlash.setImageDrawable(getDrawable(R.drawable.ic_flash_off_24));
            }
        }

        if (view.getId() == binding.btnCapture.getId()) {
            binding.btnCapture.setEnabled(false);
            binding.camera.takePicture();
        }
    }

    public void showToastMessage(final String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        setResult(Activity.RESULT_OK);
        finish();
    }
}