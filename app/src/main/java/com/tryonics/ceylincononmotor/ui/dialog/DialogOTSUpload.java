package com.tryonics.ceylincononmotor.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.apimanager.ApiInterface;
import com.tryonics.ceylincononmotor.apimanager.RxRestEngine;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.FormData;
import com.tryonics.ceylincononmotor.database.dao.FormDataDao;
import com.tryonics.ceylincononmotor.database.dao.ImageData;
import com.tryonics.ceylincononmotor.database.dao.ImageDataDao;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.model.CommonResponse;
import com.tryonics.ceylincononmotor.model.jobinit.JobInitResponse;
import com.tryonics.ceylincononmotor.model.option.ImageCategory;
import com.tryonics.ceylincononmotor.ui.SplashActivity;
import com.tryonics.ceylincononmotor.util.AppPreferences;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;


import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadServiceSingleBroadcastReceiver;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;



/**
 * Created by Dimal on 5/22/18.
 */

public class DialogOTSUpload extends DialogFragment implements UploadStatusDelegate   {

    public static final String EXTRA_JOB = "extra_JOB";
    public static final String EXTRA_IMAGE_CATEGORY = "extra_image_category";
    private static final String TAG = DialogOTSUpload.class.getSimpleName();

    private ConstraintLayout constraintLayout;
    private TextView tvUploadFileCount;
    private TextView tvUploadFileName;
    private TextView tvProgressValue;
    private ProgressBar progressBar;
    private TextView tvUpload;
    private TextView btnClose;
    private TextView tvUploadComplete;

    private DaoSession daoSession;
    private ApiInterface restEngine;
    private JobData jobData;
    private ImageCategory imageCategory;

    private FormDataDao formDataDao;
    private FormData formData;

    private ImageDataDao imageDataDao;
    private List<ImageData> imageDataList;
    private ImageData image;

    private int pendingUploadCount = 0;
    private int completeUploadCount = 0;

    private DialogOTSUploadClickListener listener;

    private boolean uploadComplete = false;

    private UploadServiceSingleBroadcastReceiver uploadReceiver;

    public DialogOTSUpload() {
        // Required empty public constructor
    }

    //Show dialog with provide text and text color
    public static DialogOTSUpload show(@NonNull AppCompatActivity appCompatActivity, JobData jobData, ImageCategory imageCategory) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_JOB, jobData);
        args.putParcelable(EXTRA_IMAGE_CATEGORY, imageCategory);
        DialogOTSUpload fragment = new DialogOTSUpload();
        fragment.setArguments(args);
        fragment.setCancelable(false);
        fragment.show(appCompatActivity.getSupportFragmentManager(), TAG);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();

        //Make dialog full screen with transparent background
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        uploadReceiver = new UploadServiceSingleBroadcastReceiver(this);
        return inflater.inflate(R.layout.fragment_dialog_upload, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        daoSession = ((ApplicationController) getActivity().getApplicationContext()).getDaoSession();
        restEngine = RxRestEngine.getInstance().getService(getActivity().getApplicationContext());

        jobData = getArguments().getParcelable(EXTRA_JOB);
        imageCategory = getArguments().getParcelable(EXTRA_IMAGE_CATEGORY);

        getOSTData();

        tvUploadFileCount = view.findViewById(R.id.tvUploadFileCount);
        tvUploadFileCount.setText(String.format("%02d/%02d", (completeUploadCount), pendingUploadCount));

        TextView tvMessage = view.findViewById(R.id.tvMessage);
        tvMessage.setText("You have successfully completed OST details related to claim number " + jobData.getSmsClaimNo() + ". Please click the \"UPLOAD\" button to confirm your OST submission.");

        constraintLayout = view.findViewById(R.id.constraintLayout);

        tvUploadFileName = view.findViewById(R.id.tvUploadFile);
        tvUploadFileName.setText("");
        tvProgressValue = view.findViewById(R.id.tvProgressValue);
        tvProgressValue.setText("");
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        tvUploadComplete = view.findViewById(R.id.tvUploadComplete);
        tvUploadComplete.setVisibility(View.GONE);

        constraintLayout.setVisibility(View.GONE);

        btnClose = view.findViewById(R.id.tvCancel);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (uploadComplete) {
                    listener.OnUploadCompleteListener();
                    dismiss();
                } else {
                    dismiss();
                }

            }
        });

        tvUpload = view.findViewById(R.id.tvUpload);
        tvUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvUpload.setEnabled(false);

                constraintLayout.setVisibility(View.VISIBLE);

                //Log.e(TAG, "onClick: " + jobData.getJobSyncLevel() + " " + Constants.SYNC_LEVEL_UNTOUCH);

                if (jobData.getJobSyncLevel() != Constants.SYNC_LEVEL_UNTOUCH) {
                    startDataUpload();
                } else {
                    dataUpload_init();
                }

            }
        });

    }


    private void getOSTData() {
        formDataDao = daoSession.getFormDataDao();
        formData = formDataDao.queryBuilder().where(FormDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), FormDataDao.Properties.CategoryId.eq(Constants.CATEGORY_REPORT_OTS_FORM), FormDataDao.Properties.SyncStatus.eq(Constants.SYNC_PENDING)).unique();

        imageDataDao = daoSession.getImageDataDao();
        imageDataList = imageDataDao.queryBuilder().where(ImageDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), ImageDataDao.Properties.ImageCategory.eq(Constants.CATEGORY_REPORT_OTS), ImageDataDao.Properties.SyncStatus.eq(Constants.SYNC_PENDING)).list();

        if (formData != null) {
            pendingUploadCount = 1 + imageDataList.size();
        } else {
            pendingUploadCount = imageDataList.size();
        }

    }


    @SuppressLint("CheckResult")
    private void dataUpload_init() {

        showUploadingFileName("Uploading job data", false);
        showLoading();

        AppPreferences appPreferences = new AppPreferences(getActivity().getApplicationContext());

        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        requestBodyMap.put("claim_no", convertValue(jobData.getSmsClaimNo()));
        requestBodyMap.put("job_ref_no", convertValue(jobData.getSmsJobRefNo()));
        requestBodyMap.put("job_type", convertValue(jobData.getSmsJobType()));
        requestBodyMap.put("imei", convertValue(appPreferences.getIMEINo()));
        requestBodyMap.put("job_receive_timestamp", convertValue(String.valueOf(jobData.getSmsReceivedTime())));
        requestBodyMap.put("job_start_timestamp", convertValue(String.valueOf(jobData.getJobStartTime())));
        requestBodyMap.put("job_originate", convertValue("1"));

        restEngine.jobInitialization(jobData.getSmsClaimNo(), requestBodyMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response<JobInitResponse>>() {
                    @Override
                    public void onNext(Response<JobInitResponse> jobInitResponseResponse) {

                        hideLoading();

                        JobInitResponse response = jobInitResponseResponse.body();

                        if (jobInitResponseResponse.code() == 200) {
                            if (response.error != null) {
                                enableUploadButton();
                                showUploadingFileName("Something went wrong", true);

                            } else {

                                jobData.setJobId(response.jobId);
                                jobData.setJobSyncLevel(Constants.SYNC_LEVEL_INIT);
                                daoSession.update(jobData);

                                //updateUploadingFileCount();

                                startDataUpload();
                            }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        enableUploadButton();
                        hideLoading();
                        showUploadingFileName("Something went wrong", true);
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @SuppressLint("CheckResult")
    private void uploadFormData() {

        showUploadingFileName("Uploading form data", false);
        showLoading();

        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        requestBodyMap.put("claim_no", convertValue(jobData.getSmsClaimNo()));
        requestBodyMap.put("category_id", convertValue(String.valueOf(Constants.CATEGORY_REPORT_OTS_FORM)));
        requestBodyMap.put("job_type", convertValue(jobData.getSmsJobType()));
        requestBodyMap.put("survey_date", convertValue(formData.getSurveyDate()));
        requestBodyMap.put("loss_date", convertValue(formData.getLossDate()));
        requestBodyMap.put("claim_amount", convertValue(formData.getClaimAmount()));
        requestBodyMap.put("cause_of_loss", convertValue(formData.getCauseOfLoss()));
        requestBodyMap.put("cause_of_loss_detail", convertValue(formData.getCauseOfLossDetail())); //NEW
        requestBodyMap.put("risk_location", convertValue(formData.getRiskLocation())); //NEW

        if(Constants.FORM_FIELD_DEDUCT == formData.getSalvage()){
            requestBodyMap.put("salvage", convertValue(getString(R.string.form_deduct)));
        } else if(Constants.FORM_FIELD_IGNORE == formData.getSalvage()){
            requestBodyMap.put("salvage", convertValue(getString(R.string.form_ignore)));
        } else if(Constants.FORM_FIELD_COLLECT == formData.getSalvage()){
            requestBodyMap.put("salvage", convertValue(getString(R.string.form_collect)));
        }

        requestBodyMap.put("notes", convertValue(formData.getNote()));
        requestBodyMap.put("claim_adjustment", convertValue(formData.getClaimAdjustment()));

        restEngine.formDataSubmit(jobData.getJobId(), requestBodyMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response<CommonResponse>>() {
                    @Override
                    public void onNext(Response<CommonResponse> jobInitResponseResponse) {

                        hideLoading();

                        CommonResponse response = jobInitResponseResponse.body();

                        if (jobInitResponseResponse.code() == 200) {

                            if (response.error != null) {
                                enableUploadButton();
                                showUploadingFileName("Something went wrong", true);
                            } else {

                                formData.setSyncStatus(Constants.SYNC_COMPLETE);
                                formDataDao.update(formData);

                                updateUploadingFileCount();

                                uploadImages();
                            }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        enableUploadButton();
                        hideLoading();
                        showUploadingFileName("Something went wrong", true);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void startDataUpload() {
        if (formData != null && formData.getSyncStatus() != Constants.SYNC_COMPLETE) {
            uploadFormData();
        } else if (imageDataList.size() > 0) {
            Log.e(TAG, "startDataUpload: imageDataList.size() " + imageDataList.size());
            uploadImages();
        } else {
            enableUploadButton();
            //showUploadingFileName("No data to upload", true);
            showUploadingFileName("OST Data upload complete", false);
            uploadComplete = true;

            tvUpload.setVisibility(View.INVISIBLE);
            btnClose.setText("DONE");

            constraintLayout.setVisibility(View.GONE);
            tvUploadComplete.setVisibility(View.VISIBLE);

            jobData.setOtsSyncStatus(Constants.SYNC_COMPLETE);
            daoSession.update(jobData);

        }
    }

    private void uploadImages() {

        if (imageDataList.size() > 0) {
            image = imageDataList.get(0);

            Log.e(TAG, "startDataUpload: SIZE " + imageDataList.size());

            if (image != null) {
                uploadBinary();
            }
        } else {
            Log.e(TAG, "No image files to upload");

            startDataUpload();
        }
    }


    public void uploadBinary() {

        try {
            AppPreferences appPreferences = new AppPreferences(getActivity().getApplicationContext());
            String accessToken = appPreferences.getAuthorization();

            Log.e(TAG, "uploadBinary:getSmsClaimNo " + jobData.getSmsClaimNo());

            showUploadingFileName("Uploading image data", false);
            showLoading();

            //String uploadId = UUID.randomUUID().toString();
            //uploadReceiver.setUploadID(uploadId);

            String uploadId = UUID.randomUUID().toString();
            uploadReceiver.setUploadID(uploadId);

            UploadNotificationConfig c =  new UploadNotificationConfig();
            c.setRingToneEnabled(false);
            c.setNotificationChannelId("1234");
            c.setClearOnActionForAllStatuses(true);

            new MultipartUploadRequest(getActivity().getApplicationContext(), uploadId,Constants.BASE_URL + jobData.getJobId() + "/image_sync")
                    .setMethod("POST")
                    .addHeader("Accept", "application/json")
                    .addHeader("Authorization", accessToken)
                    .addFileToUpload(image.getImageUrl(), "image")
                    .addParameter("claim_no", jobData.getSmsClaimNo())
                    .addParameter("category_id", String.valueOf(image.getImageCategory()))
                    .addParameter("job_type", jobData.getSmsJobType())
                    .setNotificationConfig(c)
                    .startUpload();

            /*request.subscribe(getActivity(), this, new RequestObserverDelegate() {
                @Override
                public void onProgress(@NotNull Context context, @NotNull UploadInfo uploadInfo) {
                    Log.e(TAG, "UPLOAD onProgress: ");
                    tvProgressValue.setText("" + uploadInfo.getProgressPercent() + "%");
                }

                @Override
                public void onSuccess(@NotNull Context context, @NotNull UploadInfo uploadInfo, @NotNull ServerResponse serverResponse) {

                }

                @Override
                public void onError(@NotNull Context context, @NotNull UploadInfo uploadInfo, @NotNull Throwable throwable) {
                    Log.e(TAG, "UPLOAD onError: 888 ");
                    showUploadingFileName("Request timeout. Please retry.", true);
                    hideLoading();
                    enableUploadButton();
                    hideLoading();
                }

                @Override
                public void onCompleted(@NotNull Context context, @NotNull UploadInfo uploadInfo) {
                    Log.e(TAG, "UPLOAD onCompleted: ");

                    //ImageData imageData = imageDataDao.queryBuilder().where(ImageDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), ImageDataDao.Properties.ImageCategory.eq(Constants.CATEGORY_REPORT_OTS), ImageDataDao.Properties.Id.eq(jobData.id)).unique();

                    image.setSyncStatus(Constants.SYNC_COMPLETE);
                    imageDataDao.update(image);

                    imageDataList.remove(0);
                    updateUploadingFileCount();

                    uploadImages();
                }

                @Override
                public void onCompletedWhileNotObserving() {

                }
            });*/


        } catch (Exception exc) {

            hideLoading();
            showUploadingFileName("Request timeout. Please retry.", true);
            enableUploadButton();
            hideLoading();

            //Log.e(TAG, "uploadBinary: " + exc.getMessage());
            //exc.printStackTrace();
        }
    }


    private RequestBody convertValue(String value) {
        if (value == null) {
            return RequestBody.create("", MediaType.parse("text/plain"));
        } else {
            return RequestBody.create(value, MediaType.parse("text/plain"));
        }
    }


    private void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void enableUploadButton() {
        tvUpload.setEnabled(true);
    }

    private void showUploadingFileName(String text, boolean error) {
        if (error) {
            tvUploadFileName.setTextColor(Color.RED);
        } else {
            tvUploadFileName.setTextColor(Color.BLACK);
        }
        tvUploadFileName.setText(text);
    }

    @SuppressLint("DefaultLocale")
    private void updateUploadingFileCount() {
        tvUploadFileCount.setText(String.format("%02d/%02d", (++completeUploadCount), pendingUploadCount));
    }

    @Override
    public void onResume() {
        super.onResume();
        uploadReceiver.register(DialogOTSUpload.this.getContext());
    }

    @Override
    public void onPause() {
        super.onPause();
        uploadReceiver.unregister(DialogOTSUpload.this.getContext());
    }




    /*@Override
    public void onCompleted(@NotNull UploadInfo uploadInfo, int i, @NotNull UploadNotificationConfig uploadNotificationConfig) {
        Log.e(TAG, "UPLOAD onCompleted: ");

        ImageData imageData = imageDataDao.queryBuilder().where(ImageDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), ImageDataDao.Properties.ImageCategory.eq(imageCategory.categoryId), ImageDataDao.Properties.Id.eq(jobData.id)).unique();
        imageData.setSyncStatus(Constants.SYNC_COMPLETE);
        imageDataDao.update(imageData);

        imageDataList.remove(0);
        updateUploadingFileCount();

        uploadImages();

    }

    @Override
    public void onError(@NotNull UploadInfo uploadInfo, int i, @NotNull UploadNotificationConfig uploadNotificationConfig, @NotNull Throwable throwable) {
        Log.e(TAG, "UPLOAD onError: 888 ");
        showUploadingFileName("Request timeout. Please retry.", true);
        hideLoading();
        enableUploadButton();
        hideLoading();
    }

    @Override
    public void onProgress(@NotNull UploadInfo uploadInfo, int i, @NotNull UploadNotificationConfig uploadNotificationConfig) {
        Log.e(TAG, "UPLOAD onProgress: ");
        tvProgressValue.setText("" + uploadInfo.getProgressPercent() + "%");
    }

    @Override
    public void onStart(@NotNull UploadInfo uploadInfo, int i, @NotNull UploadNotificationConfig uploadNotificationConfig) {
        Log.e(TAG, "UPLOAD onStart: ");
    }

    @Override
    public void onSuccess(@NotNull UploadInfo uploadInfo, int i, @NotNull UploadNotificationConfig uploadNotificationConfig, @NotNull ServerResponse serverResponse) {
        Log.e(TAG, "UPLOAD onSuccess: ");
    }*/


    /*protected UploadNotificationConfig getNotificationConfig(final String uploadId, @StringRes int title) {
        PendingIntent clickIntent = PendingIntent.getActivity(
                getActivity(), 1, new Intent(getContext(), SplashActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

        final boolean autoClear = false;
        final Bitmap largeIcon = null;
        final boolean clearOnAction = true;
        final boolean ringToneEnabled = true;
        final ArrayList<UploadNotificationAction> noActions = new ArrayList<>(1);

        final UploadNotificationAction cancelAction = new UploadNotificationAction(
                R.drawable.ic_location_24dp,
                "cancel upload",
                ContextExtensionsKt.getCancelUploadIntent(getActivity(), uploadId)
        );

        final ArrayList<UploadNotificationAction> progressActions = new ArrayList<>(1);
        progressActions.add(cancelAction);

        UploadNotificationStatusConfig progress = new UploadNotificationStatusConfig(
                getString(title),
                "Uploading",
                R.mipmap.ic_launcher,
                Color.BLUE,
                largeIcon,
                clickIntent,
                progressActions,
                clearOnAction,
                autoClear
        );

        UploadNotificationStatusConfig success = new UploadNotificationStatusConfig(
                getString(title),
                "Upload success",
                R.mipmap.ic_launcher,
                Color.GREEN,
                largeIcon,
                clickIntent,
                noActions,
                clearOnAction,
                autoClear
        );

        UploadNotificationStatusConfig error = new UploadNotificationStatusConfig(
                getString(title),
                "Upload error",
                R.mipmap.ic_launcher,
                Color.RED,
                largeIcon,
                clickIntent,
                noActions,
                clearOnAction,
                autoClear
        );

        UploadNotificationStatusConfig cancelled = new UploadNotificationStatusConfig(
                getString(title),
                "Upload cancelled",
                R.mipmap.ic_launcher,
                Color.YELLOW,
                largeIcon,
                clickIntent,
                noActions,
                clearOnAction
        );

        return new UploadNotificationConfig(ApplicationController.CHANNEL_ID, ringToneEnabled, progress, success, error, cancelled);
    }*/

    //Callback to listener if user is done with text editing
    public void setClickListener(DialogOTSUploadClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onProgress(Context context, UploadInfo uploadInfo) {

    }

    @Override
    public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
        showUploadingFileName("Request timeout. Please retry.", true);
        hideLoading();
        enableUploadButton();
        hideLoading();
    }

    @Override
    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
        Log.e(TAG, "UPLOAD onCompleted: ");

        //ImageData imageData = imageDataDao.queryBuilder().where(ImageDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), ImageDataDao.Properties.ImageCategory.eq(Constants.CATEGORY_REPORT_OTS), ImageDataDao.Properties.Id.eq(jobData.id)).unique();

        image.setSyncStatus(Constants.SYNC_COMPLETE);
        imageDataDao.update(image);

        imageDataList.remove(0);
        updateUploadingFileCount();

        uploadImages();
    }

    @Override
    public void onCancelled(Context context, UploadInfo uploadInfo) {

    }

    public interface DialogOTSUploadClickListener {
        void OnUploadCompleteListener();
    }
}
