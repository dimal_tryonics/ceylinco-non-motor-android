package com.tryonics.ceylincononmotor.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.adapter.CausesListAdapter;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.FormData;
import com.tryonics.ceylincononmotor.database.dao.FormDataDao;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.databinding.ActivityFormBinding;
import com.tryonics.ceylincononmotor.model.cause.Cause;
import com.tryonics.ceylincononmotor.model.option.ImageCategory;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class FormActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = FormActivity.class.getName();

    private static final String DATE_EXTRA = "date_extra";
    private static final int DATE_EXTRA_SURVEY = 100;
    private static final int DATE_EXTRA_LOSS = 102;
    private ArrayList<Cause> causesList = new ArrayList<>();
    private ActivityFormBinding binding;
    private JobData jobData;
    private ImageCategory imageCategory;
    private Cause selectedCause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFormBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        getBundleData();
        createCausesList();
        getExistingFormData();

        binding.tvBtnDateSurvey.setOnClickListener(this);
        binding.tvBtnDateLoss.setOnClickListener(this);
        binding.etCause.setOnClickListener(this);
        binding.tvSubmit.setOnClickListener(this);

        binding.progressBar2.setVisibility(View.INVISIBLE);

    }

    private void getBundleData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobData = extras.getParcelable(Constants.EXTRA_JOB_DATA);
            imageCategory = extras.getParcelable(Constants.EXTRA_JOB_CATEGORY);
        }
    }

    private void createCausesList() {
        causesList.add(new Cause(1, "Fire"));
        causesList.add(new Cause(2, "Flood"));
        causesList.add(new Cause(3, "Burglary"));
        causesList.add(new Cause(4, "Lightning"));
        causesList.add(new Cause(5, "CST"));
        causesList.add(new Cause(6, "Impact"));
        causesList.add(new Cause(7, "Riot & Strike"));
        causesList.add(new Cause(8, "Malicious"));
        causesList.add(new Cause(9, "Bursting or Overflowing of Water Tank"));
        causesList.add(new Cause(10, "Food Spoilage"));
        causesList.add(new Cause(11, "Accidental"));
        causesList.add(new Cause(12, "Theft"));
        causesList.add(new Cause(13, "Fraud or Dishonesty"));
        causesList.add(new Cause(14, "Hold-Up"));
        causesList.add(new Cause(15, "Public Liability"));
        causesList.add(new Cause(16, "Electrical Fire"));
        causesList.add(new Cause(17, "Mechanical Breakdown"));
        causesList.add(new Cause(18, "Wear & Tear "));
        causesList.add(new Cause(19, "---- OTHER ----"));
    }

    private void showLossesDialog() {

        final Dialog dialog = new Dialog(FormActivity.this, R.style.dialog_light);
        dialog.setContentView(R.layout.dialog_losses_list);
        dialog.setCanceledOnTouchOutside(true);

        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        CausesListAdapter causesListAdapter = new CausesListAdapter(getApplicationContext(), causesList);
        recyclerView.setAdapter(causesListAdapter);
        causesListAdapter.setClickListener((view, position, cause) -> {

            selectedCause = cause;

            if (selectedCause.id == 19) {
                binding.etCause.setText(selectedCause.cause);
                binding.etCauseDetail.setText(selectedCause.causeDetail);
                binding.etCauseDetail.setEnabled(true);
            } else {
                binding.etCause.setText(selectedCause.cause);
                binding.etCauseDetail.setText("");
                binding.etCauseDetail.setEnabled(false);
            }

            dialog.dismiss();

        });

        dialog.show();
    }

    private void getExistingFormData() {
        DaoSession daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();

        FormDataDao formDataDao = daoSession.getFormDataDao();
        formDataDao.queryBuilder().LOG_VALUES = true;
        formDataDao.queryBuilder().LOG_SQL = true;

        Log.e(TAG, "getExistingFormData: JOB " + jobData.getSmsClaimNo());
        Log.e(TAG, "getExistingFormData: CATEGORY " + imageCategory.categoryId);

        FormData formData = formDataDao.queryBuilder().where(FormDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), FormDataDao.Properties.CategoryId.eq(imageCategory.categoryId)).unique();

        if (formData != null) {
            binding.etRiskLocation.setText(formData.getRiskLocation());
            binding.tvDateSurvey.setText(formData.getSurveyDate());
            binding.tvDateLoss.setText(formData.getLossDate());

            selectedCause = new Cause(formData.getCauseOfLossId(), formData.getCauseOfLoss(), formData.getCauseOfLossDetail());

            if (selectedCause.id == 19) {
                binding.etCause.setText(selectedCause.cause);
                binding.etCauseDetail.setText(selectedCause.causeDetail);
                binding.etCauseDetail.setEnabled(true);
            } else {
                binding.etCause.setText(selectedCause.cause);
                binding.etCauseDetail.setText("");
                binding.etCauseDetail.setEnabled(false);
            }

            binding.etClaimAmoun.setText(formData.getClaimAmount());

            if (formData.getSalvage() == Constants.FORM_FIELD_DEDUCT) {
                binding.rbDeduct.setChecked(true);
            } else if (formData.getSalvage() == Constants.FORM_FIELD_IGNORE) {
                binding.rbIgnore.setChecked(true);
            } else if (formData.getSalvage() == Constants.FORM_FIELD_COLLECT) {
                binding.rbCollect.setChecked(true);
            }

            binding.etNote.setText(formData.getNote());
            binding.etAdjustment.setText(formData.getClaimAdjustment());

        }
    }

    private void saveFormData() {

        binding.progressBar2.setVisibility(View.VISIBLE);

        DaoSession daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();
        FormData formData = new FormData();
        formData.setSmsClaimNo(jobData.getSmsClaimNo());
        formData.setCategoryId(imageCategory.categoryId);
        formData.setRiskLocation(binding.etRiskLocation.getText().toString());
        formData.setSurveyDate(binding.tvDateSurvey.getText().toString());
        formData.setLossDate(binding.tvDateLoss.getText().toString());
        formData.setClaimAmount(binding.etClaimAmoun.getText().toString());

        if(selectedCause.id == 19) {
            formData.setCauseOfLossId(selectedCause.id);
            formData.setCauseOfLoss(selectedCause.cause);
            formData.setCauseOfLossDetail(binding.etCauseDetail.getText().toString());
        }else{
            formData.setCauseOfLossId(selectedCause.id);
            formData.setCauseOfLoss(selectedCause.cause);
            formData.setCauseOfLossDetail("");
        }

        formData.setSalvage(getSelectedOption());
        formData.setNote(binding.etNote.getText().toString());
        formData.setClaimAdjustment(binding.etAdjustment.getText().toString());
        formData.setSyncStatus(Constants.SYNC_PENDING);

        FormDataDao formDataDao = daoSession.getFormDataDao();
        formDataDao.queryBuilder().LOG_VALUES = true;
        formDataDao.queryBuilder().LOG_SQL = true;
        formDataDao.insertOrReplace(formData);

        Log.e(TAG, "saveFormData: ID " + formData.getId());

        new Handler().postDelayed(() -> {
            finish();
        }, 300);


    }

    private int getSelectedOption() {

        if (binding.rbDeduct.isChecked()) {
            return Constants.FORM_FIELD_DEDUCT;
        }

        if (binding.rbIgnore.isChecked()) {
            return Constants.FORM_FIELD_IGNORE;
        }

        if (binding.rbCollect.isChecked()) {
            return Constants.FORM_FIELD_COLLECT;
        }

        return -1;
    }

    @Override
    public void onClick(View v) {


        if (v.getId() == binding.tvBtnDateSurvey.getId()) {
            showDatePicker(DATE_EXTRA_SURVEY);
        }

        if (v.getId() == binding.tvBtnDateLoss.getId()) {
            showDatePicker(DATE_EXTRA_LOSS);
        }

        if (v.getId() == binding.etCause.getId()) {
            showLossesDialog();
        }

        if (v.getId() == binding.tvSubmit.getId()) {

            if (validateForm()) {
                saveFormData();
            }
        }

    }

    private void showDatePicker(int extra) {

        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                FormActivity.this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        Bundle bundle = new Bundle();
        bundle.putInt(DATE_EXTRA, extra);
        dpd.setArguments(bundle);

        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getSupportFragmentManager(), "Datepickerdialog");


    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        int type = view.getArguments().getInt(DATE_EXTRA);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(calendar.getTime());

        if (type == DATE_EXTRA_SURVEY) {
            binding.tvDateSurvey.setText(dateString);
        } else if (type == DATE_EXTRA_LOSS) {
            binding.tvDateLoss.setText(dateString);
        }
    }

    private boolean validateForm() {

        boolean validation = false;

        //showSnackbar("ERROR : All fields are required to continue the process " + !binding.etClaimAmoun.getText().toString().isEmpty());

        if (!binding.etRiskLocation.getText().toString().isEmpty() &&
                !binding.tvDateSurvey.getText().toString().isEmpty() &&
                !binding.tvDateLoss.getText().toString().isEmpty() &&
                !binding.etClaimAmoun.getText().toString().isEmpty() &&
                !binding.etCause.getText().toString().isEmpty() &&
                getSelectedOption() != -1 &&
                !binding.etNote.getText().toString().isEmpty() &&
                !binding.etAdjustment.getText().toString().isEmpty()) {
            validation = true;
        } else {
            showSnackbar("ERROR : All fields are required to continue the process");
            validation = false;
        }

        return validation;

    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar
                .make(binding.root, message, Snackbar.LENGTH_LONG);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(this, R.color.snack_bar_bg));
        snackbar.show();
    }
}
