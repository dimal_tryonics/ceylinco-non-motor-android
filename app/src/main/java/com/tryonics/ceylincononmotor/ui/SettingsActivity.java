package com.tryonics.ceylincononmotor.ui;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.tryonics.ceylincononmotor.databinding.ActivitySettingsBinding;
import com.tryonics.ceylincononmotor.fragment.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {

    private ActivitySettingsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySettingsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(binding.nestedScrollView.getId(), new SettingsFragment())
                .commit();
    }

}