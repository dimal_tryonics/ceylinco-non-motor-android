package com.tryonics.ceylincononmotor.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tryonics.ceylincononmotor.adapter.InProgressAdapter;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.database.dao.JobDataDao;
import com.tryonics.ceylincononmotor.databinding.FragmentInprogressJobsBinding;
import com.tryonics.ceylincononmotor.tryonicsevents.InprogressJobEvent;
import com.tryonics.ceylincononmotor.tryonicsevents.RefreshClosedJobEvent;
import com.tryonics.ceylincononmotor.tryonicsevents.RefreshInprogressJobEvent;
import com.tryonics.ceylincononmotor.tryonicsevents.RemoveInprogressJobEvent;
import com.tryonics.ceylincononmotor.ui.ViewPagerActivity;
import com.tryonics.ceylincononmotor.ui.dialog.DialogDeleteJob;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;

/**
 * A simple {@link Fragment} subclass.
 */

public class InProgressListFragment extends Fragment implements InProgressAdapter.ItemClickListener {

    private static final String TAG = InProgressListFragment.class.getName();

    private FragmentInprogressJobsBinding binding;

    private InProgressAdapter inProgressJobAdapter;
    private DaoSession daoSession;

    private RecyclerView rvInProgress;
    private LinearLayout emptyView;
    private TextView tvRefresh;

    private List<JobData> jobDataList = new LinkedList<>();

    private OnInProgressFragmentListener mListener;

    static InProgressListFragment newInstance() {
        InProgressListFragment fragment = new InProgressListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentInprogressJobsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        daoSession = ((ApplicationController) getActivity().getApplicationContext()).getDaoSession();

        rvInProgress = binding.rvInProgress;
        emptyView = binding.emptyView;
        emptyView.setVisibility(View.GONE);
        tvRefresh = binding.tvRefresh;
        tvRefresh.setOnClickListener(v -> loadPendingJobs(Constants.FILTER_ALL));

        binding.tvClaimCount.setOnClickListener(v -> loadPendingJobs(Constants.FILTER_CLAIM));
        binding.tvUnderwritingCount.setOnClickListener(v -> loadPendingJobs(Constants.FILTER_UNDERWRITING));
        binding.tvAll.setOnClickListener(v -> loadPendingJobs(Constants.FILTER_ALL));
        binding.tvOSTCount.setOnClickListener(v -> loadPendingJobs(Constants.FILTER_OST));

        setupRecyclerViewLayout();


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void setupRecyclerViewLayout() {
        LinearLayoutManager llmTextTools = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvInProgress.setLayoutManager(llmTextTools);
        inProgressJobAdapter = new InProgressAdapter(getContext());
        inProgressJobAdapter.setClickListener(this);
        //rvInProgress.setAdapter(inProgressJobAdapter); //SET EMPTY ADAPTER

        //loadPendingJobs();
        binding.linearLayout2.setVisibility(View.GONE);
    }

    public void loadPendingJobs(int filterType) {

        /*if (!jobDataList.isEmpty()) {
            return;
        }*/

        if (filterType == Constants.FILTER_ALL) {
            jobDataList = daoSession.queryBuilder(JobData.class)
                    .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.INPROGRESS_JOB))
                    .orderDesc(JobDataDao.Properties.JobStartTime)
                    .list();


            updateJobCounter();

        } else if (filterType == Constants.FILTER_CLAIM) {
            jobDataList = daoSession.queryBuilder(JobData.class)
                    .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.INPROGRESS_JOB), JobDataDao.Properties.SmsJobType.eq(Constants.CLAIM))
                    .orderDesc(JobDataDao.Properties.JobStartTime)
                    .list();
        } else if (filterType == Constants.FILTER_UNDERWRITING) {
            jobDataList = daoSession.queryBuilder(JobData.class)
                    .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.INPROGRESS_JOB), JobDataDao.Properties.SmsJobType.eq(Constants.UNDERWRITING))
                    .orderDesc(JobDataDao.Properties.JobStartTime)
                    .list();
        } else if (filterType == Constants.FILTER_OST) {
            jobDataList = daoSession.queryBuilder(JobData.class)
                    .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.INPROGRESS_JOB), JobDataDao.Properties.OtsSyncStatus.eq(Constants.SYNC_COMPLETE))
                    .orderDesc(JobDataDao.Properties.JobStartTime)
                    .list();
        }

        if (jobDataList.isEmpty()) {
            showEmptyScreen(true);
        } else {

            showEmptyScreen(false);

            inProgressJobAdapter.addJobDataList(jobDataList);
            rvInProgress.setAdapter(inProgressJobAdapter);

        }


    }

    private void showEmptyScreen(boolean show) {
        if (show) {
            rvInProgress.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            rvInProgress.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    @SuppressLint("DefaultLocale")
    private void updateJobCounter() {

        if (jobDataList != null && jobDataList.size() > 0) {
            binding.linearLayout2.setVisibility(View.VISIBLE);
            updateJobCount();
        } else {
            binding.linearLayout2.setVisibility(View.GONE);
        }

    }

    @SuppressLint("DefaultLocale")
    private void updateJobCount() {

        int claimCount = 0;
        int underwritingCount = 0;
        int ostComplete = 0;

        for (JobData jobData : jobDataList) {
            if (jobData.getSmsJobType().equalsIgnoreCase(Constants.CLAIM)) {
                ++claimCount;
            } else if (jobData.getSmsJobType().equalsIgnoreCase(Constants.UNDERWRITING)) {
                ++underwritingCount;
            }
            if (jobData.getOtsSyncStatus() == Constants.SYNC_COMPLETE) {
                ++ostComplete;
            }
        }

        binding.tvClaimCount.setText(format("Claims %02d", claimCount));
        binding.tvUnderwritingCount.setText(format("Underwriting %02d", underwritingCount));
        binding.tvOSTCount.setText(format("OTS %02d/%02d", ostComplete, claimCount));
    }

    @Override
    public void onPendingItemClick(View view, int position, JobData jobData) {
        mListener.onInProgressJobClickListener(jobData);
    }

    @Override
    public void onPendingItemLongClick(View view, int position, JobData jobData) {

        DialogDeleteJob dialogDeleteJob = DialogDeleteJob.show((AppCompatActivity) getActivity(), jobData);
        dialogDeleteJob.setClickListener(new DialogDeleteJob.JobStart() {
            @Override
            public void onClose(DialogDeleteJob dialog, JobData jobData) {
                dialog.dismiss();
            }

            @Override
            public void onDelete(DialogDeleteJob dialog, JobData jobData) {
                daoSession.delete(jobData);
                dialog.dismiss();
                inProgressJobAdapter.removeJob(position);
                updateJobCounter();
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInProgressFragmentListener) {
            mListener = (OnInProgressFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RefreshInprogressJobEvent event) {
        RefreshInprogressJobEvent stickyEvent = EventBus.getDefault().removeStickyEvent(RefreshInprogressJobEvent.class);
        // Better check that an event was actually posted before
        if (stickyEvent != null) {
            loadPendingJobs(Constants.FILTER_ALL);
        }
    }

    // This method will be called when a MessageEvent is posted (in the UI thread for Toast)
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(InprogressJobEvent event) {

        new Handler().postDelayed(() -> {
            //TODO
            inProgressJobAdapter.addJobData(event.jobData);
            binding.rvInProgress.smoothScrollToPosition(0);
            //binding.rvInProgress.scrollToPosition(0);

            updateJobCounter();

        }, 300);


    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(RemoveInprogressJobEvent event) {

        RemoveInprogressJobEvent stickyEvent = EventBus.getDefault().removeStickyEvent(RemoveInprogressJobEvent.class);
        // Better check that an event was actually posted before
        if (stickyEvent != null) {
            Log.e(TAG, "RemoveInprogressJobEvent: stickyEvent != null");

            Log.e(TAG, "REMOVE IN-PROGRESS " + event.jobData.getSmsClaimNo());

            loadPendingJobs(Constants.FILTER_ALL);

            EventBus.getDefault().postSticky(new RefreshClosedJobEvent(event.jobData));
            ((ViewPagerActivity) Objects.requireNonNull(getActivity())).viewPager.setCurrentItem(2, true);

            /*new Handler().postDelayed(() -> {

                for (int i = 0; i < jobDataList.size(); i++) {
                    if (jobDataList.get(i).getSmsClaimNo().equalsIgnoreCase(event.jobData.getSmsClaimNo())) {
                        inProgressJobAdapter.removeJob(i);
                        updateJobCounter();
                    }
                }

            }, 500);

            new Handler().postDelayed(() -> {

                ((ViewPagerActivity) Objects.requireNonNull(getActivity())).viewPager.setCurrentItem(2, true);
                EventBus.getDefault().postSticky(new RefreshClosedJobEvent(event.jobData));
                loadPendingJobs();

            }, 1000);*/

        }


    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public interface OnInProgressFragmentListener {
        void onInProgressJobClickListener(JobData jobData);
    }


}
