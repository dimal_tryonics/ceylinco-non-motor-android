package com.tryonics.ceylincononmotor.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tryonics.ceylincononmotor.adapter.PendingAdapter;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.database.dao.JobDataDao;
import com.tryonics.ceylincononmotor.databinding.FragmentPendingJobsBinding;
import com.tryonics.ceylincononmotor.tryonicsevents.InprogressJobEvent;
import com.tryonics.ceylincononmotor.tryonicsevents.RefreshPendingJobEvent;
import com.tryonics.ceylincononmotor.ui.ViewPagerActivity;
import com.tryonics.ceylincononmotor.ui.dialog.DialogDeleteJob;
import com.tryonics.ceylincononmotor.ui.dialog.DialogStartJob;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.DateTimeModule;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static java.lang.String.*;

/**
 * A simple {@link Fragment} subclass.
 */

public class PendingListFragment extends Fragment implements PendingAdapter.ItemClickListener {

    private static final String TAG = PendingListFragment.class.getName();

    private FragmentPendingJobsBinding binding;

    private PendingAdapter pendingAdapter;
    private DaoSession daoSession;

    private RecyclerView rvPending;
    private LinearLayout emptyView;
    private TextView tvRefresh;

    private List<JobData> jobDataList = new LinkedList<>();

    static PendingListFragment newInstance() {
        PendingListFragment fragment = new PendingListFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPendingJobsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        daoSession = ((ApplicationController) getActivity().getApplicationContext()).getDaoSession();

        rvPending = binding.rvPending;
        emptyView = binding.emptyView;
        emptyView.setVisibility(View.GONE);
        tvRefresh = binding.tvRefresh;
        tvRefresh.setOnClickListener(v -> loadPendingJobs(Constants.FILTER_ALL));

        binding.tvClaimCount.setOnClickListener(v -> loadPendingJobs(Constants.FILTER_CLAIM));
        binding.tvUnderwritingCount.setOnClickListener(v -> loadPendingJobs(Constants.FILTER_UNDERWRITING));
        binding.tvAll.setOnClickListener(v -> loadPendingJobs(Constants.FILTER_ALL));

        setupRecyclerViewLayout();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void setupRecyclerViewLayout() {
        LinearLayoutManager llmTextTools = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvPending.setLayoutManager(llmTextTools);
        pendingAdapter = new PendingAdapter(getContext());
        pendingAdapter.setClickListener(this);

        binding.linearLayout2.setVisibility(View.GONE);

    }

    public void loadPendingJobs(int filterType) {

        long time = DateTimeModule.getCurrentTimeStamp();
        Log.e(TAG, "DATE TIME: " + time);

        String dateTime = DateTimeModule.getFormattedDateTimeByTimestamp(time);

        Log.e(TAG, "DATE TIME: " + dateTime);

        /*if (!jobDataList.isEmpty()) {
            return;
        }*/

        if (filterType == Constants.FILTER_ALL) {
            jobDataList = daoSession.queryBuilder(JobData.class)
                    .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.PENDING_JOB))
                    .list();
            updateJobCounter();
        } else if (filterType == Constants.FILTER_CLAIM) {
            jobDataList = daoSession.queryBuilder(JobData.class)
                    .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.PENDING_JOB), JobDataDao.Properties.SmsJobType.eq(Constants.CLAIM))
                    .orderDesc(JobDataDao.Properties.SmsReceivedTime)
                    .list();
        } else if (filterType == Constants.FILTER_UNDERWRITING) {
            jobDataList = daoSession.queryBuilder(JobData.class)
                    .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.PENDING_JOB), JobDataDao.Properties.SmsJobType.eq(Constants.UNDERWRITING))
                    .orderDesc(JobDataDao.Properties.SmsReceivedTime)
                    .list();
        }

        if (jobDataList.isEmpty()) {
            showEmptyScreen(true);
        } else {
            showEmptyScreen(false);

            pendingAdapter.addJobDataList(jobDataList);
            rvPending.setAdapter(pendingAdapter);

        }


    }

    private void showEmptyScreen(boolean show) {
        if (show) {
            rvPending.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            rvPending.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    @SuppressLint("DefaultLocale")
    private void updateJobCounter() {

        if (jobDataList != null && jobDataList.size() > 0) {
            binding.linearLayout2.setVisibility(View.VISIBLE);
            updateJobCount();
        } else {
            binding.linearLayout2.setVisibility(View.GONE);
        }

    }

    @SuppressLint("DefaultLocale")
    private void updateJobCount() {

        int claimCount = 0;
        int underwritingCount = 0;

        for (JobData jobData : jobDataList) {

            if (jobData.getSmsJobType().equalsIgnoreCase(Constants.CLAIM)) {
                ++claimCount;
            } else if (jobData.getSmsJobType().equalsIgnoreCase(Constants.UNDERWRITING)) {
                ++underwritingCount;
            }
        }

        binding.tvClaimCount.setText(format("Claims %02d", claimCount));
        binding.tvUnderwritingCount.setText(format("Underwriting %02d", underwritingCount));
    }

    @Override
    public void onPendingItemClick(View view, int position, JobData jobData) {

        DialogStartJob dialogStartJob = DialogStartJob.show((AppCompatActivity) getActivity(), jobData);
        dialogStartJob.setOnJobStartListener(new DialogStartJob.JobStart() {
            @Override
            public void onClose(DialogStartJob dialog, JobData jobData) {
                dialog.dismiss();
            }

            @Override
            public void onStart(DialogStartJob dialog, JobData jobData) {
                jobData.setCurrentJobStatus(Constants.INPROGRESS_JOB);
                jobData.setJobStartTime(DateTimeModule.getCurrentTimeStamp());
                daoSession.update(jobData);
                dialog.dismiss();
                pendingAdapter.removeJob(position);
                updateJobCounter();

                /*new Handler().postDelayed(() -> {

                    EventBus.getDefault().post(new InprogressJobEvent(jobData));
                    ((ViewPagerActivity) Objects.requireNonNull(getActivity())).viewPager.setCurrentItem(1);

                }, 1000);*/

                EventBus.getDefault().post(new InprogressJobEvent(jobData));
                ((ViewPagerActivity) Objects.requireNonNull(getActivity())).viewPager.setCurrentItem(1);
            }
        });

    }

    @Override
    public void onPendingItemLongClick(View view, int position, JobData jobData) {

        DialogDeleteJob dialogDeleteJob = DialogDeleteJob.show((AppCompatActivity) getActivity(), jobData);
        dialogDeleteJob.setClickListener(new DialogDeleteJob.JobStart() {
            @Override
            public void onClose(DialogDeleteJob dialog, JobData jobData) {
                dialog.dismiss();
            }

            @Override
            public void onDelete(DialogDeleteJob dialog, JobData jobData) {
                daoSession.delete(jobData);
                dialog.dismiss();
                pendingAdapter.removeJob(position);
                updateJobCounter();
            }
        });

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(RefreshPendingJobEvent event) {

        RefreshPendingJobEvent stickyEvent = EventBus.getDefault().removeStickyEvent(RefreshPendingJobEvent.class);
        // Better check that an event was actually posted before
        if (stickyEvent != null) {
            loadPendingJobs(Constants.FILTER_ALL);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
