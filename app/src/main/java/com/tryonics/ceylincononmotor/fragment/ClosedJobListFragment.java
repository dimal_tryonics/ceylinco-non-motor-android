package com.tryonics.ceylincononmotor.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tryonics.ceylincononmotor.adapter.ClosedJobAdapter;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.database.dao.JobDataDao;
import com.tryonics.ceylincononmotor.databinding.FragmentClosedJobsBinding;
import com.tryonics.ceylincononmotor.tryonicsevents.RefreshClosedJobEvent;
import com.tryonics.ceylincononmotor.ui.ViewPagerActivity;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */

public class ClosedJobListFragment extends Fragment implements ClosedJobAdapter.ItemClickListener {

    private static final String TAG = ClosedJobListFragment.class.getName();

    private FragmentClosedJobsBinding binding;

    private ClosedJobAdapter closedJobAdapter;
    private DaoSession daoSession;

    private TextView tvJobCount;
    private RecyclerView rvClosed;
    private LinearLayout emptyView;
    private TextView tvRefresh;

    private List<JobData> jobDataList = new LinkedList<>();

    //private OnClosedFragmentListener mListener;

    static ClosedJobListFragment newInstance() {
        ClosedJobListFragment fragment = new ClosedJobListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentClosedJobsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        daoSession = ((ApplicationController) getActivity().getApplicationContext()).getDaoSession();

        tvJobCount = binding.tvJobCount;
        rvClosed = binding.rvClosed;
        emptyView = binding.emptyView;
        emptyView.setVisibility(View.GONE);
        tvRefresh = binding.tvRefresh;
        tvRefresh.setOnClickListener(v -> loadClosedJobList());

        setupRecyclerViewLayout();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void setupRecyclerViewLayout() {
        LinearLayoutManager llmTextTools = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvClosed.setLayoutManager(llmTextTools);
        closedJobAdapter = new ClosedJobAdapter(getContext());
        closedJobAdapter.setOnItemClickListener(this);
        //rvInProgress.setAdapter(inProgressJobAdapter); //SET EMPTY ADAPTER

        //loadPendingJobs();
    }

    public void loadClosedJobList() {

       /* if (!jobDataList.isEmpty()) {
            return;
        }*/

        jobDataList = daoSession.queryBuilder(JobData.class)
                .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.CLOSED_JOB))
                .orderDesc(JobDataDao.Properties.JobClosedTime)
                .list();

        if (jobDataList.isEmpty()) {
            showEmptyScreen(true);
        } else {

            showEmptyScreen(false);

            closedJobAdapter.addJobDataList(jobDataList);
            rvClosed.setAdapter(closedJobAdapter);

        }

        updateJobCounter();

    }

    private void showEmptyScreen(boolean show) {
        if (show) {
            rvClosed.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            rvClosed.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    @SuppressLint("DefaultLocale")
    private void updateJobCounter() {

        if (jobDataList != null && jobDataList.size() > 0) {
            tvJobCount.setText(String.format("Closed (%02d)", closedJobAdapter.getItemCount()));
        } else {
            tvJobCount.setText("");
        }

    }


    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnClosedFragmentListener) {
            mListener = (OnClosedFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


   /* // This method will be called when a MessageEvent is posted (in the UI thread for Toast)
    @Subscribe( threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ClosedJobEvent event) {


        Toast.makeText(getActivity(), "ADD CLOSED JOB " + event.jobData.getSmsClaimNo(), Toast.LENGTH_SHORT).show();

        *//*new Handler().postDelayed(() -> {
            //TODO
            closedJobAdapter.addJobData(event.jobData);
            binding.rvClosed.smoothScrollToPosition(0);
            //binding.rvInProgress.scrollToPosition(0);

            updateJobCounter(++jobCounter);

        }, 300);*//*

    }*/

    // This method will be called when a MessageEvent is posted (in the UI thread for Toast)
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RefreshClosedJobEvent event) {

        RefreshClosedJobEvent stickyEvent = EventBus.getDefault().removeStickyEvent(RefreshClosedJobEvent.class);
        // Better check that an event was actually posted before
        if (stickyEvent != null) {
            //Toast.makeText(getActivity(), "Refresh CLOSED JOB" + event.jobData.getSmsClaimNo(), Toast.LENGTH_SHORT).show();
            loadClosedJobList();
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onItemReSyncListener(JobData jobData) {

        //Toast.makeText(getActivity(), "Re-Sync", Toast.LENGTH_SHORT).show();

        JobDataDao jobDataDao = daoSession.getJobDataDao();
        jobData.setSyncStatus(Constants.SYNC_PENDING);
        jobDataDao.update(jobData);

        ((ViewPagerActivity) Objects.requireNonNull(getActivity())).startUploadService();

        loadClosedJobList();
    }
}
