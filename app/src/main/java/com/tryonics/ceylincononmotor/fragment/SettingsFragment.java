package com.tryonics.ceylincononmotor.fragment;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import com.tryonics.ceylincononmotor.R;


public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings_preferences, rootKey);
    }
}