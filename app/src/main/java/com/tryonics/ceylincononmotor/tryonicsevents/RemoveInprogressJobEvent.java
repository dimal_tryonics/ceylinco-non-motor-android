package com.tryonics.ceylincononmotor.tryonicsevents;

import com.tryonics.ceylincononmotor.database.dao.JobData;

import org.greenrobot.eventbus.EventBus;

public class RemoveInprogressJobEvent {

    public final JobData jobData;

    public RemoveInprogressJobEvent(JobData jobData) {
        this.jobData = jobData;

        EventBus.getDefault().removeStickyEvent(jobData);
    }

}
