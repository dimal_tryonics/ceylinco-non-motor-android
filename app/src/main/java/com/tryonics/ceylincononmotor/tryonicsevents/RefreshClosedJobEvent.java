package com.tryonics.ceylincononmotor.tryonicsevents;

import com.tryonics.ceylincononmotor.database.dao.JobData;

public class RefreshClosedJobEvent {

    public final JobData jobData;

    public RefreshClosedJobEvent(JobData jobData) {
        this.jobData = jobData;
    }

}
