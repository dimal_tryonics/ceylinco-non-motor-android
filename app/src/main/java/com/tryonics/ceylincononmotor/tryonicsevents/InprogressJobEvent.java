package com.tryonics.ceylincononmotor.tryonicsevents;

import com.tryonics.ceylincononmotor.database.dao.JobData;

public class InprogressJobEvent {

    public final JobData jobData;

    public InprogressJobEvent(JobData jobData) {
        this.jobData = jobData;
    }

}
