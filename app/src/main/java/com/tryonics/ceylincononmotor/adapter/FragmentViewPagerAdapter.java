package com.tryonics.ceylincononmotor.adapter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.tryonics.ceylincononmotor.fragment.ClosedJobListFragment;
import com.tryonics.ceylincononmotor.fragment.InProgressListFragment;
import com.tryonics.ceylincononmotor.fragment.PendingListFragment;

public class FragmentViewPagerAdapter extends FragmentStateAdapter {

    private static final String TAG = FragmentViewPagerAdapter.class.getName();

    private final String[] tabTitles = new String[]{"Pending", "In-Progress", "Closed"};

    public PendingListFragment pendingListFragment;
    public InProgressListFragment inProgressListFragment;
    public ClosedJobListFragment closedListFragment;

    public FragmentViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {

        Log.e(TAG, "Create Fragment: " + position );

        switch (position) {
            case 0:
                pendingListFragment =  new PendingListFragment();
                return pendingListFragment;
            case 1:
                inProgressListFragment =  new InProgressListFragment();
                return inProgressListFragment;
            case 2:
                closedListFragment =  new ClosedJobListFragment();
                return closedListFragment;
            default:
                throw new RuntimeException(this.toString() + " Wrong fragment!");
        }
    }



    @Override
    public int getItemCount() {
        return tabTitles.length;
    }





}