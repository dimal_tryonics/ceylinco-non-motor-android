package com.tryonics.ceylincononmotor.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.DateTimeModule;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by dimalperera on 7/30/14.
 */

public class ClosedJobAdapter extends RecyclerView.Adapter<ClosedJobAdapter.ViewHolder> {

    private static final String TAG = ClosedJobAdapter.class.getName();

    private Context context;
    private List<JobData> jobList = new LinkedList<>();
    private ItemClickListener mClickListener;


    public ClosedJobAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_closed, parent, false);
        return new ViewHolder(view);
    }
    Animation animation;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final JobData jobData = jobList.get(position);

        //TODO
        holder.tvJobReference.setText(jobData.getSmsJobRefNo());
        holder.tvClaimNo.setText(jobData.getSmsClaimNo());
        holder.tvJobType.setText(jobData.getSmsJobType());
        holder.tvDate.setText("" + jobData.getSmsJobCreateDate());
        holder.tvStartTime.setText(DateTimeModule.getFormattedDateTimeByTimestamp(jobData.getJobStartTime()));
        holder.tvClosedTime.setText(DateTimeModule.getFormattedDateTimeByTimestamp(jobData.getJobClosedTime()));

        if(jobData.getSyncStatus() == Constants.SYNC_COMPLETE){

            holder.imgStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check));

        } else if (jobData.getSyncStatus() == Constants.SYNC_FAIL) {

            holder.imgStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_sync_error));

        } else if(jobData.getSyncStatus() == Constants.SYNC_PENDING){

            holder.imgStatus.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_syncing));

            RotateAnimation rotate = new RotateAnimation(360, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(1000);
            rotate.setRepeatCount(Animation.INFINITE);
            rotate.setInterpolator(new LinearInterpolator());

            holder.imgStatus.setAnimation(rotate);

        }

        if (jobData.getSmsJobType().equalsIgnoreCase(Constants.CLAIM)) {
            holder.tvJobType.setBackground(context.getDrawable(R.drawable.bg_tag_claim));
        } else if (jobData.getSmsJobType().equalsIgnoreCase(Constants.UNDERWRITING)){
            holder.tvJobType.setBackground(context.getDrawable(R.drawable.bg_tag_underwriting));
        }

        holder.imgStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e(TAG, "onClick:Sync Status " +jobData.getSyncStatus() );

                if (jobData.getSyncStatus() == Constants.SYNC_FAIL) {
                    mClickListener.onItemReSyncListener(jobData);
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    public void removeJob(int position) {
        jobList.remove(position);
        notifyItemRemoved(position);
    }

    public void addJobDataList(List<JobData> jobDataList) {
        this.jobList = jobDataList;
    }

    public void appendPlaceList(ArrayList<JobData> jobData) {
        jobList = new ArrayList<JobData>();
        jobList.addAll(jobData);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void addJobData(JobData jobData) {
        jobList.add(0, jobData);
        //notifyItemInserted(0);
        notifyItemChanged(0);
    }

    public interface ItemClickListener {
        void onItemReSyncListener(JobData jobData);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvJobReference;
        private TextView tvClaimNo;
        private TextView tvJobType;
        private TextView tvDate;
        private TextView tvStartTime;
        private TextView tvClosedTime;
        private ImageView imgStatus;

        ViewHolder(View itemView) {
            super(itemView);
            tvJobReference = itemView.findViewById(R.id.tvJobReference);
            tvClaimNo = itemView.findViewById(R.id.tvClaimNo);
            tvJobType = itemView.findViewById(R.id.tvJobType);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvStartTime = itemView.findViewById(R.id.tvStartTime);
            tvClosedTime = itemView.findViewById(R.id.tvClosedTime);
            imgStatus = itemView.findViewById(R.id.imgStatus);

        }

    }
}
