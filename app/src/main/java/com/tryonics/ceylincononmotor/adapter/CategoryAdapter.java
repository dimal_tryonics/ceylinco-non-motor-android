package com.tryonics.ceylincononmotor.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.model.option.ImageCategory;
import com.tryonics.ceylincononmotor.util.Constants;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by dimalperera on 7/30/14.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private static final String TAG = CategoryAdapter.class.getName();

    private Context context;
    private List<ImageCategory> imageCategoryList;
    private ItemClickListener mClickListener;

    private JobData jobData;


    public CategoryAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_option, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ImageCategory imageCategory = imageCategoryList.get(position);

        holder.imgOption.setImageDrawable(ContextCompat.getDrawable(context, imageCategory.categoryImage));
        holder.tvOptionName.setText(imageCategory.categoryName);

        Log.e(TAG, "onBindViewHolder: "+ jobData.getOtsSyncStatus() + "   " + imageCategory.categoryId);

        if(jobData.getOtsSyncStatus() == Constants.SYNC_COMPLETE && imageCategory.categoryId == Constants.CATEGORY_REPORT_OTS){
            holder.tvOTSUploadStatus.setVisibility(View.VISIBLE);
        }else{
            holder.tvOTSUploadStatus.setVisibility(View.GONE);
        }


        if (imageCategory.completeStatus) {
            holder.imgOptionStatus.setImageResource(R.drawable.ic_check_24dp);
        } else {
            holder.imgOptionStatus.setImageResource(R.drawable.ic_uncheck_24dp);
        }

    }

    @Override
    public int getItemCount() {
        return imageCategoryList.size();
    }

    public void removeJob(int position) {
        imageCategoryList.remove(position);
        notifyItemRemoved(position);
    }

    public void addJobDataList(JobData jobData, List<ImageCategory> imageCategoryList) {
        this.jobData = jobData;
        this.imageCategoryList = imageCategoryList;
    }

    public void appendPlaceList(ArrayList<ImageCategory> imageCategory) {
        imageCategoryList = new ArrayList<ImageCategory>();
        imageCategoryList.addAll(imageCategory);
        notifyDataSetChanged();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onOptionItemClick(View view, int position, ImageCategory imageCategory);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgOption;
        private TextView tvOptionName;
        private TextView tvOTSUploadStatus;
        private ImageView imgOptionStatus;

        ViewHolder(View itemView) {
            super(itemView);
            imgOption = itemView.findViewById(R.id.imgOption);
            tvOptionName = itemView.findViewById(R.id.tvOptionName);
            tvOTSUploadStatus = itemView.findViewById(R.id.tvOTSUploadStatus);
            imgOptionStatus = itemView.findViewById(R.id.imgOptionStatus);

            itemView.setOnClickListener(view -> {
                if (mClickListener != null) {
                    mClickListener.onOptionItemClick(view, getAdapterPosition(), imageCategoryList.get(getAdapterPosition()));
                } else {
                    Log.e(TAG, "Please implement click listener");
                }
            });

        }

    }
}
