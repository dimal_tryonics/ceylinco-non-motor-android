package com.tryonics.ceylincononmotor.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.util.Constants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by dimalperera on 7/30/14.
 */

public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.ViewHolder> {

    private static final String TAG = PendingAdapter.class.getName();

    Context context;
    List<JobData> jobList;
    private ItemClickListener mClickListener;


    public PendingAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pending, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final JobData jobData = jobList.get(position);

        holder.tvJobReference.setText(jobData.getSmsJobRefNo());
        holder.tvClaimNo.setText(jobData.getSmsClaimNo());
        holder.tvJobType.setText(jobData.getSmsJobType());
        holder.tvDate.setText(""+jobData.getSmsJobCreateDate());

        if (jobData.getSmsJobType().equalsIgnoreCase(Constants.CLAIM)) {
            holder.tvJobType.setBackground(context.getDrawable(R.drawable.bg_tag_claim));
        } else if (jobData.getSmsJobType().equalsIgnoreCase(Constants.UNDERWRITING)){
            holder.tvJobType.setBackground(context.getDrawable(R.drawable.bg_tag_underwriting));
        }

    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    public void removeJob(int position) {
        jobList.remove(position);
        notifyItemRemoved(position);
    }

    public void addJobDataList(List<JobData> jobDataList) {
        this.jobList = jobDataList;
    }

    public void appendPlaceList(ArrayList<JobData> jobData) {
        jobList = new ArrayList<JobData>();
        jobList.addAll(jobData);
        notifyDataSetChanged();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onPendingItemClick(View view, int position, JobData jobData);
        void onPendingItemLongClick(View view, int position, JobData jobData);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvJobReference;
        private TextView tvClaimNo;
        private TextView tvJobType;
        private TextView tvDate;

        ViewHolder(View itemView) {
            super(itemView);
            tvJobReference = itemView.findViewById(R.id.tvJobReference);
            tvClaimNo = itemView.findViewById(R.id.tvClaimNo);
            tvJobType = itemView.findViewById(R.id.tvJobType);
            tvDate = itemView.findViewById(R.id.tvDate);
            itemView.setOnClickListener(view -> {
                if (mClickListener != null) {
                    mClickListener.onPendingItemClick(view, getAdapterPosition(), jobList.get(getAdapterPosition()));
                }else{
                    Log.e(TAG, "Please implement click listener");
                }
            });

            itemView.setOnLongClickListener(view -> {

                if (mClickListener != null) {
                    mClickListener.onPendingItemLongClick(view, getAdapterPosition(), jobList.get(getAdapterPosition()));
                }else{
                    Log.e(TAG, "Please implement click listener");
                }
                return true;
            });
        }

    }
}
