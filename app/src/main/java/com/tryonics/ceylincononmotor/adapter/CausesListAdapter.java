package com.tryonics.ceylincononmotor.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.model.cause.Cause;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by dimalperera on 7/30/14.
 */

public class CausesListAdapter extends RecyclerView.Adapter<CausesListAdapter.ViewHolder> {

    private static final String TAG = CausesListAdapter.class.getName();

    Context context;
    ArrayList<Cause> causeList;
    private ItemClickListener mClickListener;

    public CausesListAdapter(Context context, ArrayList<Cause> causeList) {
        this.context = context;
        this.causeList = causeList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cause, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvCause.setText(causeList.get(position).cause);
    }

    @Override
    public int getItemCount() {
        return causeList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onPendingItemClick(View view, int position, Cause lossesType);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCause;

        ViewHolder(View itemView) {
            super(itemView);
            tvCause = itemView.findViewById(R.id.tvCause);
            itemView.setOnClickListener(view -> {
                if (mClickListener != null) {
                    mClickListener.onPendingItemClick(view, getAdapterPosition(), causeList.get(getAdapterPosition()));
                } else {
                    Log.e(TAG, "Please implement click listener");
                }
            });
        }

    }
}
