package com.tryonics.ceylincononmotor.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.DateTimeModule;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by dimalperera on 7/30/14.
 */

public class InProgressAdapter extends RecyclerView.Adapter<InProgressAdapter.ViewHolder> {

    private static final String TAG = InProgressAdapter.class.getName();

    private Context context;
    private List<JobData> jobList = new LinkedList<>();
    private ItemClickListener mClickListener;


    public InProgressAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inprogress, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final JobData jobData = jobList.get(position);

        //TODO
        holder.tvJobReference.setText(jobData.getSmsJobRefNo());
        holder.tvClaimNo.setText(jobData.getSmsClaimNo());
        holder.tvJobType.setText(jobData.getSmsJobType());
        holder.tvDate.setText("" + jobData.getSmsJobCreateDate());

        holder.tvStartTime.setText(DateTimeModule.getFormattedDateTimeByTimestamp(jobData.getJobStartTime()));

        if (jobData.getSmsJobType().equalsIgnoreCase(Constants.CLAIM)) {
            holder.tvJobType.setBackground(context.getDrawable(R.drawable.bg_tag_claim));
        } else if (jobData.getSmsJobType().equalsIgnoreCase(Constants.UNDERWRITING)) {
            holder.tvJobType.setBackground(context.getDrawable(R.drawable.bg_tag_underwriting));
        }


        if (jobData.getOtsSyncStatus() == Constants.SYNC_COMPLETE) {
            holder.ostLayout.setVisibility(View.VISIBLE);
        } else {
            holder.ostLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    public void removeJob(int position) {
        jobList.remove(position);
        notifyItemRemoved(position);
    }

    public void addJobDataList(List<JobData> jobDataList) {
        this.jobList = jobDataList;
    }

    public void appendPlaceList(ArrayList<JobData> jobData) {
        jobList = new ArrayList<JobData>();
        jobList.addAll(jobData);
        notifyDataSetChanged();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void addJobData(JobData jobData) {
        jobList.add(0, jobData);
        //notifyItemInserted(0);
        notifyItemChanged(0);
    }

    public interface ItemClickListener {
        void onPendingItemClick(View view, int position, JobData jobData);

        void onPendingItemLongClick(View view, int position, JobData jobData);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvJobReference;
        private TextView tvClaimNo;
        private TextView tvJobType;
        private TextView tvDate;
        private TextView tvStartTime;
        private LinearLayout ostLayout;

        ViewHolder(View itemView) {
            super(itemView);
            tvJobReference = itemView.findViewById(R.id.tvJobReference);
            tvClaimNo = itemView.findViewById(R.id.tvClaimNo);
            tvJobType = itemView.findViewById(R.id.tvJobType);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvStartTime = itemView.findViewById(R.id.tvStartTime);
            ostLayout = itemView.findViewById(R.id.ostLayout);

            itemView.setOnClickListener(view -> {
                if (mClickListener != null) {
                    mClickListener.onPendingItemClick(view, getAdapterPosition(), jobList.get(getAdapterPosition()));
                } else {
                    Log.e(TAG, "Please implement click listener");
                }
            });

            itemView.setOnLongClickListener(view -> {

                if (mClickListener != null) {
                    mClickListener.onPendingItemLongClick(view, getAdapterPosition(), jobList.get(getAdapterPosition()));
                } else {
                    Log.e(TAG, "Please implement click listener");
                }
                return true;
            });
        }

    }
}
