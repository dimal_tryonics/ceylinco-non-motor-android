package com.tryonics.ceylincononmotor.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.database.dao.ImageData;
import com.tryonics.ceylincononmotor.util.image.GlideApp;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by dimalperera on 7/30/14.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {

    private static final String TAG = PhotoAdapter.class.getName();

    Context context;
    List<ImageData> imageDataList = new LinkedList<>();
    private ItemClickListener mClickListener;
    private boolean checkBoxVisibility = false;


    public PhotoAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_photo_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ImageData ImageData = imageDataList.get(position);

        Log.e(TAG, "onBindViewHolder: " + ImageData.getImageCategory() );

        GlideApp.with(context)
                .load(ImageData.getImageUrl())
                .override(200, 200)
                .into(holder.imgView);

        if(checkBoxVisibility){
            holder.checkBox.setVisibility(View.VISIBLE);
        }else{
            holder.checkBox.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return imageDataList.size();
    }

    public void removeJob(int position) {
        imageDataList.remove(position);
        notifyItemRemoved(position);
    }

    public void addImageDataList(List<ImageData> ImageDataList) {
        this.imageDataList = ImageDataList;
    }

    public void addImageData(ImageData imageData) {
        this.imageDataList.add(imageData);
        notifyDataSetChanged();
    }

    public void appendPlaceList(ArrayList<ImageData> ImageData) {
        imageDataList = new ArrayList<ImageData>();
        imageDataList.addAll(ImageData);
        notifyDataSetChanged();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }



    public interface ItemClickListener {
        void onImageItemClick(View view, int position, ImageData ImageData);
        void onImageItemLongClick(View view, int position, ImageData ImageData);
    }

    public void setActivePhotoRemove(boolean value) {
        checkBoxVisibility = value;
        notifyDataSetChanged();
    }

    public boolean isActivePhotoRemove(){
        return checkBoxVisibility;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgView;
        private CheckBox checkBox;


        ViewHolder(View itemView) {
            super(itemView);
            imgView = itemView.findViewById(R.id.imgView);
            checkBox = itemView.findViewById(R.id.checkBox);

            itemView.setOnClickListener(view -> {
                if (mClickListener != null) {
                    if(isActivePhotoRemove()){
                        if(checkBox.isChecked()){
                            checkBox.setChecked(false);
                        }else{
                            checkBox.setChecked(true);
                        }
                    }
                    mClickListener.onImageItemClick(view, getAdapterPosition(), imageDataList.get(getAdapterPosition()));
                } else {
                    Log.e(TAG, "Please implement click listener");
                }
            });

            itemView.setOnLongClickListener(view -> {

                if (mClickListener != null) {
                    mClickListener.onImageItemLongClick(view, getAdapterPosition(), imageDataList.get(getAdapterPosition()));
                } else {
                    Log.e(TAG, "Please implement click listener");
                }
                return true;
            });
        }

    }
}
