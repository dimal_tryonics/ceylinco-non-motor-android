package com.tryonics.ceylincononmotor.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.apimanager.ApiInterface;
import com.tryonics.ceylincononmotor.apimanager.RxRestEngine;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.FormData;
import com.tryonics.ceylincononmotor.database.dao.FormDataDao;
import com.tryonics.ceylincononmotor.database.dao.ImageData;
import com.tryonics.ceylincononmotor.database.dao.ImageDataDao;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.database.dao.JobDataDao;
import com.tryonics.ceylincononmotor.database.dao.LocationData;
import com.tryonics.ceylincononmotor.database.dao.LocationDataDao;
import com.tryonics.ceylincononmotor.model.CommonResponse;
import com.tryonics.ceylincononmotor.model.ImageUploadCompleteResponse;
import com.tryonics.ceylincononmotor.model.jobinit.JobInitResponse;
import com.tryonics.ceylincononmotor.tryonicsevents.RefreshClosedJobEvent;
import com.tryonics.ceylincononmotor.ui.SplashActivity;
import com.tryonics.ceylincononmotor.util.AppPreferences;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.DateTimeModule;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import timber.log.Timber;

public class UploadService extends Service {

    private final static String TAG_SYNC_METHOD = "TAG_SYNC_METHOD";
    private final static String TAG = UploadService.class.getName();

    private ApiInterface restEngine;

    //private JobDataDao jobDataDao;
    private DaoSession daoSession;
    private ImageDataDao imageDataDao;
    //private FormDataDao formDataDao;
    private LocationDataDao locationDataDao;

    //private JobData jobData;
    private ImageData imageData;
    private LocationData locationData;

    private AppPreferences appPreferences;

    //private UploadServiceSingleBroadcastReceiver uploadReceiver;
    private JobData jobData;
    private JobDataDao jobDataDao;

    public UploadService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {

        appPreferences = new AppPreferences(getApplicationContext());
        restEngine = RxRestEngine.getInstance().getService(getApplicationContext());
        daoSession = ((ApplicationController) getApplicationContext()).getDaoSession();

        //uploadReceiver = new UploadServiceSingleBroadcastReceiver(this);

        Intent notificationIntent = new Intent(this, SplashActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, ApplicationController.CHANNEL_ID)
                .setContentTitle("CNM upload service has started")
                .setContentText("Uploading pending data and images to server. Please wait until upload complete.")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        //new GlobalRequestObserver(getApplication(), this);
        //uploadReceiver.register(this);

        startJobSync();


        return super.onStartCommand(intent, flags, startId);
    }

    private void startJobSync() {

        try {

            jobDataDao = daoSession.getJobDataDao();

            jobData = jobDataDao.queryBuilder()
                    .where(JobDataDao.Properties.CurrentJobStatus.eq(Constants.CLOSED_JOB),
                            JobDataDao.Properties.SyncStatus.eq(Constants.SYNC_PENDING))
                    .orderDesc(JobDataDao.Properties.JobStartTime).limit(1)
                    .unique();

            // Log.e(TAG_SYNC_METHOD, "START JOB SYNC " + jobData.getSmsClaimNo());

            if (jobData != null) {

                if (Constants.SYNC_LEVEL_UNTOUCH == jobData.getJobSyncLevel()) {
                    Timber.i("Job init (Start) %s", jobData.getJobId());
                    dataUpload_init(jobData, jobDataDao); //DO INIT SYNC
                } else if (Constants.SYNC_LEVEL_INIT == jobData.getJobSyncLevel()) {
                    Timber.i("Job image sync (Start)");
                    //START IMAGE SYNC
                    imageDataDao = daoSession.getImageDataDao();
                    List<ImageData> imageDataList = imageDataDao.queryBuilder().where(ImageDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), ImageDataDao.Properties.SyncStatus.eq(Constants.SYNC_PENDING)).list();
                    Timber.i("Job image sync (Array count) %s", imageDataList.size());
                    if (imageDataList.size() > 0) {
                        imageData = imageDataList.get(0);
                        Timber.i("Job image sync (Continue)" + imageData.getSmsClaimNo() + " " + imageData.getImageUrl());
                        uploadBinary(jobData, imageData, imageDataDao, jobDataDao);
                    } else {
                        Timber.i("Job image sync (No images to sync)");
                        jobData.setJobSyncLevel(Constants.SYNC_LEVEL_PHOTOS);
                        jobDataDao.update(jobData);
                        startJobSync();
                    }

                } else if (Constants.SYNC_LEVEL_PHOTOS == jobData.getJobSyncLevel()) {
                    Timber.i("Job data sync (Start)");
                    FormDataDao formDataDao = daoSession.getFormDataDao();
                    List<FormData> formDataList = formDataDao.queryBuilder().where(FormDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), FormDataDao.Properties.SyncStatus.eq(Constants.SYNC_PENDING)).list();

                    //Log.e(TAG_SYNC_METHOD, "Form Data List SIZE: " + formDataList.size());

                    if (formDataList.size() > 0) {
                        //Log.e(TAG_SYNC_METHOD, "Form Data List SIZE: > 0 " );
                        FormData formData = formDataList.get(0);
                        Timber.i("Job form data sync (Continue)" + formData.getSmsClaimNo() + " " + formData.getCategoryId());
                        uploadFormData(jobData, formData, formDataDao, jobDataDao);
                    } else {
                        Timber.i("Job form data sync (No form data to sync)");
                        jobData.setJobSyncLevel(Constants.SYNC_LEVEL_FORM);
                        jobDataDao.update(jobData);
                        startJobSync();
                    }
                } else if (Constants.SYNC_LEVEL_FORM == jobData.getJobSyncLevel()) {
                    Timber.i("Job location data sync (Start)");
                    locationDataDao = daoSession.getLocationDataDao();
                    locationData = locationDataDao.queryBuilder().where(LocationDataDao.Properties.SmsClaimNo.eq(jobData.getSmsClaimNo()), LocationDataDao.Properties.SyncStatus.eq(Constants.SYNC_PENDING)).unique();

                    if (locationData != null) {
                        Timber.i("Location data sync (Start)");
                        uploadLocationData(jobData, locationData, jobDataDao);
                    } else {
                        Timber.i("Location data sync (No location data to sync)");
                        setLocationStatusComplete(jobData, jobDataDao);
                    }

                } else if (Constants.SYNC_LEVEL_LOCATION == jobData.getJobSyncLevel()) {

                    Timber.i("Job sync complete (Stared)");
                    syncComplete(jobData, jobDataDao);
                }

            } else {
                Timber.i("0 jobs to sync");
                stopSelf();
            }

        } catch (Exception e) {

            Timber.i("Start job sync (Exception) %s", e.getMessage());

            if (jobData != null) {
                jobData.setSyncStatus(Constants.SYNC_FAIL);
                jobDataDao.update(jobData);
                showUploadErrorNotification(jobData);
            }
        }
    }


    @SuppressLint("CheckResult")
    private void dataUpload_init(JobData jobData, JobDataDao jobDataDao) {

        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        requestBodyMap.put("claim_no", convertValue(jobData.getSmsClaimNo()));
        requestBodyMap.put("job_ref_no", convertValue(jobData.getSmsJobRefNo()));
        requestBodyMap.put("job_type", convertValue(jobData.getSmsJobType()));
        requestBodyMap.put("imei", convertValue(appPreferences.getIMEINo()));
        requestBodyMap.put("job_receive_timestamp", convertValue(String.valueOf(jobData.getSmsReceivedTime())));
        requestBodyMap.put("job_start_timestamp", convertValue(String.valueOf(jobData.getJobStartTime())));
        requestBodyMap.put("job_originate", convertValue("1"));

        restEngine.jobInitialization(jobData.getSmsClaimNo(), requestBodyMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response<JobInitResponse>>() {
                    @Override
                    public void onNext(Response<JobInitResponse> jobInitResponseResponse) {

                        JobInitResponse response = jobInitResponseResponse.body();

                        if (jobInitResponseResponse.code() == 200) {
                            if (response.error != null) {
                                Timber.i("Job init (Error) %s", response.error.details);
                                jobData.setSyncStatus(Constants.SYNC_FAIL);
                                jobDataDao.update(jobData);
                                showUploadErrorNotification(jobData);
                            } else {
                                Timber.i("Job init (Success)");
                                jobData.setJobId(response.jobId);
                                jobData.setJobSyncLevel(Constants.SYNC_LEVEL_INIT);
                                jobDataDao.update(jobData);
                                startJobSync();
                            }
                        } else if (jobInitResponseResponse.code() == 401) {
                            Timber.i("Job init (Http code - 401)");
                            jobData.setSyncStatus(Constants.SYNC_FAIL);
                            jobDataDao.update(jobData);
                            showUploadErrorNotification(jobData);
                            stopSelf();
                        } else {
                            Timber.i("Job init (Http code - " + jobInitResponseResponse.code() + ")");
                            jobData.setSyncStatus(Constants.SYNC_FAIL);
                            jobDataDao.update(jobData);
                            showUploadErrorNotification(jobData);
                            startJobSync();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.i("Job init (Exception) %s", e.getMessage());
                        jobData.setSyncStatus(Constants.SYNC_FAIL);
                        jobDataDao.update(jobData);
                        showUploadErrorNotification(jobData);
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }


    public void uploadBinary(JobData jobData, ImageData imageData, ImageDataDao imageDataDao, JobDataDao jobDataDao) {
        try {

            String uploadId = UUID.randomUUID().toString();

            UploadNotificationConfig c = new UploadNotificationConfig();
            c.setRingToneEnabled(false);
            c.setNotificationChannelId("1234");
            c.setClearOnActionForAllStatuses(true);

            new MultipartUploadRequest(this, uploadId, Constants.BASE_URL + jobData.getJobId() + "/image_sync")
                    .setMethod("POST")
                    .addHeader("Accept", "application/json")
                    .addHeader("Authorization", appPreferences.getAuthorization())
                    .addFileToUpload(imageData.getImageUrl(), "image")
                    .addParameter("claim_no", jobData.getSmsClaimNo())
                    .addParameter("category_id", String.valueOf(imageData.getImageCategory()))
                    .addParameter("job_type", jobData.getSmsJobType())
                    .setNotificationConfig(c)
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(Context context, UploadInfo uploadInfo) {
                            Log.e(TAG, "UPLOAD onProgress: " + uploadInfo.getProgressPercent() + "%");
                        }

                        @Override
                        public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
                            //Timber.i("Image sync request (onError) %s", exception.getMessage());
                            Timber.i("Image sync request (onError)");
                            jobData.setSyncStatus(Constants.SYNC_FAIL);
                            jobDataDao.update(jobData);
                            showUploadErrorNotification(jobData);
                        }

                        @Override
                        public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {

                            try {

                                if (serverResponse.getHttpCode() == 200) {

                                    ImageUploadCompleteResponse dataSyncResponse = new ObjectMapper().readValue(serverResponse.getBodyAsString(), ImageUploadCompleteResponse.class);

                                    if (dataSyncResponse.error != null) {
                                        if (dataSyncResponse.error.code.equalsIgnoreCase("Unauthenticated")) {
                                            Timber.i("Image sync request (Error - Unauthenticated)");
                                            imageData.setSyncStatus(Constants.SYNC_FAIL);
                                            imageDataDao.update(imageData);
                                            showUploadErrorNotification(jobData);
                                            sendLogoutRequest();
                                            stopSelf();
                                        } else {
                                            Timber.i("Image sync request (Error) %s", dataSyncResponse.error.details) ;
                                            imageData.setSyncStatus(Constants.SYNC_FAIL);
                                            imageDataDao.update(imageData);
                                            showUploadErrorNotification(jobData);
                                            startJobSync();
                                        }

                                    } else {
                                        Timber.i("Image sync request (success) %s", imageData.getImageUrl());

                                        imageData.setSyncStatus(Constants.SYNC_COMPLETE);
                                        imageDataDao.update(imageData);
                                        startJobSync();

                                    }
                                } else if (serverResponse.getHttpCode() == 401) {
                                    Timber.i("Image sync request (Http code - 401)");
                                    imageData.setSyncStatus(Constants.SYNC_FAIL);
                                    imageDataDao.update(imageData);
                                    showUploadErrorNotification(jobData);
                                    stopSelf();
                                } else {
                                    Timber.i("Image sync request (Http code - " + serverResponse.getHttpCode() + ")");
                                    imageData.setSyncStatus(Constants.SYNC_FAIL);
                                    imageDataDao.update(imageData);
                                    showUploadErrorNotification(jobData);
                                    startJobSync();
                                }

                            } catch (Exception e) {
                                Timber.i("Image sync request (Exception) %s", e.getMessage());
                                imageData.setSyncStatus(Constants.SYNC_FAIL);
                                imageDataDao.update(imageData);
                                showUploadErrorNotification(jobData);
                                startJobSync();
                            }
                        }

                        @Override
                        public void onCancelled(Context context, UploadInfo uploadInfo) {

                        }
                    })
                    .startUpload();

        } catch (Exception e) {
            Timber.i("Data sync request (Exception) %s", e.getMessage());
            jobData.setSyncStatus(Constants.SYNC_FAIL);
            jobDataDao.update(jobData);
            showUploadErrorNotification(jobData);
        }
    }

    @SuppressLint("CheckResult")
    private void uploadFormData(JobData jobData, FormData formData, FormDataDao formDataDao, JobDataDao jobDataDao) {

        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        requestBodyMap.put("claim_no", convertValue(jobData.getSmsClaimNo()));
        requestBodyMap.put("category_id", convertValue(String.valueOf(formData.getCategoryId())));
        requestBodyMap.put("job_type", convertValue(jobData.getSmsJobType()));
        requestBodyMap.put("survey_date", convertValue(formData.getSurveyDate()));
        requestBodyMap.put("loss_date", convertValue(formData.getLossDate()));
        requestBodyMap.put("claim_amount", convertValue(formData.getClaimAmount()));
        requestBodyMap.put("cause_of_loss", convertValue(formData.getCauseOfLoss()));
        requestBodyMap.put("cause_of_loss_detail", convertValue(formData.getCauseOfLossDetail())); //NEW
        requestBodyMap.put("risk_location", convertValue(formData.getRiskLocation())); //NEW

        if (Constants.FORM_FIELD_DEDUCT == formData.getSalvage()) {
            requestBodyMap.put("salvage", convertValue(getString(R.string.form_deduct)));
        } else if (Constants.FORM_FIELD_IGNORE == formData.getSalvage()) {
            requestBodyMap.put("salvage", convertValue(getString(R.string.form_ignore)));
        } else if (Constants.FORM_FIELD_COLLECT == formData.getSalvage()) {
            requestBodyMap.put("salvage", convertValue(getString(R.string.form_collect)));
        }


        requestBodyMap.put("notes", convertValue(formData.getNote()));
        requestBodyMap.put("claim_adjustment", convertValue(formData.getClaimAdjustment()));

        restEngine.formDataSubmit(jobData.getJobId(), requestBodyMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response<CommonResponse>>() {
                    @Override
                    public void onNext(Response<CommonResponse> jobInitResponseResponse) {

                        CommonResponse response = jobInitResponseResponse.body();

                        if (jobInitResponseResponse.code() == 200) {

                            if (response != null) {
                                if (response.error != null) {
                                    if (response.error.code.equalsIgnoreCase("Unauthenticated")) {
                                        Timber.i("Data sync request (Error - Unauthenticated) %s", response.error.details);
                                        formData.setSyncStatus(Constants.SYNC_COMPLETE);
                                        formDataDao.update(formData);
                                        showUploadErrorNotification(jobData);
                                        sendLogoutRequest();
                                        stopSelf();
                                    } else {
                                        Timber.i("Data sync request (Error) %s", response.error.details) ;
                                        formData.setSyncStatus(Constants.SYNC_COMPLETE);
                                        formDataDao.update(formData);
                                        showUploadErrorNotification(jobData);
                                        startJobSync();
                                    }
                                } else {
                                    Timber.i("Data sync request (success)");
                                    formData.setSyncStatus(Constants.SYNC_COMPLETE);
                                    formDataDao.update(formData);
                                    startJobSync();
                                }
                            }else{
                                Timber.i("Data sync request (Response null)");
                                jobData.setSyncStatus(Constants.SYNC_FAIL);
                                jobDataDao.update(jobData);
                                showUploadErrorNotification(jobData);
                                startJobSync();
                            }
                        }else if (jobInitResponseResponse.code() == 401) {
                            Timber.i("Data sync request (Http code - 401)");
                            jobData.setSyncStatus(Constants.SYNC_FAIL);
                            jobDataDao.update(jobData);
                            showUploadErrorNotification(jobData);
                            stopSelf();
                        } else {
                            Timber.i("Data sync request (Http code - " + jobInitResponseResponse.code() + ")");
                            jobData.setSyncStatus(Constants.SYNC_FAIL);
                            jobDataDao.update(jobData);
                            showUploadErrorNotification(jobData);
                            startJobSync();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.i("Data sync request (Exception) %s", e.getMessage());
                        jobData.setSyncStatus(Constants.SYNC_FAIL);
                        jobDataDao.update(jobData);
                        showUploadErrorNotification(jobData);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @SuppressLint("CheckResult")
    private void uploadLocationData(JobData jobData, LocationData locationDetails, JobDataDao jobDataDao) {

        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        requestBodyMap.put("claim_no", convertValue(jobData.getSmsClaimNo()));
        requestBodyMap.put("category_id", convertValue(String.valueOf(Constants.CATEGORY_REPORT_LOCATION)));
        requestBodyMap.put("job_type", convertValue(jobData.getSmsJobType()));

        /*
        int type = locationDetails.getLocationType();
        if (type == Constants.LOCATION_TYPE_1) {
            requestBodyMap.put("location_type", convertValue(getString(R.string.flood_area)));
        } else if (type == Constants.LOCATION_TYPE_2) {
            requestBodyMap.put("location_type", convertValue(getString(R.string.not_a_flood_area)));
        }
        */

        requestBodyMap.put("lat", convertValue(String.valueOf(locationDetails.getLatitude())));
        requestBodyMap.put("long", convertValue(String.valueOf(locationDetails.getLongitude())));


        restEngine.formDataSubmit(jobData.getJobId(), requestBodyMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response<CommonResponse>>() {
                    @Override
                    public void onNext(Response<CommonResponse> jobInitResponseResponse) {

                        CommonResponse response = jobInitResponseResponse.body();

                        if (jobInitResponseResponse.code() == 200) {

                            if (response != null) {
                                if (response.error != null) {
                                    jobData.setSyncStatus(Constants.SYNC_FAIL);
                                    jobDataDao.update(jobData);
                                    showUploadErrorNotification(jobData);

                                    if (response.error.code.equalsIgnoreCase("Unauthenticated")) {
                                        Timber.i("Data sync request (Error - Unauthenticated) %s", response.error.details);
                                        jobData.setSyncStatus(Constants.SYNC_FAIL);
                                        jobDataDao.update(jobData);
                                        showUploadErrorNotification(jobData);
                                        sendLogoutRequest();
                                        stopSelf();
                                    } else {
                                        Timber.i("Data sync request (Error) %s", response.error.details) ;
                                        jobData.setSyncStatus(Constants.SYNC_FAIL);
                                        jobDataDao.update(jobData);
                                        showUploadErrorNotification(jobData);
                                        startJobSync();
                                    }

                                } else {
                                    Timber.i("Location data sync request (success)");
                                    locationDetails.setSyncStatus(Constants.SYNC_COMPLETE);
                                    locationDataDao.update(locationDetails);

                                    jobData.setJobSyncLevel(Constants.SYNC_LEVEL_LOCATION);
                                    jobDataDao.update(jobData);

                                    startJobSync();
                                }
                            }
                        }else if (jobInitResponseResponse.code() == 401) {
                            Timber.i("Location data sync request (Http code - 401)");
                            jobData.setSyncStatus(Constants.SYNC_FAIL);
                            jobDataDao.update(jobData);
                            showUploadErrorNotification(jobData);
                            stopSelf();
                        } else {
                            Timber.i("Location data sync request (Http code - " + jobInitResponseResponse.code() + ")");
                            jobData.setSyncStatus(Constants.SYNC_FAIL);
                            jobDataDao.update(jobData);
                            showUploadErrorNotification(jobData);
                            startJobSync();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.i("Location data sync request (Exception) %s", e.getMessage());
                        jobData.setSyncStatus(Constants.SYNC_FAIL);
                        jobDataDao.update(jobData);
                        showUploadErrorNotification(jobData);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void setLocationStatusComplete(JobData jobData, JobDataDao jobDataDao) {

        jobData.setJobSyncLevel(Constants.SYNC_LEVEL_LOCATION);
        jobDataDao.update(jobData);

        startJobSync();
    }

    @SuppressLint("CheckResult")
    private void syncComplete(JobData jobData, JobDataDao jobDataDao) {

        RequestBody empty = RequestBody.create(String.valueOf(jobData.getJobClosedTime()), MediaType.parse("multipart/form-data"));

        restEngine.jobDataUploadComplete(jobData.getJobId(), empty)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response<CommonResponse>>() {

                    @Override
                    protected void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onNext(Response<CommonResponse> jobInitResponseResponse) {

                        if (jobInitResponseResponse.code() == 200) {

                            CommonResponse response = jobInitResponseResponse.body();

                            if (response != null) {
                                if (response.error != null) {
                                    Timber.i("Job complete request (Error) %s", response.error.details) ;
                                    jobData.setSyncStatus(Constants.SYNC_FAIL);
                                    jobDataDao.update(jobData);
                                    showUploadErrorNotification(jobData);
                                } else {
                                    Timber.i("Job complete request (Success)");
                                    jobData.setJobSyncDoneTime(DateTimeModule.getCurrentTimeStamp());
                                    jobData.setJobSyncLevel(Constants.SYNC_LEVEL_LOCATION);
                                    jobData.setSyncStatus(Constants.SYNC_COMPLETE);
                                    jobDataDao.update(jobData);

                                    EventBus.getDefault().postSticky(new RefreshClosedJobEvent(jobData));

                                    showUploadSuccessNotification(jobData);
                                    startJobSync();
                                }
                            }else {
                                Timber.i("Job complete request (Response - null)");
                                jobData.setSyncStatus(Constants.SYNC_FAIL);
                                jobDataDao.update(jobData);
                                showUploadErrorNotification(jobData);
                            }

                        }else if (jobInitResponseResponse.code() == 401) {
                            Timber.i("Job complete request (Http code - 401)");
                            jobData.setSyncStatus(Constants.SYNC_FAIL);
                            jobDataDao.update(jobData);
                            showUploadErrorNotification(jobData);
                            stopSelf();
                        } else {
                            Timber.i("Job complete request (Http code - " + jobInitResponseResponse.code() + ")");
                            jobData.setSyncStatus(Constants.SYNC_FAIL);
                            jobDataDao.update(jobData);
                            showUploadErrorNotification(jobData);
                            startJobSync();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.i("Job complete request (Exception) %s", e.getMessage());
                        EventBus.getDefault().postSticky(new RefreshClosedJobEvent(jobData));

                        jobData.setSyncStatus(Constants.SYNC_FAIL);
                        jobDataDao.update(jobData);
                        showUploadErrorNotification(jobData);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    private RequestBody convertValue(String value) {
        if (value == null) {
            return RequestBody.create("", MediaType.parse("text/plain"));
        } else {
            return RequestBody.create(value, MediaType.parse("text/plain"));
        }
    }

    private void showUploadSuccessNotification(JobData jobData) {

        EventBus.getDefault().postSticky(new RefreshClosedJobEvent(jobData));


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), ApplicationController.CHANNEL_ID)
                // Sets whether notifications posted to this channel should display notification lights
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(android.provider.Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentTitle("Job successfully uploaded (Job ID : " + jobData.getSmsJobRefNo() + ")")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setTicker(getApplicationContext().getString(R.string.app_name));

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(0022, mBuilder.build());


    }

    private void showUploadErrorNotification(JobData jobData) {

        EventBus.getDefault().postSticky(new RefreshClosedJobEvent(jobData));

        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("RESTART", true);
        intent.putExtra("JOB_ID", jobData.getSmsJobRefNo());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), ApplicationController.CHANNEL_ID_FAIL)
                // Sets whether notifications posted to this channel should display notification lights
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(android.provider.Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentTitle("Job failed to upload (Job ID : " + jobData.getSmsJobRefNo() + ")")
                .setContentText("Please Tap to retry")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setTicker(getApplicationContext().getString(R.string.app_name));

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(001, mBuilder.build());


    }


    private void sendLogoutRequest(){
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                .getInstance(UploadService.this);
        localBroadcastManager.sendBroadcast(new Intent(
                "com.tryonics.csa_icms.service.logout"));

    }
}
