package com.tryonics.ceylincononmotor.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

public class NetworkConnection {

	public static final String NETWORK_CLASS_2G = "2G";
	public static final String NETWORK_CLASS_3G = "3G";
	public static final String NETWORK_CLASS_4G = "4G";
	public static final String NETWORK_WIFI = "WIFI";

	private Context context;
	private NetworkConnectionStatus networkConnectionStatus;

	public NetworkConnection(final Context applicationContext) {
		this.context = applicationContext;
	}

	private void isConnectingToInternet(final Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (NetworkInfo xx : info) {
					if (xx.getState() == NetworkInfo.State.CONNECTED) {
						networkConnectionStatus.connectionSuccessful();
						return;
					}
				}
			}
		}
		networkConnectionStatus.connectionFail();
	}

	public void setListener(
			final NetworkConnectionStatus networkConnectionStatus) {
		this.networkConnectionStatus = networkConnectionStatus;
		this.isConnectingToInternet(context);
	}

	public interface NetworkConnectionStatus {
		void connectionSuccessful();

		void connectionFail();
	}



	public static String getNetworkClass(Context context){

		ConnectivityManager cm =
				(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

		TelephonyManager tm =
				(TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();



		switch (activeNetwork.getType()) {

			case (ConnectivityManager.TYPE_WIFI):
				return NETWORK_WIFI;

			case (ConnectivityManager.TYPE_MOBILE): {

				//Logger.e("NETWORK CLASS " , " " + tm.getNetworkType());

				switch (tm.getNetworkType()) {

					case TelephonyManager.NETWORK_TYPE_GPRS:
					case TelephonyManager.NETWORK_TYPE_EDGE:
					case TelephonyManager.NETWORK_TYPE_CDMA:
					case TelephonyManager.NETWORK_TYPE_1xRTT:
					case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
						//PrefetchCacheSize /= 2;
						return NETWORK_CLASS_2G;

					case TelephonyManager.NETWORK_TYPE_UMTS:
					case TelephonyManager.NETWORK_TYPE_EVDO_0:
					case TelephonyManager.NETWORK_TYPE_EVDO_A:
					case TelephonyManager.NETWORK_TYPE_HSDPA:
					case TelephonyManager.NETWORK_TYPE_HSUPA:
					case TelephonyManager.NETWORK_TYPE_HSPA:
					case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
					case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
                    case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
						return NETWORK_CLASS_3G;

					case TelephonyManager.NETWORK_TYPE_LTE:
						return NETWORK_CLASS_4G;

					default:
						return "-";
				}
			}
			default:
				return "-";
		}

	}

}
