package com.tryonics.ceylincononmotor.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public final class AppPreferences {

	private static final String IMEI_NO = "imei_no";
	private static final String AUTHORIZATION = "Authorization";
	public static final String MODE_IDLE = "MODE_IDLE";
	public static final String IMEI_TIMESTAMP = "IMEI_TIMESTAMP";
	public static final String LAST_LOCATION = "LAST_LOCATION";
	public static final String JOB_ID = "JOB_ID";
    public static final String DUTY_STATUS = "duty_status";

	private static final String APP_SHARED_PREFS = AppPreferences.class.getSimpleName(); // Name of the file -.xml
	private SharedPreferences sharedPrefs;
	private Editor prefsEditor;

	public AppPreferences(Context context) {
		this.sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
		this.prefsEditor = sharedPrefs.edit();
	}

    public String getIMEINo() {

        String imei = sharedPrefs.getString(IMEI_NO, null);
        Log.e("AppPreferences ", "" + imei);
        return imei;
    }

	public void setIMEINo(String phoneNo) {

		removeIMEINo();

		prefsEditor.putString(IMEI_NO, phoneNo);
		prefsEditor.commit();
	}

	public void removeIMEINo() {
		prefsEditor.putString(IMEI_NO, null);
		prefsEditor.commit();
	}

	public Boolean isEmptyIMEINo() {
        if (getIMEINo() != null) {
            return true;
        } else {
            return false;
        }
    }

    public String getAuthorization() {
		String authorization = sharedPrefs.getString(AUTHORIZATION, "");
		//Log.e("AppPreferences", " AUTHORIZATION " + authorization);
		return authorization;
    }

	public void setAuthorization(String authorization) {

		removeAuthorization();

		prefsEditor.putString(AUTHORIZATION, authorization);
		prefsEditor.commit();
	}

	public void removeAuthorization() {
		prefsEditor.putString(AUTHORIZATION, null);
		prefsEditor.commit();
	}

	public Boolean isAuthorized() {
		if (!getAuthorization().isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	//Foreground related status
	public void putModeIdle(boolean mode) {

		prefsEditor.putBoolean(MODE_IDLE, mode);
		prefsEditor.commit();
	}

	public boolean getModeIdle() {
		return sharedPrefs.getBoolean(MODE_IDLE, false);

	}

	////

	///// SAVE IMEI AND TIMESTAMP /////

	public void putImeiNumberWithTimestamp(String imeiTimestamp) {
		prefsEditor.putString(IMEI_TIMESTAMP, imeiTimestamp);
		prefsEditor.commit();
	}

	public String getImeiNumberWithTimestamp() {
		return sharedPrefs.getString(IMEI_TIMESTAMP, null);
	}

	public void removeImeiNumberWithTimestamp() {
		prefsEditor.putString(IMEI_TIMESTAMP, null);
		prefsEditor.commit();
	}

	public boolean isImeiNumberWithTimestampAvailable() {
		if (getImeiNumberWithTimestamp() != null) {
			return true;
		} else {
			return false;
		}
	}

	///// LAST LOCATION /////

	public void putLastLocation(String lastLocation) {
		prefsEditor.putString(LAST_LOCATION, lastLocation);
		prefsEditor.commit();
	}

	public String getLastLocation() {
		return sharedPrefs.getString(LAST_LOCATION, null);
	}

	public void removeLastLocation() {
		prefsEditor.putString(LAST_LOCATION, null);
		prefsEditor.commit();
	}

	public boolean isLastLocationAvailable() {
		if (getLastLocation() != null) {
			return true;
		} else {
			return false;
		}
	}


	public void putJobId(String id) {

		prefsEditor.putString(JOB_ID, id);
		prefsEditor.commit();
	}

	public String getJobId() {
		return sharedPrefs.getString(JOB_ID, null);

	}

	public boolean isJobIdAvailable() {
		if (getJobId() != null) {
			return true;
		} else {
			return false;
		}
	}

	public void removeJobId() {
		prefsEditor.putString(JOB_ID, null);
		prefsEditor.commit();
	}


    ///// DUTY STATUS /////

    public void putDutyStatus(boolean dutyStatus) {
        prefsEditor.putBoolean(DUTY_STATUS, dutyStatus);
        prefsEditor.commit();
    }

    public boolean getDutyStatus() {
        return sharedPrefs.getBoolean(DUTY_STATUS, false);
    }

    public void removeDutyStatus() {
        prefsEditor.putBoolean(DUTY_STATUS, false);
        prefsEditor.commit();
    }

    public boolean isDutyStatusAvailable() {
        return getDutyStatus();
    }

}