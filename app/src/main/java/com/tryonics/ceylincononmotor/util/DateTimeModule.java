package com.tryonics.ceylincononmotor.util;

import android.text.format.DateUtils;
import android.util.Log;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dimal on 3/26/17.
 */

public class DateTimeModule {

    private static final String TAG = DateTimeModule.class.getName();

    private static String DATE_FORMAT = "dd/MM/yyyy hh:mm:ss";


    public static long getCurrentTimeStamp(){
        Calendar mCalendar = Calendar.getInstance();
        return mCalendar.getTimeInMillis() / 1000L;
    }

    public static String getFormattedDateTimeByTimestamp(long timestamp) {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp * 1000L);
        Date d = c.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);

        return formatter.format(d);
    }

    public static long getMilliFromDate(String dateFormat) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date = formatter.parse(dateFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Today is " + date);
        return date.getTime()/1000;
    }

    public static long getHoursDiff(long syncCompleteTime){


        long diff;
        long numOfDays = 0, hours = 0, minutes = 0, seconds = 0;

        try {

            Calendar currentDate = Calendar.getInstance();
            currentDate.setTimeInMillis(getCurrentTimeStamp() * 1000L);
            Date currentDateTime = currentDate.getTime();

            Calendar syncDate = Calendar.getInstance();
            syncDate.setTimeInMillis(syncCompleteTime * 1000L);
            Date syncDateTime = syncDate.getTime();

            Log.e(TAG , " : Current Time : "+ currentDateTime.getTime());
            Log.e(TAG , " : Complete Time : " + syncDateTime.getTime());


            diff =  currentDateTime.getTime() - syncDateTime.getTime();

            Log.e(TAG , " : Current diff : "+ diff);

            numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
            hours = (int) (diff / (1000 * 60 * 60));
            minutes = (int) (diff / (1000 * 60));
            seconds = (int) (diff / (1000));

            Log.e("DELETE JOB", " : Time DIFF : "+ " hours: " + hours + " minutes: " + minutes + " seconds: " + seconds);


        } catch (Exception e) {
            Log.e("ERROR", e.getMessage());
        }

        return hours;

    }



}
