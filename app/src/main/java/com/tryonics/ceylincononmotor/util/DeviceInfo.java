package com.tryonics.ceylincononmotor.util;

import android.os.Build;
import android.util.Log;

import static android.os.Build.BRAND;

/**
 * Created by Dimal on 5/3/18.
 */

public class DeviceInfo {


    /*
    SAMPLE PHONE DATA

    E/BRAND: Nokia
    E/DEVICE: ND1
    E/DISPLAY: 00WW_3_32A
    E/HARDWARE: qcom
    E/MODEL: TA-1053
    E/PRODUCT: HMD Global
    E/Release: 7.1.1
    E/SDK Version: 25

     */

    public static String getDeviceData(){

        String brand = BRAND;
        String manufacture = Build.MANUFACTURER;
        String model = Build.MODEL;

        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;

        /*Log.e("BRAND", brand);
        Log.e("MANUFACTURER", "" + manufacture);
        Log.e("MODEL", "" + model);
        Log.e("Release", release);
        Log.e("SDK Version", "" + sdkVersion);*/

        String result = brand +
                "#" +
                manufacture +
                "#" +
                model +
                "#" +
                release +
                "#" +
                String.valueOf(sdkVersion);

        Log.e("DEVICE INFO ", result);

        return result;
    }
}
