package com.tryonics.ceylincononmotor.util.file;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.tryonics.ceylincononmotor.database.dao.JobData;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TryonicsFileManager {

    private final static String TAG = TryonicsFileManager.class.getName();

    public static File createLocalImageFile(Context context) {

        try {

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

            String imageFileName = timeStamp + "_original";

            File storageDir = context.getFilesDir();

            File image = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir
            );

            return image;

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "createLocalImageFile: " + e.getMessage() );
            return null;
        }

    }

    public static File createFolderWithVehicleNumber(JobData jobData) {
//.getSmsClaimNo()
        try {
            String ROOT_FOLDER_NAME = "CSA_NON_ICMS";
            String JOB_FOLDER_NAME = jobData.getSmsClaimNo() + "-" +jobData.getSmsJobType();

            File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                    + File.separator + ROOT_FOLDER_NAME + File.separator + JOB_FOLDER_NAME);

            if (!folder.exists()) {
                folder.mkdirs();
            }

            File file = new File( folder.getAbsolutePath() + File.separator
                    + jobData.getSmsClaimNo() + "_csa_non_icms_" + System.currentTimeMillis() + ".jpg");
            file.createNewFile();

            return file;

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "createFolderWithVehicleNumber: " + e.getMessage() );
            return null;
        }
    }

    /*public static File createFolderWithVehicleNumber(String vehicleNumber) {

        try {
            String FOLDER_NAME = "CSA_NON_ICMS";

            File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                    + File.separator + FOLDER_NAME);

            if (!folder.exists()) {
                folder.mkdir();
            }

            File file = new File(Environment
                    .getExternalStorageDirectory().getAbsolutePath()
                    + File.separator + FOLDER_NAME + File.separator
                    + vehicleNumber + "_csa_non_icms_" + System.currentTimeMillis() + ".jpg");
            file.createNewFile();

            return file;

        } catch (IOException e) {
            e.printStackTrace();
            //Log.e(TAG, "createFolderWithVehicleNumber: " + e.getMessage() );
            return null;
        }
    }*/
}
