package com.tryonics.ceylincononmotor.util.location;

import android.util.Log;

public class LocationValidator {

    private final static String TAG = LocationValidator.class.getName();

    public static boolean meterDistanceBetweenPoints(double lat_a, double lng_a, double lat_b, double lng_b, int boundary) {
        float pk = (float) (180.f/ Math.PI);

        double a1 = lat_a / pk;
        double a2 = lng_a / pk;
        double b1 = lat_b / pk;
        double b2 = lng_b / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        double distance = 6366000 * tt;

        Log.e(TAG, "CURRENT LOCATION " + lat_a + ", "+lng_a);
        Log.e(TAG, "TARGET LOCATION " + lat_b + ", "+lng_b);
        Log.e(TAG, "DISTANCE " + distance);
        Log.e(TAG, "BOUNDARY " + boundary);

        if(distance <= boundary){
           return true;
        }else{
            return false;
        }

    }

    public static boolean isValidLatLng(double lat, double lng){
        if(lat < -90 || lat > 90) {
            return false;
        } else if(lng < -180 || lng > 180) {
            return false;
        }
        return true;
    }



}
