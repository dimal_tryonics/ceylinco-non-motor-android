package com.tryonics.ceylincononmotor.util;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

/**
 * Created by Dimal Perera on 07/05/2020.
 * Version 1.1
 */
public class LocationServiceValidator {

    private static final String TAG = LocationServiceValidator.class.getSimpleName();

    private Context context;
    private OnLocationServiceListener locationServiceListener;
    private int locationMode = -1;
    private boolean result = false;

    public LocationServiceValidator(Context context) {
        this.context = context;
    }

    private void isLocationServiceAvailable(Context context) {

        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            Log.e(TAG, e.getMessage());
            result = false;
            locationServiceListener.LocationServiceUnavailable();
        }

        Log.e(TAG, "LOCATION MODE " + locationMode);

        switch (locationMode) {
            case Settings.Secure.LOCATION_MODE_HIGH_ACCURACY:
            case Settings.Secure.LOCATION_MODE_SENSORS_ONLY:
            case Settings.Secure.LOCATION_MODE_BATTERY_SAVING:
                locationServiceListener.LocationServiceAvailable(locationMode);
                result = true;
                break;
            case Settings.Secure.LOCATION_MODE_OFF:
                result = false;
                locationServiceListener.LocationServiceUnavailable();
                break;
        }

        Log.e(TAG, "GPS Result " + result);

    }


    public void setLocationServiceListener(OnLocationServiceListener locationServiceListener) {
        this.locationServiceListener = locationServiceListener;
        this.isLocationServiceAvailable(context);
    }


    public interface OnLocationServiceListener {
        void LocationServiceAvailable(int LocationMode);

        void LocationServiceUnavailable();
    }
}