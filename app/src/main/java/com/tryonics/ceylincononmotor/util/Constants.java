package com.tryonics.ceylincononmotor.util;

import com.tryonics.ceylincononmotor.R;

public final class Constants {

	//UAT
	//public static final String BASE_URL = "http://134.209.154.207/ceylinco-non-motor-assessor/api/v1.0/";

	//QA
	//public static final String BASE_URL = "http://134.209.154.207/ceylinco-non-motor-assessor/api/v1.0/";

	//Live
	public static final String BASE_URL = "http://134.209.154.207/ceylinco-non-motor-assessor/api/v1.0/";

	//Hours
	public static final long JOB_DELETE_TIME = 3;

	public static final String CLAIM = "OST";
	public static final String UNDERWRITING = "UWI";

	public static final int PENDING_JOB = 10;
	public static final int INPROGRESS_JOB = 11;
	public static final int CLOSED_JOB = 12;

	//CATEGORIES
	public static final int CATEGORY_PHOTOS = 1;
	public static final int CATEGORY_SCAN_DOCUMENT = 2;
	public static final int CATEGORY_REPORT_NORMAL = 3;
	public static final int CATEGORY_REPORT_OTS = 4;
	public static final int CATEGORY_REPORT_OTS_FORM = 5;
	public static final int CATEGORY_REPORT_FLA = 6;
	public static final int CATEGORY_REPORT_FLA_FORM = 7;
	public static final int CATEGORY_REPORT_LOCATION = 8;

	//BUNDLE DATA PASS
	public static final String EXTRA_JOB_DATA = "job_data";
	public static final String EXTRA_JOB_CATEGORY = "job_CATEGORY";
	public static final String EXTRA_IMAGE_PATH = "image_path";

	public static final int REQUEST_IMAGE_CAPTURE = 200;
	public static final int REQUEST_CUSTOM_CAMERA = 201;

	//ROW SYNC STATUS
	public static final int SYNC_PENDING = 100;
	public static final int SYNC_COMPLETE = 101;
	public static final int SYNC_FAIL = 102;

	//FORM FIELDS
	public static final int FORM_FIELD_DEDUCT = 300;
	public static final int FORM_FIELD_IGNORE = 301;
	public static final int FORM_FIELD_COLLECT = 302;

	//FORM FIELDS
	public static final int LOCATION_TYPE_1 = 350;
	public static final int LOCATION_TYPE_2 = 351;

	//SYNC LEVEL
	public static final int SYNC_LEVEL_UNTOUCH = 400;
	public static final int SYNC_LEVEL_INIT = 401;
	public static final int SYNC_LEVEL_PHOTOS = 402;
	public static final int SYNC_LEVEL_FORM = 403;
	public static final int SYNC_LEVEL_LOCATION = 404;
	public static final int SYNC_LEVEL_SYNC_COMPLETE = 405;

	//FILTERS
	public static final int FILTER_CLAIM = 1;
	public static final int FILTER_UNDERWRITING = 2;
	public static final int FILTER_ALL = 3;
	public static final int FILTER_OST = 4;


}
