package com.tryonics.ceylincononmotor.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;

import androidx.core.content.ContextCompat;

/**
 * Created by Dimal on 12/21/16.
 */

public class IMEI {

    public static String getIMEI(Context context){


        /*TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

        String imei = null;

        if (android.os.Build.VERSION.SDK_INT >= 26) {
            imei = telephonyManager.getImei();
        }
        else
        {
            imei = telephonyManager.getDeviceId();
        }

        Logger.d("DEVICE IMEI NO", imei);

        return imei;*/

        try {

            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            String imei;

            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    imei =  telephonyManager.getImei();
                } else {
                    imei = telephonyManager.getDeviceId();
                }

                return imei;

                /*if (imei != null && !imei.isEmpty()) {
                    return imei;
                } else {
                    return android.os.Build.SERIAL;
                }*/
            }

        } catch (Exception e) {

            return null;
        }

        return null;
    }

}


