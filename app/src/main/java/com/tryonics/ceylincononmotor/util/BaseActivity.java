package com.tryonics.ceylincononmotor.util;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.tryonics.ceylincononmotor.R;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    Dialog networkErrorDialog;
    private NetworkErrorDialogCallback listener;

    public void showNetworkErrorDialog() {
        networkErrorDialog = new Dialog(this, R.style.dialog_light);

        networkErrorDialog.setContentView(R.layout.dialog_network_error_message);
        networkErrorDialog.setCancelable(false);

        TextView tvTitle = networkErrorDialog.findViewById(R.id.tvTitle);
        TextView tvMessage = networkErrorDialog.findViewById(R.id.tvMessage);

        Button btnCancel = networkErrorDialog.findViewById(R.id.btnCancle);
        btnCancel.setText("OK");

        tvTitle.setText("No Network Connection");
        tvMessage.setText("Mobile data is disabled connect to wifi network instead, or enable mobile data and try again.");

        btnCancel.setOnClickListener(v -> listener.onCancelClickListener());
        networkErrorDialog.show();

    }

    public void setNetworkErrorDialogListener(NetworkErrorDialogCallback listener){
        this.listener = listener;
    }

    public void networkErrorDialogDismiss() {
        networkErrorDialog.dismiss();
    }

    public interface NetworkErrorDialogCallback {
        void onCancelClickListener();
    }

    public void showInfoDialog(String dialogTitle, String dialogMessage) {

        Dialog dialog = new Dialog(this, R.style.dialog_light);
        dialog.setContentView(R.layout.dialog_text_message);
        dialog.setCancelable(false);

        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        TextView tvMessage = dialog.findViewById(R.id.tvMessage);

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancle);
        btnCancel.setText("OK");

        tvTitle.setText(dialogTitle);
        tvMessage.setText(dialogMessage);

        btnCancel.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }
}
