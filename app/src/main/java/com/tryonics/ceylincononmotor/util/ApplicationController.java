package com.tryonics.ceylincononmotor.util;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.util.Log;

import com.tryonics.ceylincononmotor.BuildConfig;
import com.tryonics.ceylincononmotor.database.dao.DaoMaster;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;

import net.gotev.uploadservice.Logger;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.okhttp.OkHttpStack;

import org.greenrobot.greendao.database.Database;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import timber.log.Timber;


public class ApplicationController extends Application {

    public static final String CHANNEL_ID = "com.tryonics.ceylincononmotor";
    public static final String CHANNEL_ID_FAIL = "com.tryonics.ceylincononmotor_fail";

    public static final String CHANNEL_DESCRIPTION = "Ceylinco";

    public static final String CHANNEL_ID_UPLOAD_SERVICE = "Upload Service";

    private static final String TAG = ApplicationController.class.getName();
    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannel();
        createNotificationChannelFail();
        createUploadNotificationChannel();


        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "notes-db");
        Database db = helper.getWritableDb();

        // encrypted SQLCipher database
        // note: you need to add SQLCipher to your dependencies, check the build.gradle file
        // DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "notes-db-encrypted");
        // Database db = helper.getEncryptedWritableDb("encryption-key");

        daoSession = new DaoMaster(db).newSession();

        createUploadService();

        Timber.plant(new FileLoggingTree(getApplicationContext()));
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    public void createUploadService() {
        //UploadServiceConfig.initialize(this, CHANNEL_ID_UPLOAD_SERVICE, BuildConfig.DEBUG);
        //UploadServiceConfig.setNotificationHandlerFactory(uploadService -> new MyNotificationHandler(uploadService));
        //UploadServiceConfig.setNotificationHandlerFactory(MyNotificationHandler::new);
        //new GlobalRequestObserver(this, new GlobalRequestObserverDelegate());
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;

        net.gotev.uploadservice.Logger.setLogLevel(Logger.LogLevel.DEBUG);

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);
        UploadService.HTTP_STACK = new OkHttpStack(okHttpBuilder.build());
    }


    private void createNotificationChannel() {

        //Android O == API 26
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Log.e(TAG, "Notification Channel Created:");

            NotificationChannel serviceChannel = new NotificationChannel(
                    ApplicationController.CHANNEL_ID,
                    CHANNEL_DESCRIPTION,
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            if (manager != null) {
                manager.createNotificationChannel(serviceChannel);
            }
        }
    }

    private void createNotificationChannelFail() {
        //Android O == API 26
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.e(TAG, "createNotificationChannel: 1");
            NotificationChannel serviceChannel = new NotificationChannel(
                    ApplicationController.CHANNEL_ID_FAIL,
                    "Foreground Service Channel 2",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            if (manager != null) {
                manager.createNotificationChannel(serviceChannel);
            }
        }
    }

    private void createUploadNotificationChannel() {
        //Android O == API 26
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.e(TAG, "Notification Channel Created:");
            NotificationChannel serviceChannel = new NotificationChannel(
                    ApplicationController.CHANNEL_ID_UPLOAD_SERVICE,
                    CHANNEL_DESCRIPTION,
                    NotificationManager.IMPORTANCE_LOW
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            if (manager != null) {
                manager.createNotificationChannel(serviceChannel);
            }
        }
    }

}
