package com.tryonics.ceylincononmotor.util;

import android.util.Log;

import java.io.File;

/**
 * Created by Dimal on 2/1/17.
 */

public class ImageManager {

    private static String TAG = ImageManager.class.getName();


    public static void removeLocalImage(String filePath){

        File file = new File(filePath);

        if (file.exists()) {

            Log.e(TAG, "File path - " + filePath);

            if (file.delete()) {
                Log.e(TAG, "File deleted");
            } else {
                Log.e(TAG, "File not deleted");
            }
        }else{
            Log.e(TAG, "File not exists");
        }

    }
}
