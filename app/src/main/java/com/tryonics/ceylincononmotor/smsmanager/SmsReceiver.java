package com.tryonics.ceylincononmotor.smsmanager;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.tryonics.ceylincononmotor.BuildConfig;
import com.tryonics.ceylincononmotor.R;
import com.tryonics.ceylincononmotor.database.dao.DaoSession;
import com.tryonics.ceylincononmotor.database.dao.JobData;
import com.tryonics.ceylincononmotor.tryonicsevents.RefreshPendingJobEvent;
import com.tryonics.ceylincononmotor.ui.ViewPagerActivity;
import com.tryonics.ceylincononmotor.util.AppPreferences;
import com.tryonics.ceylincononmotor.util.ApplicationController;
import com.tryonics.ceylincononmotor.util.Constants;
import com.tryonics.ceylincononmotor.util.DateTimeModule;

import org.greenrobot.eventbus.EventBus;

import timber.log.Timber;

/**
 * Created by Dimal on 27/04/2020.
 */

public class SmsReceiver extends BroadcastReceiver {

    public static final String FILTER_BY_SMS_PREFIX = "HNBGI";

    /*

    The Job Assignment SMS
    HT0007TC001379-CLHT00TC20000004*0HT002003526088-1-OST*03-04-2020*OST

    HT0007TC001379-CLHT00TC20000004 - Reference No
    0HT002003526088-1-OST - Claim no
    03-04-2020 - Job create date
    OST

    The Job Rejection SMS
    JD  ##  0DL002003511681

    HT0007TC001379-CLHT00TC20000004*0ST002003526088-1-OST*03-04-2020*OST

    HT0007TC0013333-CLHT00456008888*UWI002003526055567-1-UWI*03-04-2020*OST

    */
    public static final String FILTER_CEYLINCO_ADD_JOB = "A";
    public static final String FILTER_CEYLINCO_REJECT_JOB = "R";


    public static final int NOTIFICATION_ID = 1923;

    private static final String TAG = SmsReceiver.class.getName();

    public Context context;
    public AppPreferences appPreferences;

    @TargetApi(Build.VERSION_CODES.M)
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        appPreferences = new AppPreferences(context);

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();
        String format = bundle.getString("format");

        try {

            // Check the Android version.
            boolean isVersionM = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);

            final Object[] pdusObj = (Object[]) bundle.get("pdus");

            for (Object o : pdusObj) {

                SmsMessage currentMessage;

                if (isVersionM) {
                    // If Android version M or newer:
                    currentMessage = SmsMessage.createFromPdu((byte[]) o, format);
                } else {
                    // If Android version L or older:
                    currentMessage = SmsMessage.createFromPdu((byte[]) o);
                }

                String prefix = currentMessage.getOriginatingAddress();
                String senderNumber = currentMessage.getDisplayOriginatingAddress();
                String message = currentMessage.getDisplayMessageBody().replace("*", "#");

                Timber.i(TAG + " - " + "Prefix: " + prefix + " Sender Number: " + senderNumber + " Message: " + message);

                splitMessage(message);


            }

        } catch (Exception e) {
            Timber.i(TAG + " - " + " SMS Receiver Exception "+ e.getMessage());

        }
    }

    public void splitMessage(String message) {

        String[] decodeSms = message.split("#");

        String jobReference = decodeSms[0];

        String claimDetail = decodeSms[1];

        String claimNo = claimDetail.split("-")[0] + "-" + claimDetail.split("-")[1];
        String claimType = claimDetail.split("-")[2];
        String jobCreateDate = decodeSms[2];

        saveMessage(jobReference, claimNo, claimType, jobCreateDate);
    }

    private void saveMessage(@NonNull String jobReference, @NonNull String claimNo, @NonNull String claimType, @NonNull String jobCreateDate) {

        DaoSession daoSession = ((ApplicationController) context.getApplicationContext()).getDaoSession();

        JobData jobData = new JobData();
        jobData.setSmsJobRefNo(jobReference);
        jobData.setSmsClaimNo(claimNo);
        jobData.setSmsJobType(claimType);
        jobData.setSmsJobCreateDate(jobCreateDate);
        jobData.setSmsReceivedTime(DateTimeModule.getCurrentTimeStamp());
        jobData.setCurrentJobStatus(Constants.PENDING_JOB);
        jobData.setJobSyncLevel(Constants.SYNC_LEVEL_UNTOUCH);

        daoSession.insert(jobData);

        EventBus.getDefault().postSticky(new RefreshPendingJobEvent());

        Timber.i(TAG + " - " + "SMS ADDED TO DATABASE");

        sendNotification(context, "A");

        /*for (int i = 0 ; i < 100 ; i++) {
            JobData jobData = new JobData();
            jobData.setSmsJobRefNo(jobReference);
            jobData.setSmsClaimNo(claimNo+i);


            if((i % 2) == 0) {
                jobData.setSmsJobType("OST");
            }else{
                jobData.setSmsJobType("UWI");
            }
            jobData.setSmsJobCreateDate(jobCreateDate);

            jobData.setSmsReceivedTime(DateTimeModule.getCurrentTimeStamp());
            jobData.setCurrentJobStatus(Constants.PENDING_JOB);

            daoSession.insert(jobData);
        }*/


    }


    public void sendNotification(Context context, String jobType) {

        // Use NotificationCompat.Builder to set up our notification.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, ApplicationController.CHANNEL_ID);

        //icon appears in device notification bar and right hand corner of notification
        builder.setSmallIcon(R.mipmap.ic_launcher);
        // Large icon appears on the left of the notification
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));

        if (jobType.equals(SmsReceiver.FILTER_CEYLINCO_REJECT_JOB)) {
            // Content title, which appears in large type at the top of the notification
            builder.setContentTitle("Job rejection request received");
            // Content text, which appears in smaller text below the title
            builder.setContentText("The job rejection SMS is sent by the CRC");
            // The subtext, which appears under the text on newer devices.
            // This will show-up in the devices with Android 4.2 and above only
            builder.setSubText("The job rejection SMS is sent by the CRC");
        } else if (jobType.equals(SmsReceiver.FILTER_CEYLINCO_ADD_JOB)) {
            // Content title, which appears in large type at the top of the notification
            builder.setContentTitle("New job request received");
            // Content text, which appears in smaller text below the title
            builder.setContentText("The job assignment SMS is sent by the CRC");
            // The subtext, which appears under the text on newer devices.
            // This will show-up in the devices with Android 4.2 and above only
            builder.setSubText("The job assignment SMS is sent by the CRC");
        }


        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);
        builder.setAutoCancel(true);


        Intent resultIntent = new Intent(context, ViewPagerActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        builder.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Will display the notification in the notification bar
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }


}
